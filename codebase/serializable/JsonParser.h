//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//
//
// JsonParser.h
//
// Copyright (C) 2016 Min Shih
//
#pragma once

#include <vidi_base_export.h>

#include <vidiBase.h>
#include <vidiPlatform.h>

#include <cassert>
#include <cmath>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <type_traits>
#include <typeinfo>
#include <utility>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Macros                                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
#define JSON_IS_TYPE_FUNC(NameType, NameTYPE) \
    __forceinline bool is##NameType() const { return (type() == NameTYPE##_TYPE); }

#define JSON_TO_TYPE_FUNC(NameType, NameTYPE, field)   \
    __forceinline NameType& to##NameType()             \
    {                                                  \
        _assert(NameTYPE##_TYPE);                      \
        return field;                                  \
    }                                                  \
    __forceinline const NameType& to##NameType() const \
    {                                                  \
        _assert(NameTYPE##_TYPE);                      \
        return field;                                  \
    }

#define JSON_VALUE_NUMERIC(NameType, NameTYPE, field)                                   \
    JSON_IS_TYPE_FUNC(NameType, NameTYPE)                                               \
    __forceinline JsonValue(NameType value) : _type(NameTYPE##_TYPE) { field = value; } \
    __forceinline NameType& to##NameType()                                              \
    {                                                                                   \
        _assert(NameTYPE##_TYPE);                                                       \
        return field;                                                                   \
    }                                                                                   \
    __forceinline NameType to##NameType() const { return toNumber<NameType>(); }

#define JSON_ASSERT_RETURN(NameTYPE, field) \
    _assert(NameTYPE);                      \
    return field;

// This macro is for warning unexpected value types. This situation should not happen usually,
// unless there is a data corruption. So we do not throw an exception.
#define JSON_WRONG_TYPE                                                                   \
    {                                                                                     \
        assert(false);                                                                    \
        vidi::forcewarn() << "[JsonValue] unexpected type found in " << __FILE__ << " : " \
                          << __LINE__ << std::endl;                                       \
    }

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  JsonValue                                                                 //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace serializable {

class JsonValue;
class JsonParser;

/**
 * @brief Store the JSON data structure
 */
class JsonValue {
    friend class JsonParser;

public:
    // JSON Data Type
    enum Type {
        NULL_TYPE = 0, // clang-format off
        INT8_TYPE,  INT16_TYPE,  INT32_TYPE,  INT64_TYPE,
        UINT8_TYPE, UINT16_TYPE, UINT32_TYPE, UINT64_TYPE,
        FLOAT_TYPE, DOUBLE_TYPE, STRING_TYPE, BOOL_TYPE,
        ARRAY_TYPE, OBJECT_TYPE // clang-format on
    };
    // Scalar Types
    typedef bool        Bool;   // Boolean Type
    typedef int8_t      Int8;   // Numeric Type
    typedef int16_t     Int16;  //
    typedef int32_t     Int32;  //
    typedef int64_t     Int64;  //
    typedef uint8_t     UInt8;  //
    typedef uint16_t    UInt16; //
    typedef uint32_t    UInt32; //
    typedef uint64_t    UInt64; //
    typedef float       Float;  //
    typedef double      Double; //
    typedef std::string String; // String Type
    // Other Types
    typedef std::string KeyType;
    typedef int         IdxType;
    // Non-scalar Type
    typedef std::vector<JsonValue>       Array;  // JSON Array Type
    typedef std::map<KeyType, JsonValue> Object; // JSON Object Type

public:
    static const JsonValue       Null;
    static const std::type_info& typeInfo(Type type);
    static std::string           typeName(Type type);

private:
    void _assert_single(int& count, Type expectedType) const noexcept
    {
        if (_type == expectedType)
            ++count;
    }
    template<typename... Args> void _assert(Args... expectedType) const
    {
        int count = 0;
        int unpack[]{ 0, (_assert_single(count, std::forward<Args>(expectedType)), 0)... };
        static_cast<void>(unpack);
        if (count == 0)
            throw ValueTypeException(_type, std::forward<Args>(expectedType)...);
    }
    union {
        // clang-format off
        Int8   i8; Int16   i16; Int32   i32;  Int64  i64;
        UInt8 ui8; UInt16 ui16; UInt32 ui32; UInt64 ui64;
        Bool    b; Float     f; Double    d;
        // clang-format on
        String* str;
        Array*  arr;
        Object* obj;
    } _data;    // the actual storage
    Type _type; // the corresponding type

public:
    class ValueTypeException : public Exception {
    public:
        template<typename... Args>
        ValueTypeException(Type type, Args... expectedType)
          : Exception("[JsonValue::ValueTypeException] Expected types:",
                      formMessage(expectedType...) + ";",
                      "got \"" + typeName(type) + "\".")
        {
        }

        template<typename... Args> std::string formMessage(Args&&... args)
        {
            std::string result;
            int         unpack[]{
                0, (result += "\"" + JsonValue::typeName(std::forward<Args>(args)) + "\", ", 0)...
            };
            static_cast<void>(unpack);
            return result.substr(0, result.size() - 2);
        }
    };

    class InvalidIterationError : public Exception {
    public:
        explicit InvalidIterationError(const std::string& message) : Exception(message) {}
    };

    class InvalidKeyError : public Exception {
    public:
        explicit InvalidKeyError(const KeyType& key)
          : Exception("[JsonValue::InvalidKeyError] required object key",
                      "\"" + key + "\"",
                      "not found.")
        {
        }
    };

    class InvalidIndexError : public Exception {
    public:
        explicit InvalidIndexError(const IdxType& idx)
          : Exception("[JsonValue::InvalidIndexError] invalid index", idx, "found.")
        {
        }
    };

private:
    /**
     * This class implements the iterator for JsonValue. However only
     * Array and Object supports the use of iterators. Essentially it
     * contains a pointer to a pair of string and JsonValue.
     * @tparam JT
     */
    template<typename JT> class IterType {
        friend class JsonValue;

    private:
        union {
            Array::iterator  _ait;
            Object::iterator _oit;
        };
        bool   _af  = false;
        size_t _idx = 0;

    public:
        typedef std::input_iterator_tag iterator_category;
        typedef JT                      value_type;
        typedef ptrdiff_t               difference_type;
        typedef JT*                     pointer;
        typedef JT&                     reference;
        ~IterType() {};
        explicit IterType(Array::iterator x, size_t idx) : _ait(x), _idx(idx), _af(true) {}
        explicit IterType(Object::iterator x) : _oit(x), _af(false) {}
        IterType(const IterType& it) : _af(it._af)
        {
            if (_af) {
                _ait = it._ait;
                _idx = it._idx;
            }
            else {
                _oit = it._oit;
            }
        }
        const JT& operator*() { return _af ? *_ait : _oit->second; }
        IterType& operator++()
        {
            if (_af) {
                ++_ait;
                ++_idx;
            }
            else
                ++_oit;
            return *this;
        }
        const IterType operator++(int) // NOLINT(readability-const-return-type)
        {
            IterType tmp(*this);
                     operator++();
            return tmp;
        }
        bool operator==(const IterType& rhs) const
        {
            return _af ? _ait == rhs._ait : _oit == rhs._oit;
        }
        bool operator!=(const IterType& rhs) const
        {
            return _af ? _ait != rhs._ait : _oit != rhs._oit;
        }
        const std::string& key() const
        {
            if (_af)
                throw TypeError("JSON array iterator does not have a key, use idx() instead.");
            else
                return _oit->first;
        }
        const size_t& idx() const
        {
            if (!_af)
                throw TypeError("JSON object iterator does not have an index, "
                                "use key() instead.");
            else
                return _idx;
        }
    };

public:
    using iterator       = IterType<JsonValue>;
    using const_iterator = IterType<const JsonValue>;

public:
    /** Special Functions */
    /// @{
    ~JsonValue() { clear(); }
    JsonValue() : _type(NULL_TYPE) {}
    JsonValue(JsonValue&& other) noexcept : _type(NULL_TYPE) { *this = std::move(other); }
    JsonValue(const JsonValue& other) : _type(NULL_TYPE) { *this = other; }
    JsonValue& operator=(const JsonValue& other);
    JsonValue& operator=(JsonValue&& other) noexcept;
    /// @}

    /** Comparison Operators */
    /// @{
    bool             operator==(const JsonValue& other) const;
    bool             operator!=(const JsonValue& other) const { return !(*this == other); }
    JsonValue&       operator[](const IdxType& idx);
    const JsonValue& operator[](const IdxType& idx) const;
    JsonValue&       operator[](const KeyType& key);
    const JsonValue& operator[](const KeyType& key) const;
    /// @}

    /** @brief Create an empty JSON of the corresponding type */
    JsonValue(Type type);
    /** @brief Access the type of the JSON */
    Type type() const { return _type; }
    /** @brief swap the values */
    void swap(JsonValue& other) noexcept;
    /** @brief clear everything and set to null */
    void clear();

    /** Type Conversion */
    /// @{
    JsonValue(Bool value) : _type(BOOL_TYPE) { _data.b = value; }
    JsonValue(const char* value) : _type(STRING_TYPE) { _data.str = new String(value); }
    JsonValue(const String& value) : _type(STRING_TYPE) { _data.str = new String(value); }
    JSON_VALUE_NUMERIC(Int8, INT8, _data.i8)
    JSON_VALUE_NUMERIC(Int16, INT16, _data.i16)
    JSON_VALUE_NUMERIC(Int32, INT32, _data.i32)
    JSON_VALUE_NUMERIC(Int64, INT64, _data.i64)
    JSON_VALUE_NUMERIC(UInt8, UINT8, _data.ui8)
    JSON_VALUE_NUMERIC(UInt16, UINT16, _data.ui16)
    JSON_VALUE_NUMERIC(UInt32, UINT32, _data.ui32)
    JSON_VALUE_NUMERIC(UInt64, UINT64, _data.ui64)
    JSON_VALUE_NUMERIC(Float, FLOAT, _data.f)
    JSON_VALUE_NUMERIC(Double, DOUBLE, _data.d)
    JSON_IS_TYPE_FUNC(Null, NULL)
    JSON_IS_TYPE_FUNC(Bool, BOOL)
    JSON_IS_TYPE_FUNC(String, STRING)
    JSON_IS_TYPE_FUNC(Array, ARRAY)
    JSON_IS_TYPE_FUNC(Object, OBJECT)
    JSON_TO_TYPE_FUNC(Bool, BOOL, _data.b)
    JSON_TO_TYPE_FUNC(String, STRING, *_data.str)
    JSON_TO_TYPE_FUNC(Array, ARRAY, *_data.arr)
    JSON_TO_TYPE_FUNC(Object, OBJECT, *_data.obj)
    const char* toCString() const { JSON_ASSERT_RETURN(STRING_TYPE, _data.str->c_str()) }
    bool        isInt() const { return (_type == INT32_TYPE); }
    int&        toInt() { JSON_ASSERT_RETURN(INT32_TYPE, _data.i32) }
    const int&  toInt() const { JSON_ASSERT_RETURN(INT32_TYPE, _data.i32) }
    /** @brief Generic function to check if the JSON is a numeric type */
    bool isNumber() const
    {
        switch (_type) {
        // clang-format off
        case INT8_TYPE:  case INT16_TYPE:  case INT32_TYPE:  case INT64_TYPE:
        case UINT8_TYPE: case UINT16_TYPE: case UINT32_TYPE: case UINT64_TYPE:
        case FLOAT_TYPE: case DOUBLE_TYPE: return true; // clang-format on
        default:
            break;
        }
        return false;
    }
    /** Generic function to access the JSON as a numeric type */
    template<typename T> T toNumber() const
    {
        switch (_type) {
        case INT8_TYPE:
            return (T)_data.i8;
        case INT16_TYPE:
            return (T)_data.i16;
        case INT32_TYPE:
            return (T)_data.i32;
        case INT64_TYPE:
            return (T)_data.i64;
        case UINT8_TYPE:
            return (T)_data.ui8;
        case UINT16_TYPE:
            return (T)_data.ui16;
        case UINT32_TYPE:
            return (T)_data.ui32;
        case UINT64_TYPE:
            return (T)_data.ui64;
        case FLOAT_TYPE:
            return (T)_data.f;
        case DOUBLE_TYPE:
            return (T)_data.d;
        default:
            break;
        }
        return 0;
    }
    /// @}

    /** Iterator Operations */
    /// @{
    iterator begin()
    {
        _assert(ARRAY_TYPE, OBJECT_TYPE);
        if (_type == ARRAY_TYPE)
            return iterator(_data.arr->begin(), 0);
        else
            return iterator(_data.obj->begin());
    }
    const_iterator begin() const
    {
        _assert(ARRAY_TYPE, OBJECT_TYPE);
        if (_type == ARRAY_TYPE)
            return const_iterator(_data.arr->begin(), 0);
        else
            return const_iterator(_data.obj->begin());
    }
    iterator end()
    {
        _assert(ARRAY_TYPE, OBJECT_TYPE);
        if (_type == ARRAY_TYPE)
            return iterator(_data.arr->end(), _data.arr->size());
        else
            return iterator(_data.obj->end());
    }
    const_iterator end() const
    {
        _assert(ARRAY_TYPE, OBJECT_TYPE);
        if (_type == ARRAY_TYPE)
            return const_iterator(_data.arr->end(), _data.arr->size());
        else
            return const_iterator(_data.obj->end());
    }
    int size() const; // 0 if not array nor object
    /// @}

    /** Access Through Keys or Indices */
    /// @{
    bool contains(const KeyType& key) const; // object or null
    bool contains(const IdxType& idx) const; // array or null
    template<typename T1, typename T2, class... Args>
    bool contains(const T1& hint1, const T2& hint2, Args... hints) const
    {
        if (contains(hint1))
            return (*this)[hint1].contains(hint2, hints...);
        else
            return false;
    }
    const JsonValue& fetch(IdxType idx) const;
    JsonValue& fetch(IdxType idx);
    const JsonValue& fetch(KeyType key) const;
    JsonValue& fetch(KeyType key);
    template<typename T1, typename T2, class... Args>
    const JsonValue& fetch(const T1& hint1, const T2& hint2, Args... hints) const
    {
        return (*this).fetch(hint1).fetch(hint2, hints...);
    }
    template<typename T1, typename T2, class... Args>
    JsonValue& fetch(const T1& hint1, const T2& hint2, Args... hints)
    {
        return (*this).fetch(hint1).fetch(hint2, hints...);
    }
    const JsonValue& fetchDefault(const JsonValue& defaultValue, IdxType idx) const noexcept;
    const JsonValue& fetchDefault(const JsonValue& defaultValue, KeyType key) const noexcept;
    JsonValue& fetchDefault(JsonValue& defaultValue, IdxType idx) noexcept;
    JsonValue& fetchDefault(JsonValue& defaultValue, KeyType key) noexcept;
    /// @}

    /** Other Functions */
    /// @{
    void resize(int size)
    {
        // _assert(ARRAY_TYPE, NULL_TYPE);
        if (isNull())
            *this = JsonValue(ARRAY_TYPE);
        _data.arr->resize(size);
    }
    void append(const JsonValue& value)
    {
        // _assert(ARRAY_TYPE, NULL_TYPE);
        if (isNull())
            *this = JsonValue(ARRAY_TYPE);
        _data.arr->push_back(value);
    }
    /// @}
};
} // namespace serializable
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  JsonParser                                                                //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
namespace vidi {
namespace serializable {
class JsonParser {
public:
    class ParseException : public Exception {
    public:
        ParseException(const std::string& message, int line) : Exception(message), _line(line) {}
        int line() const { return _line; }

    private:
        int _line;
    };

public:
    JsonParser();
    void        setIndent(int indent) { _indent = indent; }
    void        parse(std::istream& is, JsonValue& value) const;
    JsonValue   parse(const std::string& text) const;
    void        stringify(std::ostream& os, const JsonValue& value) const;
    std::string stringify(const JsonValue& value) const;
    bool        load(const std::string& fileName, JsonValue& value) const;
    bool        save(const std::string& fileName, const JsonValue& value) const;

private:
    char        getChar(std::istream& is) const;
    double      getNumber(std::istream& is) const;
    std::string getString(std::istream& is) const;
    bool        skipComment(std::istream& is) const;
    char        getNonSpaceChar(std::istream& is) const;
    bool        matchToken(std::istream& is, char token) const;
    bool        match(std::istream& is, const char* pattern, char* gots) const;
    void        readDigit(std::istream& is, JsonValue& value) const;
    void        readArray(std::istream& is, JsonValue& value) const;
    void        readObject(std::istream& is, JsonValue& value) const;
    void        readValue(std::istream& is, JsonValue& value) const;
    void        writeArray(std::ostream& os, const JsonValue& value) const;
    void        writeObject(std::ostream& os, const JsonValue& value) const;
    void        writeValue(std::ostream& os, const JsonValue& value) const;

protected:
    mutable int _line;
    mutable int _level;
    int         _indent;
};
} // namespace serializable
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  functions                                                                 //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
namespace vidi {
namespace serializable {
std::istream&
operator>>(std::istream& is, JsonValue& value);
std::ostream&
operator<<(std::ostream& os, const JsonValue& value);
} // namespace serializable
} // namespace vidi

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// finally the interface for object serialization                            //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
namespace vidi {
namespace serializable {
template<class T>
T
deserialize(const JsonValue& json)
{
    T obj;
    if (obj.fromJson(json))
        return obj;
    else
        throw Exception("Deserialization failure.");
}
template<class T>
JsonValue
serialize(const T& object)
{
    JsonValue json;
    object.toJson(json);
    return json;
}
} // namespace serializable
} // namespace vidi
