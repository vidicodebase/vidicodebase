//
// Created by Qi Wu on 2019-06-26.
//

#include "JsonSerialization.h"
#include <vidiMath.h>

using namespace vidi::math;

namespace vidi {
namespace serializable {

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Basic Scalar                                                              //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

template<>
std::string
deserialize<std::string>(const JsonValue& json)
{
    if (!json.isString())
        return std::string();
    return json.toString();
}

template<>
JsonValue
serialize<std::string>(const std::string& object)
{
    return JsonValue(object);
}

template<>
int8_t
deserialize<int8_t>(const JsonValue& json)
{
    if (!json.isInt8())
        return int8_t();
    return json.toInt8();
}

template<>
JsonValue
serialize<int8_t>(const int8_t& object)
{
    return JsonValue(object);
}

template<>
int16_t
deserialize<int16_t>(const JsonValue& json)
{
    if (!json.isInt16())
        return int16_t();
    return json.toInt16();
}

template<>
JsonValue
serialize<int16_t>(const int16_t& object)
{
    return JsonValue(object);
}

template<>
int32_t
deserialize<int32_t>(const JsonValue& json)
{
    if (!json.isInt32())
        return int32_t();
    return json.toInt32();
}

template<>
JsonValue
serialize<int32_t>(const int32_t& object)
{
    return JsonValue(object);
}

template<>
int64_t
deserialize<int64_t>(const JsonValue& json)
{
    if (!json.isInt64())
        return int64_t();
    return json.toInt64();
}

template<>
JsonValue
serialize<int64_t>(const int64_t& object)
{
    return JsonValue(object);
}

template<>
uint8_t
deserialize<uint8_t>(const JsonValue& json)
{
    if (!json.isUInt8())
        return uint8_t();
    return json.toUInt8();
}

template<>
JsonValue
serialize<uint8_t>(const uint8_t& object)
{
    return JsonValue(object);
}

template<>
uint16_t
deserialize<uint16_t>(const JsonValue& json)
{
    if (!json.isUInt16())
        return uint16_t();
    return json.toUInt16();
}

template<>
JsonValue
serialize<uint16_t>(const uint16_t& object)
{
    return JsonValue(object);
}

template<>
uint32_t
deserialize<uint32_t>(const JsonValue& json)
{
    if (!json.isUInt32())
        return uint32_t();
    return json.toUInt32();
}

template<>
JsonValue
serialize<uint32_t>(const uint32_t& object)
{
    return JsonValue(object);
}

template<>
uint64_t
deserialize<uint64_t>(const JsonValue& json)
{
    if (!json.isUInt64())
        return uint64_t();
    return json.toUInt64();
}

template<>
JsonValue
serialize<uint64_t>(const uint64_t& object)
{
    return JsonValue(object);
}


template<>
float
deserialize<float>(const JsonValue& json)
{
    if (!json.isNumber())
        return float();
    return json.toFloat();
}

template<>
JsonValue
serialize<float>(const float& object)
{
    return JsonValue(object);
}

template<>
double
deserialize<double>(const JsonValue& json)
{
    if (!json.isNumber())
        return double();
    return json.toDouble();
}

template<>
JsonValue
serialize<double>(const double& object)
{
    return JsonValue(object);
}

} // namespace serializable
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Range                                                                     //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace serializable {

static const char* const _MAXIMUM = "maximum";
static const char* const _MINIMUM = "minimum";

template<typename T>
Range<T>
RangefromJson(const JsonValue& json) noexcept
{
    Range<T> ret;
    if (json.contains(_MINIMUM) && json.contains(_MAXIMUM)) {
        ret.minimum() = deserialize<T>(json[_MINIMUM]);
        ret.maximum() = deserialize<T>(json[_MAXIMUM]);
    }
    return std::move(ret);
}

template<typename T>
JsonValue
RangetoJson(const Range<T>& val) noexcept
{
    JsonValue json;
    json[_MINIMUM] = serialize<T>(val.minimum());
    json[_MAXIMUM] = serialize<T>(val.maximum());
    return std::move(json);
}

template<>
range1i
deserialize(const JsonValue& json)
{
    return std::move(RangefromJson<int32_t>(json));
}

template<>
range1f
deserialize(const JsonValue& json)
{
    return std::move(RangefromJson<float>(json));
}

template<>
range1d
deserialize(const JsonValue& json)
{
    return std::move(RangefromJson<double>(json));
}

} // namespace serializable
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Vectors                                                                   //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace serializable {

// Special Names for Serialization
static const char* const _X = "x";
static const char* const _Y = "y";
static const char* const _Z = "z";
static const char* const _W = "w";

template<typename T>
Vector2<T>
Vec2fromJson(const JsonValue& json) noexcept
{
    Vector2<T> ret;
    if (json.contains(_X) && json.contains(_Y)) {
        ret.x = json[_X].template toNumber<T>();
        ret.y = json[_Y].template toNumber<T>();
    }
    return std::move(ret);
}

template<typename T>
JsonValue
Vec2toJson(const Vector2<T>& val) noexcept
{
    JsonValue json;
    json[_X] = val.x;
    json[_Y] = val.y;
    return std::move(json);
}

template<typename T, bool A = false>
Vector<T, 3, A>
Vec3fromJson(const JsonValue& json) noexcept
{
    Vector<T, 3, A> ret;
    if (json.contains(_X) && json.contains(_Y)) {
        ret.x = json[_X].template toNumber<T>();
        ret.y = json[_Y].template toNumber<T>();
        ret.z = json[_Z].template toNumber<T>();
    }
    return std::move(ret);
}

template<typename T, bool A = false>
JsonValue
Vec3toJson(const Vector<T, 3, A>& val) noexcept
{
    JsonValue json;
    json[_X] = val.x;
    json[_Y] = val.y;
    json[_Z] = val.z;
    return std::move(json);
}

template<typename T>
Vector4<T>
Vec4fromJson(const JsonValue& json) noexcept
{
    Vector4<T> ret;
    if (json.contains(_X) && json.contains(_Y)) {
        ret.x = json[_X].template toNumber<T>();
        ret.y = json[_Y].template toNumber<T>();
        ret.z = json[_Z].template toNumber<T>();
        ret.w = json[_W].template toNumber<T>();
    }
    return std::move(ret);
}

template<typename T>
JsonValue
Vec4toJson(const Vector4<T>& val) noexcept
{
    JsonValue json;
    json[_X] = val.x;
    json[_Y] = val.y;
    json[_Z] = val.z;
    json[_W] = val.w;
    return std::move(json);
}

template<>
vec2i
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y))
        return vec2i{};
    return vec2i{ json[_X].toInt32(), json[_Y].toInt32() };
}

template<>
vec2f
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y))
        return vec2f{};
    return vec2f{ json[_X].toFloat(), json[_Y].toFloat() };
}

template<>
vec2d
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y))
        return vec2d{};
    return vec2d{ json[_X].toDouble(), json[_Y].toDouble() };
}

template<>
vec3i
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y) || !json.contains(_Z))
        return vec3i{};
    return vec3i{ json[_X].toInt32(), json[_Y].toInt32(), json[_Z].toInt32() };
}

template<>
vec3f
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y) || !json.contains(_Z))
        return vec3f{};
    return vec3f{ json[_X].toFloat(), json[_Y].toFloat(), json[_Z].toFloat() };
}

template<>
vec3d
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y) || !json.contains(_Z))
        return vec3d{};
    return vec3d{ json[_X].toDouble(), json[_Y].toDouble(), json[_Z].toDouble() };
}

template<>
vec4i
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y) || !json.contains(_Z) || !json.contains(_W))
        return vec4i{};
    return vec4i{ json[_X].toInt32(), json[_Y].toInt32(), json[_Z].toInt32(), json[_W].toInt32() };
}

template<>
vec4f
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y) || !json.contains(_Z) || !json.contains(_W))
        return vec4f{};
    return vec4f{ json[_X].toFloat(), json[_Y].toFloat(), json[_Z].toFloat(), json[_W].toFloat() };
}

template<>
vec4d
deserialize(const JsonValue& json)
{
    if (!json.contains(_X) || !json.contains(_Y) || !json.contains(_Z) || !json.contains(_W))
        return vec4d{};
    return vec4d{ json[_X].toDouble(), json[_Y].toDouble(), json[_Z].toDouble(), json[_W].toDouble() };
}

template<>
JsonValue
serialize(const vec2i& object)
{
    return Vec2toJson(object);
}
template<>
JsonValue
serialize(const vec2f& object)
{
    return Vec2toJson(object);
}
template<>
JsonValue
serialize(const vec2d& object)
{
    return Vec2toJson(object);
}
template<>
JsonValue
serialize(const vec3i& object)
{
    return Vec3toJson(object);
}
template<>
JsonValue
serialize(const vec3f& object)
{
    return Vec3toJson(object);
}
template<>
JsonValue
serialize(const vec3d& object)
{
    return Vec3toJson(object);
}
template<>
JsonValue
serialize(const vec3ia& object)
{
    return Vec3toJson(object);
}
template<>
JsonValue
serialize(const vec3fa& object)
{
    return Vec3toJson(object);
}
template<>
JsonValue
serialize(const vec3da& object)
{
    return Vec3toJson(object);
}
template<>
JsonValue
serialize(const vec4i& object)
{
    return Vec4toJson(object);
}
template<>
JsonValue
serialize(const vec4f& object)
{
    return Vec4toJson(object);
}
template<>
JsonValue
serialize(const vec4d& object)
{
    return Vec4toJson(object);
}
} // namespace serializable
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Range                                                                     //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace serializable {

template<typename T, bool A = false>
Box<T, A>
BoxFromJson(const JsonValue& json) noexcept
{
    Box<T, A> ret;
    if (json.contains(_MINIMUM) && json.contains(_MAXIMUM)) {
        ret.minimum() = Vec3fromJson<T, A>(json[_MINIMUM]);
        ret.maximum() = Vec3fromJson<T, A>(json[_MAXIMUM]);
    }
    return std::move(ret);
}

template<typename T, bool A = false>
JsonValue
BoxToJson(const Box<T, A>& box) noexcept
{
    JsonValue json;
    json[_MINIMUM] = Vec3toJson<T, 3, A>(box.minimum());
    json[_MAXIMUM] = Vec3toJson<T, 3, A>(box.maximum());
    return json;
}

template<>
box3i
deserialize(const JsonValue& json)
{
    return BoxFromJson<int32_t>(json);
}
template<>
box3f
deserialize(const JsonValue& json)
{
    return BoxFromJson<float>(json);
}
template<>
box3fa
deserialize(const JsonValue& json)
{
    return BoxFromJson<float, true>(json);
}
template<>
box3d
deserialize(const JsonValue& json)
{
    return BoxFromJson<double>(json);
}

} // namespace serializable
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Colors                                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace serializable {

static const char* const R = "r";
static const char* const G = "g";
static const char* const B = "b";
static const char* const A = "a";

JsonValue
serializeColor(const vec3f& color)
{
    JsonValue json;
    json[R] = color.r;
    json[G] = color.g;
    json[B] = color.b;
    return json;
}

JsonValue
serializeColor(const vec4f& color)
{
    JsonValue json;
    json[R] = color.r;
    json[G] = color.g;
    json[B] = color.b;
    json[A] = color.a;
    return json;
}

vec3f
deserializeColor3f(const JsonValue& json)
{
    if (!json.contains(R) || !json.contains(G) || !json.contains(B))
        return vec3f{};
    return vec3f{ json[R].toFloat(), json[G].toFloat(), json[B].toFloat() };
}

vec4f
deserializeColor4f(const JsonValue& json)
{
    if (!json.contains(R) || !json.contains(G) || !json.contains(B) || !json.contains(A))
        return vec4f{};
    return vec4f{ json[R].toFloat(), json[G].toFloat(), json[B].toFloat(), json[A].toFloat() };
}

} // namespace serializable
} // namespace vidi
