//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_SERIALIZABLE_TRAIT_TO_STRING_H
#define VIDICODEBASE_SERIALIZABLE_TRAIT_TO_STRING_H

#include <string>
#include <vidi_base_export.h>

namespace vidi {
namespace serializable {

/**
 * @brief defines an object whose type can be deduced as a string
 */
class TraitToString {
public:
    virtual ~TraitToString() = default;
    /**
     * This function should return the class typename
     * @return
     */
    virtual std::string toString() const = 0;
};

} // namespace serializable
} // namespace vidi

#endif // VIDICODEBASE_SERIALIZABLE_TRAIT_TO_STRING_H
