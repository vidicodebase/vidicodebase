//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//
//
// JsonParser.cpp
//
// Copyright (C) 2016 Min Shih
//

#include "JsonParser.h"

#include <array>
#include <cctype>
#include <regex>
#include <sstream>
#include <utility>

namespace vidi {
namespace serializable {

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//  Static Variables                                                         //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

const JsonValue JsonValue::Null = JsonValue();

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  JsonValue                                                                 //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

JsonValue::JsonValue(Type type) : _type(type)
{
    switch (_type) {
    case NULL_TYPE: break;
    case BOOL_TYPE: _data.b = false; break;
    case INT8_TYPE: _data.i8 = 0; break;
    case INT16_TYPE: _data.i16 = 0; break;
    case INT32_TYPE: _data.i32 = 0; break;
    case INT64_TYPE: _data.i64 = 0; break;
    case UINT8_TYPE: _data.ui8 = 0; break;
    case UINT16_TYPE: _data.ui16 = 0; break;
    case UINT32_TYPE: _data.ui32 = 0; break;
    case UINT64_TYPE: _data.ui64 = 0; break;
    case FLOAT_TYPE: _data.f = 0.f; break;
    case DOUBLE_TYPE: _data.d = 0.0; break;
    case STRING_TYPE: _data.str = new String(); break;
    case ARRAY_TYPE: _data.arr = new Array(); break;
    case OBJECT_TYPE: _data.obj = new Object(); break;
    default: assert(false);
    }
}

JsonValue&
JsonValue::operator=(const JsonValue& other)
{
    clear();
    _type = other._type;
    switch (_type) {
    case NULL_TYPE: break;
    case BOOL_TYPE: _data.b = other._data.b; break;
    case INT8_TYPE: _data.i8 = other._data.i8; break;
    case INT16_TYPE: _data.i16 = other._data.i16; break;
    case INT32_TYPE: _data.i32 = other._data.i32; break;
    case INT64_TYPE: _data.i64 = other._data.i64; break;
    case UINT8_TYPE: _data.ui8 = other._data.ui8; break;
    case UINT16_TYPE: _data.ui16 = other._data.ui16; break;
    case UINT32_TYPE: _data.ui32 = other._data.ui32; break;
    case UINT64_TYPE: _data.ui64 = other._data.ui64; break;
    case FLOAT_TYPE: _data.f = other._data.f; break;
    case DOUBLE_TYPE: _data.d = other._data.d; break;
    case STRING_TYPE: _data.str = new String(*other._data.str); break;
    case ARRAY_TYPE: _data.arr = new Array(*other._data.arr); break;
    case OBJECT_TYPE: _data.obj = new Object(*other._data.obj); break;
    default: assert(false);
    }
    return *this;
}

JsonValue&
JsonValue::operator=(JsonValue&& other) noexcept
{
    swap(other);
    return *this;
}

bool
JsonValue::operator==(const JsonValue& other) const
{
    if (_type != other._type)
        return false;
    switch (_type) {
    case NULL_TYPE: return true;
    case BOOL_TYPE: return (_data.b == other._data.b);
    case INT8_TYPE: return (_data.i8 == other._data.i8);
    case INT16_TYPE: return (_data.i16 == other._data.i16);
    case INT32_TYPE: return (_data.i32 == other._data.i32);
    case INT64_TYPE: return (_data.i64 == other._data.i64);
    case UINT8_TYPE: return (_data.ui8 == other._data.ui8);
    case UINT16_TYPE: return (_data.ui16 == other._data.ui16);
    case UINT32_TYPE: return (_data.ui32 == other._data.ui32);
    case UINT64_TYPE: return (_data.ui64 == other._data.ui64);
    case FLOAT_TYPE: return (_data.f == other._data.f);
    case DOUBLE_TYPE: return (_data.d == other._data.d);
    case STRING_TYPE: return (*_data.str == *other._data.str);
    case ARRAY_TYPE: {
        if (size() != other.size())
            return false;
        for (auto it = _data.arr->cbegin(), it2 = other._data.arr->cbegin(); it != _data.arr->cend(); ++it, ++it2) {
            if (*it != *it2)
                return false;
        }
        return true;
    }
    case OBJECT_TYPE: {
        if (size() != other.size())
            return false;
        for (auto it = _data.obj->cbegin(); it != _data.obj->cend(); ++it) {
            if (!other.contains(it->first))
                return false;
            if (it->second != other[it->first])
                return false;
        }
        return true;
    }
    default: assert(false);
    }
    return false; // unreachable
}

JsonValue& JsonValue::operator[](const IdxType& index)
{
    _assert(ARRAY_TYPE, NULL_TYPE);
    if (index < 0)
        throw Exception("Negative array index");
    if (isNull())
        *this = JsonValue(ARRAY_TYPE);
    if (index >= (int)_data.arr->size()) {
        _data.arr->resize(index + 1);
    }
    return (*_data.arr)[index];
}

const JsonValue& JsonValue::operator[](const IdxType& index) const
{
    _assert(ARRAY_TYPE, NULL_TYPE);
    if (index < 0)
        throw Exception("Negative array index");
    if (isNull() || index >= int(_data.arr->size()))
        return Null;
    return (*_data.arr)[index];
}

JsonValue& JsonValue::operator[](const KeyType& key)
{
    _assert(OBJECT_TYPE, NULL_TYPE);
    if (isNull())
        *this = JsonValue(OBJECT_TYPE);
    if (!contains(key))
        (*_data.obj)[key] = JsonValue();
    return (*_data.obj)[key];
}

const JsonValue& JsonValue::operator[](const KeyType& key) const
{
    _assert(OBJECT_TYPE, NULL_TYPE);
    if (isNull() || !contains(key))
        return Null;
    return (*_data.obj)[key];
}

void
JsonValue::clear()
{
    switch (_type) {
    case NULL_TYPE:
    case BOOL_TYPE:
    case INT8_TYPE:
    case INT16_TYPE:
    case INT32_TYPE:
    case INT64_TYPE:
    case UINT8_TYPE:
    case UINT16_TYPE:
    case UINT32_TYPE:
    case UINT64_TYPE:
    case FLOAT_TYPE:
    case DOUBLE_TYPE: break;
    case STRING_TYPE: delete _data.str; break;
    case ARRAY_TYPE: delete _data.arr; break;
    case OBJECT_TYPE: delete _data.obj; break;
    default: assert(false);
    }
    _type = NULL_TYPE;
}

void
JsonValue::swap(JsonValue& other) noexcept
{
    std::swap(_type, other._type);
    std::swap(_data, other._data);
}

int
JsonValue::size() const
{
    switch (_type) {
    case NULL_TYPE:
    case BOOL_TYPE:
    case INT8_TYPE:
    case INT16_TYPE:
    case INT32_TYPE:
    case INT64_TYPE:
    case UINT8_TYPE:
    case UINT16_TYPE:
    case UINT32_TYPE:
    case UINT64_TYPE:
    case FLOAT_TYPE:
    case DOUBLE_TYPE:
    case STRING_TYPE: return 0;
    case ARRAY_TYPE: return (int)(_data.arr->size());
    case OBJECT_TYPE: return (int)(_data.obj->size());
    default: assert(false);
    }
    return 0; // unreachable
}

bool
JsonValue::contains(const KeyType& key) const
{
    if (!isObject())
        return false;
    return (_data.obj->find(key) != _data.obj->end());
}

bool
JsonValue::contains(const IdxType& idx) const
{
    if (!isArray())
        return false;
    return (_data.arr->size() >= static_cast<size_t>(idx + 1));
}

const JsonValue&
JsonValue::fetch(IdxType idx) const
{
    _assert(ARRAY_TYPE, NULL_TYPE);
    if (isNull() || idx < 0 || idx >= (int)_data.arr->size()) {
        throw InvalidIndexError(idx);
    }
    return (*_data.arr)[idx];
}

JsonValue&
JsonValue::fetch(IdxType idx)
{
    _assert(ARRAY_TYPE, NULL_TYPE);
    if (isNull() || idx < 0 || idx >= (int)_data.arr->size()) {
        throw InvalidIndexError(idx);
    }
    return (*_data.arr)[idx];
}

const JsonValue&
JsonValue::fetch(KeyType key) const
{
    _assert(OBJECT_TYPE, NULL_TYPE);
    if (isNull() || !contains(key)) {
        throw InvalidKeyError(key);
    }
    return (*_data.obj)[key];
}

JsonValue&
JsonValue::fetch(KeyType key)
{
    _assert(OBJECT_TYPE, NULL_TYPE);
    if (isNull() || !contains(key)) {
        throw InvalidKeyError(key);
    }
    return (*_data.obj)[key];
}

const JsonValue&
JsonValue::fetchDefault(const JsonValue& defaultValue, IdxType idx) const noexcept
{
    if (isNull() || !isArray()) {
        return defaultValue;
    }
    else if (isArray() && (idx < 0 || idx >= (int)_data.arr->size())) {
        return defaultValue;
    }
    return (*_data.arr)[idx];
}

JsonValue&
JsonValue::fetchDefault(JsonValue& defaultValue, IdxType idx) noexcept
{
    if (isNull() || !isArray()) {
        return defaultValue;
    }
    else if (isArray() && (idx < 0 || idx >= (int)_data.arr->size())) {
        return defaultValue;
    }
    return (*_data.arr)[idx];
}

const JsonValue&
JsonValue::fetchDefault(const JsonValue& defaultValue, KeyType key) const noexcept
{
    if (isNull() || !isObject() || !contains(key)) {
        return defaultValue;
    }
    return (*_data.obj)[key];
}

JsonValue&
JsonValue::fetchDefault(JsonValue& defaultValue, KeyType key) noexcept
{
    if (isNull() || !isObject() || !contains(key)) {
        return defaultValue;
    }
    return (*_data.obj)[key];
}

const std::type_info&
JsonValue::typeInfo(Type type)
{
     switch (type) {
    case NULL_TYPE: return typeid(nullptr);
    case BOOL_TYPE: return typeid(Bool);
    case INT8_TYPE: return typeid(Int8);
    case INT16_TYPE: return typeid(Int16);
    case INT32_TYPE: return typeid(Int32);
    case INT64_TYPE: return typeid(Int64);
    case UINT8_TYPE: return typeid(UInt8);
    case UINT16_TYPE: return typeid(UInt16);
    case UINT32_TYPE: return typeid(UInt32);
    case UINT64_TYPE: return typeid(UInt64);
    case FLOAT_TYPE: return typeid(Float);
    case DOUBLE_TYPE: return typeid(Double);
    case STRING_TYPE: return typeid(String);
    case ARRAY_TYPE: return typeid(Array);
    case OBJECT_TYPE: return typeid(Object);
    default: assert(false); return typeid(nullptr);
    }
}

std::string
JsonValue::typeName(Type type)
{
    switch (type) {
    case NULL_TYPE: return "Null";
    case BOOL_TYPE: return "Bool";
    case INT8_TYPE: return "Int8";
    case INT16_TYPE: return "Int16";
    case INT32_TYPE: return "Int32";
    case INT64_TYPE: return "Int64";
    case UINT8_TYPE: return "UInt8";
    case UINT16_TYPE: return "UInt16";
    case UINT32_TYPE: return "UInt32";
    case UINT64_TYPE: return "UInt64";
    case FLOAT_TYPE: return "Float";
    case DOUBLE_TYPE: return "Double";
    case STRING_TYPE: return "String";
    case ARRAY_TYPE: return "Array";
    case OBJECT_TYPE: return "Object";
    default: assert(false); return "";
    }
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  JsonParser                                                                //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

JsonParser::JsonParser() : _indent(2) {}

void
JsonParser::parse(std::istream& is, JsonValue& value) const
{
    _line = 1;
    readValue(is, value);
}

JsonValue
JsonParser::parse(const std::string& text) const
{
    std::stringstream ss(text);
    JsonValue         value;
    parse(ss, value);
    return value;
}

void
JsonParser::stringify(std::ostream& os, const JsonValue& value) const
{
    _level            = 0;
    std::streamsize p = os.precision();
    os.precision(17);
    writeValue(os, value);
    os.precision(p);
}

std::string
JsonParser::stringify(const JsonValue& value) const
{
    std::stringstream ss;
    stringify(ss, value);
    return ss.str();
}

bool
JsonParser::load(const std::string& fileName, JsonValue& value) const
{
    std::ifstream ifs;
    ifs.open(fileName.c_str(), std::ios::in);
    if (ifs.fail())
        return false;
    parse(ifs, value);
    ifs.close();
    return true;
}

bool
JsonParser::save(const std::string& fileName, const JsonValue& value) const
{
    std::ofstream ofs;
    ofs.open(fileName.c_str(), std::ios::out);
    if (ofs.fail())
        return false;
    stringify(ofs, value);
    ofs.close();
    return true;
}

inline char
JsonParser::getChar(std::istream& is) const
{
    char c;
    is.get(c);
    if (is.fail()) {
        if (is.eof())
            throw ParseException("Unexpected end-of-file found", _line);
        else
            throw ParseException("Fetch error", _line);
    }
    return c;
}

double
JsonParser::getNumber(std::istream& is) const
{
    double num;
    is >> num;
    if (is.fail()) {
        if (is.eof())
            throw ParseException("Unexpected end-of-file found", _line);
        else
            throw ParseException("Fetch error", _line);
    }
    return num;
}

std::string
JsonParser::getString(std::istream& is) const
{
    std::string str;
    char        c = getChar(is);
    while (c != '"') {
        if (c == '\\') {
            c = getChar(is);
            switch (c) {
            case '"': str += '"'; break;
            case '\\': str += '\\'; break;
            case '/': str += '/'; break;
            case 'b': str += '\b'; break;
            case 'f': str += '\f'; break;
            case 'n': str += '\n'; break;
            case 'r': str += '\r'; break;
            case 't': str += '\t'; break;
            default: throw ParseException(std::string("Bad escape sequence in string: '\\") + c + "'", _line);
            }
        }
        else {
            str += c;
        }
        c = getChar(is);
    }
    return str;
}

bool
JsonParser::skipComment(std::istream& is) const
{
    char c = getChar(is); // character next to '/'
    if (c == '*') {       // C-style comment
        c       = getChar(is);
        char cc = '\0';                                 // last character
        while (!(c == '/' && cc == '*')) {              // not '*/'
            if (c == '\r' || (c == '\n' && cc != '\r')) // '\r', '\n', '\r\n'
                _line++;
            cc = c;
            c  = getChar(is);
        }
    }
    else if (c == '/') { // C++-style comment
        do {
            c = getChar(is);
        } while (c != '\r' && c != '\n');
        is.putback(c); // put '\r' or '\n' back
    }
    else { // not valid comment
        is.putback(c);
        return false;
    }
    return true;
}

char
JsonParser::getNonSpaceChar(std::istream& is) const
{
    char c = getChar(is), cc = '\0';
    while (isspace(c) || c == '/') {                // skip white spaces and comments
        if (c == '\r' || (c == '\n' && cc != '\r')) // '\r', '\n', '\r\n'
            _line++;
        if (c == '/')             // comment
            if (!skipComment(is)) // not valid comment
                break;
        cc = c;
        c  = getChar(is);
    }
    return c;
}

bool
JsonParser::matchToken(std::istream& is, char token) const
{
    char c = getNonSpaceChar(is);
    if (c != token) {
        is.putback(c);
        return false;
    }
    return true;
}

bool
JsonParser::match(std::istream& is, const char* pattern, char* gots) const
{
    for (int i = 0; pattern[i] != '\0'; ++i) {
        gots[i] = getChar(is);
        if (gots[i] != pattern[i]) {
            for (int j = i; j >= 0; --j)
                is.putback(gots[j]);
            gots[i + 1] = '\0';
            return false;
        }
    }
    return true;
}

void
JsonParser::readDigit(std::istream& is, JsonValue& value) const
{
    const std::string valid_chars = // valid characters for parsing
      "0123456789abcdef"
      ".+-" /* decimal, sign*/
      "ul"  /* integer suffix*/
      "f"   /* floating point suffix*/
      "e"   /* exponent (scientific) */
      "p"   /* hex exponent (hexadecimal) */
      "x"
      "b";

    // iterate over all digits
    std::string input;
    while (true) {
        char c = std::tolower(getChar(is));
        if (valid_chars.find(c) == std::string::npos) {
            is.putback(c);
            break;
        }
        input += c;
    }
//    std::cout << input << std::endl;

    // make sure the number read is not empty
    if (input.empty()) {
        throw ParseException("Read an empty string while expecting a number", _line);
    }

    // match using regular expression
    const std::string sign  = "[+-]*";
    const std::string f_suf = "([fl]?)";
    const std::string i_suf = "([ul]{0,3})";

    // integer literal
    const std::array<std::pair<std::string, int>, 4> ip = {
        std::make_pair("()([1-9]\\d*)", 10),   // decimal-literal integer-suffix(optional)
        std::make_pair("(0)([0-7]*)", 8),      // octal-literal integer-suffix(optional)
        std::make_pair("(0x)([0-9a-f]*)", 16), // hex-literal integer-suffix(optional)
        std::make_pair("(0b)([01]*)", 2),      // binary-literal integer-suffix(optional)
    };
    for (auto&& p : ip) {
        auto        b = p.second;
        std::smatch m;
        if (std::regex_search(input, m, std::regex("^" + sign + p.first + i_suf + "$"))) {
            auto suffix = m[3].str();
            auto number = m[0].str().substr(0, m[0].length() - suffix.length());
            if (suffix == "ull" || suffix == "llu" || suffix == "lul") {
                auto ull = strtoull(number.c_str(), NULL, b);
                value    = JsonValue((uint64_t)ull);
//                std::cout << "read as uint64_t " << ull << std::endl;
            }
            else if (suffix == "ll" || suffix == "lll") {
                auto ll = strtoll(number.c_str(), NULL, b);
                value   = JsonValue((int64_t)ll);
//                std::cout << "read as int64_t " << ll << std::endl;
            }
            else if (suffix == "uul" || suffix == "ulu" || suffix == "luu" || // 3 letters
                     suffix == "ul" || suffix == "lu" ||                      // 2 letters
                     suffix == "u") {
                auto ul = strtoul(number.c_str(), NULL, b);
                value   = JsonValue((uint32_t)ul);
//                std::cout << "read as uint32_t " << ul << std::endl;
            }
            else { // everything else should be int32_t
                auto l = strtol(number.c_str(), NULL, b);
                value  = JsonValue((int32_t)l);
//                std::cout << "read as int32_t " << l << std::endl;
            }
            return;
        }
    }

    // floating point literal
    const std::array<std::string, 6> fp = {
        "(\\d+)(e[+-]*\\d+)",                  // digits exponent suffix(optional)
        "(\\d+.\\d*)(e[+-]*\\d+)?",            // digits . exponent(optional) suffix(optional)
        "(\\d*.\\d+)(e[+-]*\\d+)?",            // digits . exponent(optional) suffix(optional)
        "0x([\\da-f]+)(p[+-]*\\d+)",           // 0x hex-digits exponent suffix(optional)
        "0x([\\da-f]+.)(p[+-]*\\d+)?",         // 0x hex-digits . exponent suffix(optional)
        "0x([\\da-f]+.[\\da-f]+)(p[+-]*\\d+)?" // 0x hex-digits(optional) . hex-digits exponent suffix(optional)
    };
    for (auto&& p : fp) {
        std::smatch m;
        if (std::regex_search(input, m, std::regex("^" + sign + p + f_suf + "$"))) {
            auto suffix = m[3].str();
            auto number = m[0].str().substr(0, m[0].length() - suffix.length());
            if (suffix == "f") {
                float f = strtof(input.c_str(), NULL);
                value   = JsonValue(f);
//                std::cout << "read as float " << f << std::endl;
            }
            else { // no support for long double
                double d = strtof(input.c_str(), NULL);
                value    = JsonValue(d);
//                std::cout << "read as double " << d << std::endl;
            }
            return;
        }
    }
//    std::cout << "failed \n";
}

void
JsonParser::readArray(std::istream& is, JsonValue& value) const
{
    if (matchToken(is, ']'))
        return;
    while (true) {
        value._data.arr->push_back(std::move(JsonValue()));
        readValue(is, value._data.arr->back());
        char c = getNonSpaceChar(is);
        if (c == ']')
            break;
        if (c != ',')
            throw ParseException("Missing ',' in array declaration", _line);
    }
}

void
JsonParser::readObject(std::istream& is, JsonValue& value) const
{
    if (matchToken(is, '}'))
        return;
    while (true) {
        if (!matchToken(is, '"'))
            throw ParseException("Missing object member name", _line);
        std::string key = getString(is);
        if (!matchToken(is, ':'))
            throw ParseException("Missing ':' after object member name", _line);
        JsonValue val;
        readValue(is, val);
        (*value._data.obj)[key] = std::move(val);
        char c                  = getNonSpaceChar(is);
        if (c == '}')
            break;
        if (c != ',')
            throw ParseException("Missing ',' in object declaration", _line);
    }
}

void
JsonParser::readValue(std::istream& is, JsonValue& value) const
{
    char s[10];
    char c = getNonSpaceChar(is);
    if (c == 'n') {
        if (!match(is, "ull", s))
            throw ParseException(std::string("Expected 'null', got 'n") + std::string(s) + "'", _line);
        value = JsonValue();
    }
    else if (c == 't') {
        if (!match(is, "rue", s))
            throw ParseException(std::string("Expected 'true', got 't") + std::string(s) + "'", _line);
        value = JsonValue(true);
    }
    else if (c == 'f') {
        if (!match(is, "alse", s))
            throw ParseException(std::string("Expected 'false', got 'f") + std::string(s) + "'", _line);
        value = JsonValue(false);
    }
    else if (c == '-' || c == '+' || isdigit(c)) {
        is.putback(c);
        readDigit(is, value);
    }
    else if (c == '"') {
        value = JsonValue(getString(is));
    }
    else if (c == '[') {
        value = JsonValue(JsonValue::ARRAY_TYPE);
        readArray(is, value);
    }
    else if (c == '{') {
        value = JsonValue(JsonValue::OBJECT_TYPE);
        readObject(is, value);
    }
    else {
        throw ParseException(std::string("Expected value, object, or array, got '") + c + "'", _line);
    }
}

void
JsonParser::writeArray(std::ostream& os, const JsonValue& value) const
{
    _level++;
    os << '[' << std::endl;
    for (int i = 0; i < value.size(); ++i) {
        os << std::string(_indent * _level, ' ');
        writeValue(os, value[i]);
        if (i + 1 < value.size())
            os << ',';
        os << std::endl;
    }
    _level--;
    os << std::string(_indent * _level, ' ') << ']';
}

void
JsonParser::writeObject(std::ostream& os, const JsonValue& value) const
{
    _level++;
    os << '{' << std::endl;
    for (auto it = value.toObject().cbegin(); it != value.toObject().cend(); ++it) {
        os << std::string(_indent * _level, ' ');
        os << '"' << it->first << "\" : ";
        writeValue(os, it->second);
        auto next = it;
        if (++next != value.toObject().cend())
            os << ',';
        os << std::endl;
    }
    _level--;
    os << std::string(_indent * _level, ' ') << "}";
}

void
JsonParser::writeValue(std::ostream& os, const JsonValue& value) const
{
    switch (value.type()) {
    case JsonValue::NULL_TYPE: os << "null"; break;
    case JsonValue::BOOL_TYPE: os << (value.toBool() ? "true" : "false"); break;
    case JsonValue::INT8_TYPE: os << value.toInt8(); break;
    case JsonValue::INT16_TYPE: os << value.toInt16(); break;
    case JsonValue::INT32_TYPE: os << value.toInt32(); break;
    case JsonValue::INT64_TYPE: os << value.toInt64(); break;
    case JsonValue::UINT8_TYPE: os << value.toUInt8(); break;
    case JsonValue::UINT16_TYPE: os << value.toUInt16(); break;
    case JsonValue::UINT32_TYPE: os << value.toUInt32(); break;
    case JsonValue::UINT64_TYPE: os << value.toUInt64(); break;
    case JsonValue::FLOAT_TYPE: os << value.toFloat(); break;
    case JsonValue::DOUBLE_TYPE: os << value.toDouble(); break;
    case JsonValue::STRING_TYPE: os << '"' << value.toString() << '"'; break;
    case JsonValue::ARRAY_TYPE: writeArray(os, value); break;
    case JsonValue::OBJECT_TYPE: writeObject(os, value); break;
    default: assert(false);
    }
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Others                                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

std::istream&
operator>>(std::istream& is, JsonValue& value)
{
    JsonParser().parse(is, value);
    return is;
}

std::ostream&
operator<<(std::ostream& os, const JsonValue& value)
{
    JsonParser().stringify(os, value);
    return os;
}

// the iterator type
template class JsonValue::IterType<JsonValue>;
template class JsonValue::IterType<const JsonValue>;

} // namespace serializable
} // namespace vidi

REGISTER_TYPE(json, vidi::serializable::JsonValue, "json");
