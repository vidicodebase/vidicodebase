//
// Created by Qi Wu on 2019-06-30.
//

#ifndef VIDICODEBASE_SERIALIZABLE_FUNCTIONS_H
#define VIDICODEBASE_SERIALIZABLE_FUNCTIONS_H

#include <string>

namespace vidi {
namespace serializable {
template<typename T>
std::string
castPtrToStr(const T* obj)
{
    char str[17];
    sprintf(str, "%p", obj);
    return str;
}

template<typename T>
T*
castStrToPtr(const std::string& address)
{
    T* ptr;
    sscanf(address.c_str(), "%p", &ptr);
    return ptr;
}
} // namespace serializable
} // namespace vidi

#endif // VIDICODEBASE_SERIALIZABLE_FUNCTIONS_H
