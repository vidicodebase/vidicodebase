//
// Created by Qi Wu on 2019-06-26.
//

#ifndef VIDICODEBASE_SERIALIZABLE_JSONSERIALIZATION_H
#define VIDICODEBASE_SERIALIZABLE_JSONSERIALIZATION_H

#include "JsonParser.h"
#include <vidiMath.h>

namespace vidi {
namespace serializable {

template<>
std::string
deserialize<std::string>(const JsonValue& json);
template<>
int8_t
deserialize<int8_t>(const JsonValue& json);
template<>
int16_t
deserialize<int16_t>(const JsonValue& json);
template<>
int32_t
deserialize<int32_t>(const JsonValue& json);
template<>
int64_t
deserialize<int64_t>(const JsonValue& json);
template<>
uint8_t
deserialize<uint8_t>(const JsonValue& json);
template<>
uint16_t
deserialize<uint16_t>(const JsonValue& json);
template<>
uint32_t
deserialize<uint32_t>(const JsonValue& json);
template<>
uint64_t
deserialize<uint64_t>(const JsonValue& json);
template<>
float
deserialize<float>(const JsonValue& json);
template<>
double
deserialize<double>(const JsonValue& json);

template<>
JsonValue
serialize<std::string>(const std::string& object);
template<>
JsonValue
serialize<int8_t>(const int8_t& object);
template<>
JsonValue
serialize<int16_t>(const int16_t& object);
template<>
JsonValue
serialize<int32_t>(const int32_t& object);
template<>
JsonValue
serialize<int64_t>(const int64_t& object);
template<>
JsonValue
serialize<uint8_t>(const uint8_t& object);
template<>
JsonValue
serialize<uint16_t>(const uint16_t& object);
template<>
JsonValue
serialize<uint32_t>(const uint32_t& object);
template<>
JsonValue
serialize<uint64_t>(const uint64_t& object);
template<>
JsonValue
serialize<float>(const float& object);
template<>
JsonValue
serialize<double>(const double& object);

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Vectors                                                                   //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

JsonValue
serializeColor(const vec3f& color);
JsonValue
serializeColor(const vec4f& color);
vec3f
deserializeColor3f(const JsonValue& json);
vec4f
deserializeColor4f(const JsonValue& json);

template<>
JsonValue
serialize(const vec2i& object);
template<>
JsonValue
serialize(const vec2f& object);
template<>
JsonValue
serialize(const vec2d& object);
template<>
JsonValue
serialize(const vec3i& object);
template<>
JsonValue
serialize(const vec3f& object);
template<>
JsonValue
serialize(const vec3d& object);
template<>
JsonValue
serialize(const vec3ia& object);
template<>
JsonValue
serialize(const vec3fa& object);
template<>
JsonValue
serialize(const vec3da& object);
template<>
JsonValue
serialize(const vec4i& object);
template<>
JsonValue
serialize(const vec4f& object);
template<>
JsonValue
serialize(const vec4d& object);

template<>
vec2i
deserialize(const JsonValue& json);
template<>
vec2f
deserialize(const JsonValue& json);
template<>
vec2d
deserialize(const JsonValue& json);
template<>
vec3i
deserialize(const JsonValue& json);
template<>
vec3f
deserialize(const JsonValue& json);
template<>
vec3d
deserialize(const JsonValue& json);
template<>
vec3ia
deserialize(const JsonValue& json);
template<>
vec3fa
deserialize(const JsonValue& json);
template<>
vec3da
deserialize(const JsonValue& json);
template<>
vec4i
deserialize(const JsonValue& json);
template<>
vec4f
deserialize(const JsonValue& json);
template<>
vec4d
deserialize(const JsonValue& json);

template<>
range1i
deserialize(const JsonValue& json);
template<>
range1f
deserialize(const JsonValue& json);
template<>
range1d
deserialize(const JsonValue& json);

template<>
box3i
deserialize(const JsonValue& json);
template<>
box3f
deserialize(const JsonValue& json);
template<>
box3fa
deserialize(const JsonValue& json);
template<>
box3d
deserialize(const JsonValue& json);

} // namespace serializable
} // namespace vidi

#endif // VIDICODEBASE_SERIALIZABLE_JSONSERIALIZATION_H
