//
// Created by qadwu on 5/27/19.
//

#ifndef VIDICODEBASE_SERIALIZABLE_TRAIT_SERIALIZABLE_H
#define VIDICODEBASE_SERIALIZABLE_TRAIT_SERIALIZABLE_H

#include "JsonParser.h"
#include <memory>
#include <string>
#include <vidi_base_export.h>

namespace vidi {
namespace serializable {

/**
 * @brief Object that can be serialized into a Json
 */
class TraitSerializable {
public:
    virtual bool fromJson(const JsonValue& json) noexcept = 0;
    virtual void toJson(JsonValue& json) const noexcept   = 0;
};

} // namespace serializable
} // namespace vidi

#endif // VIDICODEBASE_SERIALIZABLE_TRAIT_SERIALIZABLE_H
