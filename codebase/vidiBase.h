//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_BASE_H
#define VIDICODEBASE_BASE_H

#include "base/Error.h"
#include "base/Log.h"
#include "base/TypeSystem.h"

#endif // VIDICODEBASE_BASE_H
