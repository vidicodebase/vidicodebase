//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//
// clang-format off

#ifndef VIDICODEBASE_PLATFORM_H
#define VIDICODEBASE_PLATFORM_H

////////////////////////////////////////////////////////////////////////////////
/// detect platform
////////////////////////////////////////////////////////////////////////////////

/* detect 32 or 64 platform */
#if defined(__x86_64__) || defined(__ia64__) || defined(_M_X64)
#define __X86_64__
#endif

/* detect Linux platform */
#if defined(linux) || defined(__linux__) || defined(__LINUX__)
#  if !defined(__LINUX__)
#     define __LINUX__
#  endif
#  if !defined(__UNIX__)
#     define __UNIX__
#  endif
#endif

/* detect FreeBSD platform */
#if defined(__FreeBSD__) || defined(__FREEBSD__)
#  if !defined(__FREEBSD__)
#     define __FREEBSD__
#  endif
#  if !defined(__UNIX__)
#     define __UNIX__
#  endif
#endif

/* detect Windows 95/98/NT/2000/XP/Vista/7/8/10 platform */
#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)) && !defined(__CYGWIN__)
#  if !defined(__WIN32__)
#     define __WIN32__
#  endif
#endif

/* detect Cygwin platform */
#if defined(__CYGWIN__)
#  if !defined(__UNIX__)
#     define __UNIX__
#  endif
#endif

/* detect MAC OS X platform */
#if defined(__APPLE__) || defined(MACOSX) || defined(__MACOSX__)
#  if !defined(__MACOSX__)
#     define __MACOSX__
#  endif
#  if !defined(__UNIX__)
#     define __UNIX__
#  endif
#endif

/* try to detect other Unix systems */
#if defined(__unix__) || defined (unix) || defined(__unix) || defined(_unix)
#  if !defined(__UNIX__)
#     define __UNIX__
#  endif
#endif

#if defined (_DEBUG)
#define DEBUG
#endif

////////////////////////////////////////////////////////////////////////////////
/// Macros
////////////////////////////////////////////////////////////////////////////////

#ifdef __WIN32__
#define dll_export __declspec(dllexport)
#define dll_import __declspec(dllimport)
#else
#define dll_export __attribute__ ((visibility ("default")))
#define dll_import 
#endif

#ifdef __WIN32__
#if !defined(__noinline)
#define __noinline             __declspec(noinline)
#endif
//#define __forceinline        // built-in
#if defined(__INTEL_COMPILER)
#define __restrict__           __restrict
#else
#define __restrict__           // causes issues with MSVC
#endif
#if !defined(__thread)
#define __thread               __declspec(thread)
#endif
#if !defined(__aligned)
#define __aligned(...)         __declspec(align(__VA_ARGS__))
#endif
#define debugbreak()           __debugbreak()
#else
#if !defined(__noinline)
#define __noinline             __attribute__((noinline))
#endif
#if !defined(__forceinline)
#define __forceinline          inline __attribute__((always_inline))
#endif
//#define __restrict__         // built-in
//#define __thread             // built-in
#if !defined(__aligned)
#define __aligned(...)           __attribute__((aligned(__VA_ARGS__)))
#endif
#if !defined(__FUNCTION__)
#define __FUNCTION__           __PRETTY_FUNCTION__
#endif
#define debugbreak()           asm("int $3")
#endif

#if defined(__clang__) || defined(__GNUC__)
#define MAYBE_UNUSED __attribute__((unused))
#else
#define MAYBE_UNUSED
#endif

#if defined(_MSC_VER) && (_MSC_VER < 1900) // before VS2015 deleted functions are not supported properly
#define DELETED
#else
#define DELETED  = delete
#endif

#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#define   likely(expr) (expr)
#define unlikely(expr) (expr)
#else
#define   likely(expr) __builtin_expect((bool)(expr),true )
#define unlikely(expr) __builtin_expect((bool)(expr),false)
#endif

#ifndef VIDI_USE_CXX_STD_14
#if defined(__GNUC__) || defined(__clang__)
#define __DEPRECATED__(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
#define __DEPRECATED__(func) __declspec(deprecated) func
#else
#define __DEPRECATED__(func) func // unimplemented
#endif
#else // starting from c++14, [[deprecated]] is introduced
#define __DEPRECATED__(func) [[deprecated]] func 
#endif

#endif // VIDICODEBASE_PLATFORM_H
