//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_BASE_MEMORY_H
#define VIDICODEBASE_BASE_MEMORY_H

#include "base/Error.h"

#include <vidiPlatform.h>
#include <vidiTypeTraits.h>

#include <memory>

namespace vidi {

#ifndef VIDI_USE_CXX_STD_14
template<class T, class... Args>
__forceinline std::unique_ptr<T>
make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
#else
template<class T, class... Args>
__forceinline std::unique_ptr<T>
make_unique(Args&&... args)
{
    return std::make_unique<T>(std::forward<Args>(args)...);
}
#endif

template<typename OT, typename IT>
std::shared_ptr<OT>
dCastTo(std::shared_ptr<IT>& iptr)
{
    auto optr = std::dynamic_pointer_cast<OT>(iptr);
    if (optr) {
        return std::move(optr);
    }
    else {
        throw Exception("[dCastTo] Wrong Parameter Type: \"" + iptr->toString() + "\",",
                        "the typeid of the expecting type is", typeid(OT).name());
    }
}

template<typename OT, typename IT>
std::shared_ptr<OT>
dCastTo(const std::shared_ptr<IT>& iptr)
{
    auto optr = std::dynamic_pointer_cast<OT>(iptr);
    if (optr) {
        return std::move(optr);
    }
    else {
        throw Exception("Wrong Parameter Type: " + iptr->toString());
    }
}

} // namespace vidi

#endif // VIDICODEBASE_BASE_MEMORY_H
