//
// Created by qadwu on 6/2/19.
//

#include "Array.h"
#include <vidiBase.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Explicit Instancing                                                       //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
namespace vidi {
namespace math {
// scalar
template class Array<char, 1>;
template class Array<int8_t, 1>;
template class Array<uint8_t, 1>;
template class Array<int16_t, 1>;
template class Array<uint16_t, 1>;
template class Array<int32_t, 1>;
template class Array<uint32_t, 1>;
template class Array<int64_t, 1>;
template class Array<uint64_t, 1>;
template class Array<float, 1>;
template class Array<double, 1>;
// vec2
template class Array<vec2i, 1>;
template class Array<vec2f, 1>;
template class Array<vec2d, 1>;
// vec3
template class Array<vec3i, 1>;
template class Array<vec3f, 1>;
template class Array<vec3d, 1>;
// vec3 aligned
template class Array<vec3ia, 1>;
template class Array<vec3fa, 1>;
template class Array<vec3da, 1>;
// vec4
template class Array<vec4i, 1>;
template class Array<vec4f, 1>;
template class Array<vec4d, 1>;
} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Others                                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

using std::string;
using vidi::registerType;
using namespace vidi::math;

// define array name as <type>[N]
#define REGISTER_ARRAY(Type, Name, N)                                        \
    namespace vidi {                                                         \
    template<> std::string getTypeName<Array<Type, N>>() noexcept            \
    {                                                                        \
        return "Array<" Name ">[" #N "]";                                    \
    }                                                                        \
    static size_t _##Type##_##N##_typeinfo_r_ = /* NOLINT(cert-err58-cpp) */ \
      registerType("Array<" Name ">[" #N "]", typeid(Array<Type, N>));       \
    }

REGISTER_ARRAY(char, "char", 1);
REGISTER_ARRAY(int8_t, "int8", 1);
REGISTER_ARRAY(uint8_t, "uint8", 1);
REGISTER_ARRAY(int16_t, "int16", 1);
REGISTER_ARRAY(uint16_t, "uint16", 1);
REGISTER_ARRAY(int32_t, "int32", 1);
REGISTER_ARRAY(uint32_t, "uint32", 1);
REGISTER_ARRAY(float, "float", 1);
REGISTER_ARRAY(double, "double", 1);
REGISTER_ARRAY(vec2i, "vec2i", 1);
REGISTER_ARRAY(vec2f, "vec2f", 1);
REGISTER_ARRAY(vec2d, "vec2d", 1);
REGISTER_ARRAY(vec3i, "vec3i", 1);
REGISTER_ARRAY(vec3f, "vec3f", 1);
REGISTER_ARRAY(vec3d, "vec3d", 1);
REGISTER_ARRAY(vec3ia, "vec3ia", 1);
REGISTER_ARRAY(vec3fa, "vec3fa", 1);
REGISTER_ARRAY(vec3da, "vec3da", 1);
REGISTER_ARRAY(vec4i, "vec4i", 1);
REGISTER_ARRAY(vec4f, "vec4f", 1);
REGISTER_ARRAY(vec4d, "vec4d", 1);
REGISTER_ARRAY(string, "string", 1);

REGISTER_ARRAY(int32_t, "int32", 3);
REGISTER_ARRAY(float, "float", 3);
REGISTER_ARRAY(double, "double", 3);

namespace type {
static bool _0 = /* NOLINT(cert-err58-cpp) */
  registerType("Array<uchar>[1]", typeid(Array<unsigned char, 1>));
static bool _1 = /* NOLINT(cert-err58-cpp) */
  registerType("Array<int>[1]", typeid(Array<int, 1>));
static bool _2 = /* NOLINT(cert-err58-cpp) */
  registerType("Array<uint>[1]", typeid(Array<unsigned int, 1>));
} // namespace type
