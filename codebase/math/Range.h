//
// Created by qadwu on 5/30/19.
//

#ifndef VIDICODEBASE_MATH_RANGE_H
#define VIDICODEBASE_MATH_RANGE_H

#include "Basic.h"
#include <stdint.h>
#include <vidi_base_export.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Forward Declarations                                                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {
template<typename T> class Range;
using range1i = Range<int32_t>;
using range1f = Range<float>;
using range1d = Range<double>;
} // namespace math
} // namespace vidi

namespace vidi {
using math::range1d;
using math::range1f;
using math::range1i;
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Actual Definitions Start from Here                                        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

/*! default implementation of 'anyLessThan' for scalar types, so we
    can make a range<float>s etc. Vec-types will overwrite that and
    test if _any_ dimension is less */
template<typename TA, typename TB>
inline bool
anyLessThan(const TA& a, const TB& b)
{
    return a < b;
}

namespace { // this should be equivalent to operator==
template<typename TA, typename TB>
inline bool
allEqual(const TA& a, const TB& b)
{
    return (!anyLessThan(a, b)) && (!anyLessThan(b, a));
}
} // namespace

template<typename T> class Range {
public:
    Range() : _min(), _max() {}
    Range(const T& min, const T& max)
    {
        setExtents(min, max);
    }

    const T& minimum() const
    {
        return _min;
    }
    const T& maximum() const
    {
        return _max;
    }
    T& minimum()
    {
        return _min;
    }
    T& maximum()
    {
        return _max;
    }

    void setExtents(const T& min, const T& max)
    {
        if (anyLessThan(max, min)) {
            _min = max;
            _max = min;
        }
        else {
            _min = min;
            _max = max;
        }
    }

    T center() const
    {
        return ((_min + _max) * T(.5));
    }
    T size() const
    {
        return (_max - _min);
    }

    bool contains(const T& point) const
    {
        return !anyLessThan(point, _min) && !anyLessThan(_max, point);
    }

    bool contains(const Range<T>& box) const
    {
        return !anyLessThan(box._min, _min) && !anyLessThan(_max, box._max);
    }

    void extend(const Range<T>& other)
    {
        if (anyLessThan(other._min, _min))
            _min = other._min;
        if (anyLessThan(_max, other._max))
            _max = other._max;
    }

private:
    T _min;
    T _max;
};

template<typename T>
inline bool
operator==(const Range<T>& lhs, const Range<T>& rhs)
{
    return allEqual(lhs.minimum(), rhs.minimum()) && allEqual(rhs.maximum(), lhs.maximum());
}

} // namespace math
} // namespace vidi

#endif // VIDICODEBASE_MATH_RANGE_H
