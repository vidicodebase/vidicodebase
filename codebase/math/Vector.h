//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_MATH_VECTOR_H
#define VIDICODEBASE_MATH_VECTOR_H

#include "Range.h"
#include <cmath>
#include <iostream>
#include <memory>
#include <type_traits>
#include <vidiPlatform.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Macro Definitions Goes Here                                               //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Forward Declarations                                                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T, int N, bool ALIGN = false> struct Vector {
};
template<typename T> using Vector2  = Vector<T, 2, false>;
template<typename T> using Vector3  = Vector<T, 3, false>;
template<typename T> using Vector3a = Vector<T, 3, true>;
template<typename T> using Vector4  = Vector<T, 4, false>;

using vec2b = Vector2<bool>;
using vec2i = Vector2<int>;
using vec2f = Vector2<float>;
using vec2d = Vector2<double>;

using vec3b = Vector3<bool>;
using vec3i = Vector3<int>;
using vec3f = Vector3<float>;
using vec3d = Vector3<double>;

using vec3ba = Vector3a<bool>;
using vec3ia = Vector3a<int>;
using vec3fa = Vector3a<float>;
using vec3da = Vector3a<double>;

using vec4b = Vector4<bool>;
using vec4i = Vector4<int>;
using vec4f = Vector4<float>;
using vec4d = Vector4<double>;

__DEPRECATED__(typedef math::Vector2<bool> bvec2);
__DEPRECATED__(typedef math::Vector2<int> ivec2);
__DEPRECATED__(typedef math::Vector2<float> vec2);
__DEPRECATED__(typedef math::Vector2<double> dvec2);
__DEPRECATED__(typedef math::Vector3<bool> bvec3);
__DEPRECATED__(typedef math::Vector3<int> ivec3);
__DEPRECATED__(typedef math::Vector3<float> vec3);
__DEPRECATED__(typedef math::Vector3<double> dvec3);
__DEPRECATED__(typedef math::Vector4<bool> bvec4);
__DEPRECATED__(typedef math::Vector4<int> ivec4);
__DEPRECATED__(typedef math::Vector4<float> vec4);
__DEPRECATED__(typedef math::Vector4<double> dvec4);

} // namespace math
} // namespace vidi

namespace vidi {
using math::vec2b;
using math::vec2d;
using math::vec2f;
using math::vec2i;
using math::vec3b;
using math::vec3ba;
using math::vec3d;
using math::vec3da;
using math::vec3f;
using math::vec3fa;
using math::vec3i;
using math::vec3ia;
using math::vec4b;
using math::vec4d;
using math::vec4f;
using math::vec4i;
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Actual Definitions Start from Here                                        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Vector2                                                                   //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T> struct Vector<T, 2> {
public:
    union {
        T x, r, s;
    };
    union {
        T y, g, t;
    };

public:
    Vector() : x(T(0)), y(T(0)) {}

    template<typename _T> explicit Vector(_T s) : x(T(s)), y(T(s)) {}

    template<typename _T1, typename _T2> Vector(_T1 _x, _T2 _y) : x(T(_x)), y(T(_y)) {}

    template<typename _T> Vector(const Vector2<_T>& v) : x(T(v.x)), y(T(v.y)) {}

    template<typename _T, bool _A> Vector(const Vector<_T, 3, _A>& v) : x(T(v.x)), y(T(v.y)) {}

    template<typename _T> Vector(const Vector4<_T>& v) : x(T(v.x)), y(T(v.y)) {}

    explicit Vector(const T* v, int offset = 1) : x(v[0]), y(v[offset]) {}

    T& operator[](int i)
    {
        return (&x)[i];
    }
    const T& operator[](int i) const
    {
        return (&x)[i];
    }
    operator T*()
    {
        return &x;
    }
    operator const T*() const
    {
        return &x;
    }

    Vector2<T>& operator*=(T s)
    {
        x *= s;
        y *= s;
        return *this;
    }
    Vector2<T>& operator*=(const Vector2<T>& v)
    {
        x *= v.x;
        y *= v.y;
        return *this;
    }
    Vector2<T>& operator+=(const Vector2<T>& v)
    {
        x += v.x;
        y += v.y;
        return *this;
    }
    Vector2<T>& operator-=(const Vector2<T>& v)
    {
        x -= v.x;
        y -= v.y;
        return *this;
    }
    Vector2<T>& operator/=(T s)
    {
        x /= s;
        y /= s;
        return *this;
    }
    Vector2<T>& operator/=(const Vector2<T>& v)
    {
        x /= v.x;
        y /= v.y;
        return *this;
    }
};

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Vector3                                                                   //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T> struct Vector<T, 3, false> {
public:
    union {
        T x, r, s;
    };
    union {
        T y, g, t;
    };
    union {
        T z, b, p;
    };

public:
    Vector() : x(T(0)), y(T(0)), z(T(0)) {}

    template<typename _T> explicit Vector(_T s) : x(T(s)), y(T(s)), z(T(s)) {}

    template<typename _T1, typename _T2, typename _T3> Vector(_T1 _x, _T2 _y, _T3 _z) : x(T(_x)), y(T(_y)), z(T(_z)) {}

    template<typename _T1, typename _T2> Vector(const Vector2<_T1>& v, _T2 _z) : x(T(v.x)), y(T(v.y)), z(T(_z)) {}

    template<typename _T> Vector(const Vector3<_T>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z)) {}

    template<typename _T> // we can convert from aligned to non-aligned
    explicit Vector(const Vector3a<_T>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z))
    {
    }

    template<typename _T> Vector(const Vector4<_T>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z)) {}

    explicit Vector(const T* v, int offset = 1) : x(v[0]), y(v[offset]), z(v[offset * 2]) {}

    T& operator[](int i)
    {
        return (&x)[i];
    }
    const T& operator[](int i) const
    {
        return (&x)[i];
    }
    operator T*()
    {
        return &x;
    }
    operator const T*() const
    {
        return &x;
    }

    Vector3<T>& operator*=(T s)
    {
        x *= s;
        y *= s;
        z *= s;
        return *this;
    }
    Vector3<T>& operator*=(const Vector3<T>& v)
    {
        x *= v.x;
        y *= v.y;
        z *= v.z;
        return *this;
    }
    Vector3<T>& operator+=(const Vector3<T>& v)
    {
        x += v.x;
        y += v.y;
        z += v.z;
        return *this;
    }
    Vector3<T>& operator-=(const Vector3<T>& v)
    {
        x -= v.x;
        y -= v.y;
        z -= v.z;
        return *this;
    }
    Vector3<T>& operator/=(T s)
    {
        x /= s;
        y /= s;
        z /= s;
        return *this;
    }
    Vector3<T>& operator/=(const Vector3<T>& v)
    {
        x /= v.x;
        y /= v.y;
        z /= v.z;
        return *this;
    }
};

// memory aligned version
template<typename T> struct Vector<T, 3, true> {
public:
    union {
        T x, r, s;
    };
    union {
        T y, g, t;
    };
    union {
        T z, b, p;
    };
    T padding_;

public:
    Vector() : x(T(0)), y(T(0)), z(T(0)) {}

    template<typename _T> explicit Vector(_T s) : x(T(s)), y(T(s)), z(T(s)) {}

    template<typename _T1, typename _T2, typename _T3> Vector(_T1 _x, _T2 _y, _T3 _z) : x(T(_x)), y(T(_y)), z(T(_z)) {}

    template<typename _T1, typename _T2> Vector(const Vector2<_T1>& v, _T2 _z) : x(T(v.x)), y(T(v.y)), z(T(_z)) {}

    template<typename _T> Vector(const Vector3a<_T>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z)) {}

    template<typename _T> // we can convert
    explicit Vector(const Vector3<_T>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z))
    {
    }

    template<typename _T> Vector(const Vector4<_T>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z)) {}

    explicit Vector(const T* v, int offset = 1) : x(v[0]), y(v[offset]), z(v[offset * 2]) {}

    T& operator[](int i)
    {
        return (&x)[i];
    }
    const T& operator[](int i) const
    {
        return (&x)[i];
    }
    operator T*()
    {
        return &x;
    }
    operator const T*() const
    {
        return &x;
    }

    Vector3a<T>& operator*=(T s)
    {
        x *= s;
        y *= s;
        z *= s;
        return *this;
    }
    Vector3a<T>& operator*=(const Vector3a<T>& v)
    {
        x *= v.x;
        y *= v.y;
        z *= v.z;
        return *this;
    }
    Vector3a<T>& operator+=(const Vector3a<T>& v)
    {
        x += v.x;
        y += v.y;
        z += v.z;
        return *this;
    }
    Vector3a<T>& operator-=(const Vector3a<T>& v)
    {
        x -= v.x;
        y -= v.y;
        z -= v.z;
        return *this;
    }
    Vector3a<T>& operator/=(T s)
    {
        x /= s;
        y /= s;
        z /= s;
        return *this;
    }
    Vector3a<T>& operator/=(const Vector3a<T>& v)
    {
        x /= v.x;
        y /= v.y;
        z /= v.z;
        return *this;
    }
};

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Vector4                                                                   //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T> struct Vector<T, 4> {
public:
    union {
        T x, r, s;
    };
    union {
        T y, g, t;
    };
    union {
        T z, b, p;
    };
    union {
        T w, a, q;
    };

public:
    Vector() : x(T(0)), y(T(0)), z(T(0)), w(T(0)) {}

    template<typename _T> explicit Vector(_T s) : x(T(s)), y(T(s)), z(T(s)), w(T(s)) {}

    template<typename _T1, typename _T2, typename _T3, typename _T4>
    Vector(_T1 _x, _T2 _y, _T3 _z, _T4 _w) : x(T(_x)), y(T(_y)), z(T(_z)), w(T(_w))
    {
    }

    template<typename _T1, typename _T2, typename _T3>
    Vector(const Vector2<_T1>& v, _T2 _z, _T3 _w) : x(T(v.x)), y(T(v.y)), z(T(_z)), w(T(_w))
    {
    }

    template<typename _T1, typename _T2, bool _A>
    Vector(const Vector<_T1, 3, _A>& v, _T2 _w) : x(T(v.x)), y(T(v.y)), z(T(v.z)), w(T(_w))
    {
    }

    template<typename _T> Vector(const Vector4<_T>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z)), w(T(v.w)) {}

    explicit Vector(const T* v, int offset = 1) : x(v[0]), y(v[offset]), z(v[offset * 2]), w(v[offset * 3]) {}

    T& operator[](int i)
    {
        return (&x)[i];
    }
    const T& operator[](int i) const
    {
        return (&x)[i];
    }
    operator T*()
    {
        return &x;
    }
    operator const T*() const
    {
        return &x;
    }

    Vector4<T>& operator*=(T s)
    {
        x *= s;
        y *= s;
        z *= s;
        w *= s;
        return *this;
    }
    Vector4<T>& operator*=(const Vector4<T>& v)
    {
        x *= v.x;
        y *= v.y;
        z *= v.z;
        w *= v.w;
        return *this;
    }
    Vector4<T>& operator+=(const Vector4<T>& v)
    {
        x += v.x;
        y += v.y;
        z += v.z;
        w += v.w;
        return *this;
    }
    Vector4<T>& operator-=(const Vector4<T>& v)
    {
        x -= v.x;
        y -= v.y;
        z -= v.z;
        w -= v.w;
        return *this;
    }
    Vector4<T>& operator/=(T s)
    {
        x /= s;
        y /= s;
        z /= s;
        w /= s;
        return *this;
    }
    Vector4<T>& operator/=(const Vector4<T>& v)
    {
        x /= v.x;
        y /= v.y;
        z /= v.z;
        w /= v.w;
        return *this;
    }
};

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Other Operators                                                           //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

// vec2
template<typename T> inline Vector2<T> operator*(T s, const Vector2<T>& v)
{
    return (v * s);
}
template<typename T> inline Vector2<T> operator*(const Vector2<T>& v, T s)
{
    return Vector2<T>(v.x * s, v.y * s);
}
template<typename T> inline Vector2<T> operator*(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return Vector2<T>(v1.x * v2.x, v1.y * v2.y);
}
template<typename T>
inline Vector2<T>
operator+(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return Vector2<T>(v1.x + v2.x, v1.y + v2.y);
}
template<typename T>
inline Vector2<T>
operator-(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return Vector2<T>(v1.x - v2.x, v1.y - v2.y);
}
template<typename T>
inline Vector2<T>
operator-(const Vector2<T>& v)
{
    return Vector2<T>(-v.x, -v.y);
}
template<typename T>
inline Vector2<T>
operator/(const Vector2<T>& v, T s)
{
    return Vector2<T>(v.x / s, v.y / s);
}
template<typename T>
inline Vector2<T>
operator/(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return Vector2<T>(v1.x / v2.x, v1.y / v2.y);
}
template<typename T>
inline bool
operator==(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return (v1.x == v2.x && v1.y == v2.y);
}
template<typename T>
inline bool
operator!=(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return !(v1 == v2);
}

// vec3
template<typename T, bool A> inline Vector<T, 3, A> operator*(T s, const Vector<T, 3, A>& v)
{
    return (v * s);
}
template<typename T, bool A> inline Vector<T, 3, A> operator*(const Vector<T, 3, A>& v, T s)
{
    return Vector<T, 3, A>(v.x * s, v.y * s, v.z * s);
}
template<typename T, bool A> inline Vector<T, 3, A> operator*(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return Vector<T, 3, A>(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}
template<typename T, bool A>
inline Vector<T, 3, A>
operator+(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return Vector<T, 3, A>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}
template<typename T, bool A>
inline Vector<T, 3, A>
operator-(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return Vector<T, 3, A>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}
template<typename T, bool A>
inline Vector<T, 3, A>
operator-(const Vector<T, 3, A>& v)
{
    return Vector<T, 3, A>(-v.x, -v.y, -v.z);
}
template<typename T, bool A>
inline Vector<T, 3, A>
operator/(const Vector<T, 3, A>& v, T s)
{
    return Vector<T, 3, A>(v.x / s, v.y / s, v.z / s);
}
template<typename T, bool A>
inline Vector<T, 3, A>
operator/(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return Vector<T, 3, A>(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}
template<typename T, bool A>
inline bool
operator==(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
}
template<typename T, bool A>
inline bool
operator!=(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return !(v1 == v2);
}

// vec4
template<typename T> inline Vector4<T> operator*(T s, const Vector4<T>& v)
{
    return (v * s);
}
template<typename T> inline Vector4<T> operator*(const Vector4<T>& v, T s)
{
    return Vector4<T>(v.x * s, v.y * s, v.z * s, v.w * s);
}
template<typename T> inline Vector4<T> operator*(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return Vector4<T>(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
}
template<typename T>
inline Vector4<T>
operator+(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return Vector4<T>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
}
template<typename T>
inline Vector4<T>
operator-(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return Vector4<T>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
}
template<typename T>
inline Vector4<T>
operator-(const Vector4<T>& v)
{
    return Vector4<T>(-v.x, -v.y, -v.z, -v.w);
}
template<typename T>
inline Vector4<T>
operator/(const Vector4<T>& v, T s)
{
    return Vector4<T>(v.x / s, v.y / s, v.z / s, v.w / s);
}
template<typename T>
inline Vector4<T>
operator/(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return Vector4<T>(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
}
template<typename T>
inline bool
operator==(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
}
template<typename T>
inline bool
operator!=(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return !(v1 == v2);
}

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Min/Max/Clamp and Reductions                                              //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

// scalar
template<typename T>
inline const T&
min(const T& x, const T& y)
{
    return !(y < x) ? x : y;
}
template<typename T>
inline const T&
max(const T& x, const T& y)
{
    return (x < y) ? y : x;
}
template<typename T>
inline T
clamp(const T& x, const T& minVal, const T& maxVal)
{
    return min(max(x, minVal), maxVal);
}

// vec2
template<typename T>
inline Vector2<T>
min(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return Vector2<T>(min(v1.x, v2.x), min(v1.y, v2.y));
}
template<typename T>
inline Vector2<T>
max(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return Vector2<T>(max(v1.x, v2.x), max(v1.y, v2.y));
}
template<typename T>
inline Vector2<T>
clamp(const Vector2<T>& x, T minVal, T maxVal)
{
    return Vector2<T>(clamp(x.x, minVal, maxVal), clamp(x.y, minVal, maxVal));
}

// vec3
template<typename T, bool A>
inline Vector<T, 3, A>
min(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return Vector<T, 3, A>(min(v1.x, v2.x), min(v1.y, v2.y), min(v1.z, v2.z));
}
template<typename T, bool A>
inline Vector<T, 3, A>
max(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return Vector<T, 3, A>(max(v1.x, v2.x), max(v1.y, v2.y), max(v1.z, v2.z));
}
template<typename T, bool A>
inline Vector<T, 3, A>
clamp(const Vector<T, 3, A>& x, T minVal, T maxVal)
{
    return Vector<T, 3, A>(clamp(x.x, minVal, maxVal), clamp(x.y, minVal, maxVal), clamp(x.z, minVal, maxVal));
}

// vec4
template<typename T>
inline Vector4<T>
min(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return Vector4<T>(min(v1.x, v2.x), min(v1.y, v2.y), min(v1.z, v2.z), min(v1.w, v2.w));
}
template<typename T>
inline Vector4<T>
max(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return Vector4<T>(max(v1.x, v2.x), max(v1.y, v2.y), max(v1.z, v2.z), max(v1.w, v2.w));
}
template<typename T>
inline Vector4<T>
clamp(const Vector4<T>& x, T minVal, T maxVal)
{
    return Vector4<T>(clamp(x.x, minVal, maxVal), clamp(x.y, minVal, maxVal), clamp(x.z, minVal, maxVal),
                      clamp(x.w, minVal, maxVal));
}

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Linear Algebra Functions                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

// cross
template<typename T, bool A, typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline Vector<T, 3, A>
cross(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return Vector<T, 3, A>(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
}

// dot
template<typename T, typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline T
dot(const Vector2<T>& v1, const Vector2<T>& v2)
{
    return (v1.x * v2.x + v1.y * v2.y);
}
template<typename T, bool A, typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline T
dot(const Vector<T, 3, A>& v1, const Vector<T, 3, A>& v2)
{
    return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}
template<typename T, typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline T
dot(const Vector4<T>& v1, const Vector4<T>& v2)
{
    return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w);
}

template<template<typename, int, bool> class VecType,
         typename T,
         int N,
         typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline T
length(const VecType<T, N, false>& v)
{
    return sqrt(dot(v, v));
}
template<typename T, typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline T
length(const Vector3a<T>& v)
{
    return sqrt(dot(v, v));
}

template<template<typename, int, bool> class VecType,
         typename T,
         int N,
         typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline VecType<T, N, false>
normalize(const VecType<T, N, false>& v)
{
    T len    = length(v);
    T invLen = (len == T(0.0)) ? T(0.0) : (T(1.0) / len);
    return (v * invLen);
}
template<typename T, typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline Vector3a<T>
normalize(const Vector3a<T>& v)
{
    T len    = length(v);
    T invLen = (len == T(0.0)) ? T(0.0) : (T(1.0) / len);
    return (v * invLen);
}

// scalar & vec
template<typename T>
inline T
degrees(const T& radians)
{
    return (radians * T(180.0) / T(M_PI));
}
template<typename T>
inline T
radians(const T& degrees)
{
    return (degrees * T(M_PI) / T(180.0));
}

template<typename T1, typename T2>
inline T1
mix(const T1& x, const T1& y, const T2& a)
{
    return (x + a * (y - x));
}
template<typename T1, typename T2>
inline T1
lerp(const T1& x, const T1& y, const T2& a)
{
    return (x + a * (y - x));
}

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Generic Comparisons                                                       //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

// generic operators
template<typename T>
inline bool
_lt_(const Vector2<T>& a, const Vector2<T>& b)
{
    return a.x < b.x || a.y < b.y;
}

template<typename T, bool A>
inline bool
_lt_(const Vector<T, 3, A>& a, const Vector<T, 3, A>& b)
{
    return a.x < b.x || a.y < b.y || a.z < b.z;
}

template<typename T>
inline bool
_lt_(const Vector4<T>& a, const Vector4<T>& b)
{
    return a.x < b.x || a.y < b.y || a.z < b.z || a.w < b.w;
}

template<template<typename, int> class VecType,
         typename T,
         int N,
         typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
inline bool
_eq_(const VecType<T, N>& a, const VecType<T, N>& b)
{
    return a == b;
}

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  iostream                                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

// iostream
template<typename T>
inline std::ostream&
operator<<(std::ostream& os, const Vector2<T>& v)
{
    os << "(" << v.x << ", " << v.y << ")";
    return os;
}

template<typename T, bool A>
inline std::ostream&
operator<<(std::ostream& os, const Vector<T, 3, A>& v)
{
    os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
    return os;
}

template<typename T>
inline std::ostream&
operator<<(std::ostream& os, const Vector4<T>& v)
{
    os << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
    return os;
}

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Others                                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

// hash functions
namespace {
template<class T>
inline void
hash_combine(std::size_t& seed, const T& v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}
} // namespace

namespace std {

template<typename T> struct hash<vidi::math::Vector<T, 2>> {
    inline size_t operator()(const vidi::math::Vector<T, 2>& v) const
    {
        size_t seed = 0;
        hash_combine(seed, v.x);
        hash_combine(seed, v.y);
        return seed;
    }
};

template<typename T, bool A> struct hash<vidi::math::Vector<T, 3, A>> {
    inline size_t operator()(const vidi::math::Vector<T, 3, A>& v) const
    {
        size_t seed = 0;
        hash_combine(seed, v.x);
        hash_combine(seed, v.y);
        hash_combine(seed, v.z);
        return seed;
    }
};

template<typename T> struct hash<vidi::math::Vector<T, 4>> {
    inline size_t operator()(const vidi::math::Vector<T, 4>& v) const
    {
        size_t seed = 0;
        hash_combine(seed, v.x);
        hash_combine(seed, v.y);
        hash_combine(seed, v.z);
        hash_combine(seed, v.w);
        return seed;
    }
};

/*! template specialization for std::less comparison operator;
 *  we need those to be able to put vec's in std::map etc @{ */
/* Defining just operator< is prone to bugs, because a definition of an
 * ordering of vectors is a bit arbitrary and depends on the context.
 * For example, in box::extend we certainly want the element-wise min/max and
 * not the std::min/std::max made applicable by vec3f::operator<.
 */
template<typename T> struct less<vidi::math::Vector<T, 2>> {
    inline bool operator()(const vidi::math::Vector<T, 2>& a, const vidi::math::Vector<T, 2>& b) const
    {
        return (a.x < b.x) || ((a.x == b.x) && (a.y < b.y));
    }
};

template<typename T, bool A> struct less<vidi::math::Vector<T, 3, A>> {
    inline bool operator()(const vidi::math::Vector<T, 3>& a, const vidi::math::Vector<T, 3>& b) const
    {
        return (a.x < b.x) || ((a.x == b.x) && ((a.y < b.y) || ((a.y == b.y) && (a.z < b.z))));
    }
};

template<typename T> struct less<vidi::math::Vector<T, 4>> {
    inline bool operator()(const vidi::math::Vector<T, 4>& a, const vidi::math::Vector<T, 4>& b) const
    {
        return (a.x < b.x) ||
               ((a.x == b.x) && ((a.y < b.y) || ((a.y == b.y) && ((a.z < b.z) || ((a.z == b.z) && (a.w < b.w))))));
    }
};

} // namespace std

#endif // VIDICODEBASE_MATH_VECTOR_H
