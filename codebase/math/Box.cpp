//
// Created by qadwu on 6/1/19.
//

#include "Box.h"
#include <vidiBase.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Explicit Instantiation                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template class Range<Vector<int32_t, 3, false>>;
template class Range<Vector<int32_t, 3, true>>;
template class Range<Vector<float, 3, false>>;
template class Range<Vector<float, 3, true>>;
template class Range<Vector<double, 3, false>>;
template class Range<Vector<double, 3, true>>;

} // namespace math
} // namespace vidi

REGISTER_TYPE_SIMPLE(0, box3i);
REGISTER_TYPE_SIMPLE(1, box3f);
REGISTER_TYPE_SIMPLE(2, box3d);
