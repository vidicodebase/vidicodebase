//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_MATH_MATRIX_H
#define VIDICODEBASE_MATH_MATRIX_H

#include "Vector.h"
#include <cassert>
#include <cmath>
#include <vidi_base_export.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Forward Declarations                                                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename> struct Matrix3x3;
template<typename> struct Matrix4x4;

__DEPRECATED__(typedef Matrix3x3<float> mat3);
__DEPRECATED__(typedef Matrix3x3<double> dmat3);
__DEPRECATED__(typedef Matrix4x4<float> mat4);
__DEPRECATED__(typedef Matrix4x4<double> dmat4);

using mat3f = Matrix3x3<float>;
using mat3d = Matrix3x3<double>;
using mat4f = Matrix4x4<float>;
using mat4d = Matrix4x4<double>;

} // namespace math
} // namespace vidi

namespace vidi {
using math::mat3d;
using math::mat3f;
using math::mat4d;
using math::mat4f;
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Matrix 3x3                                                                //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

/**
 *  Matrix3x3
 */
template<typename T> struct Matrix3x3 {
public:
    T m[3][3]; // colume-major order

public:
    Matrix3x3();

    template<typename _T> explicit Matrix3x3(_T s);

    template<typename _T>
    Matrix3x3(_T m11,
              _T m21,
              _T m31, // colomn-major order
              _T m12,
              _T m22,
              _T m32,
              _T m13,
              _T m23,
              _T m33);

    template<typename _T, bool _A>
    Matrix3x3(const Vector<_T, 3, _A>& column1, const Vector<_T, 3, _A>& column2, const Vector<_T, 3, _A>& column3);

    template<typename _T> explicit Matrix3x3(const Matrix3x3<_T>& other);

    template<typename _T> explicit Matrix3x3(const Matrix4x4<_T>& other);

    const T& operator()(int row, int column) const
    {
        assert(row >= 0 && row < 3 && column >= 0 && column < 3);
        return m[column][row];
    }
    T& operator()(int row, int column)
    {
        assert(row >= 0 && row < 3 && column >= 0 && column < 3);
        return m[column][row];
    }
    const T* operator[](int index) const
    {
        return m[index];
    }
    T* operator[](int index)
    {
        return m[index];
    }

    Matrix3x3<T>& operator*=(const Matrix3x3<T>& other);
    Matrix3x3<T>& operator*=(T factor);
    Matrix3x3<T>& operator+=(const Matrix3x3<T>& other);
    Matrix3x3<T>& operator-=(const Matrix3x3<T>& other);
    Matrix3x3<T>& operator/=(T divisor);
};

template<typename T> Matrix3x3<T>            operator*(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2);
template<typename T, bool A> Vector<T, 3, A> operator*(const Vector<T, 3, A>& v, const Matrix3x3<T>& m);
template<typename T, bool A> Vector<T, 3, A> operator*(const Matrix3x3<T>& m, const Vector<T, 3, A>& v);
template<typename T> Matrix3x3<T>            operator*(T factor, const Matrix3x3<T>& m);
template<typename T> Matrix3x3<T>            operator*(const Matrix3x3<T>& m, T factor);
template<typename T>
Matrix3x3<T>
operator+(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2);
template<typename T>
Matrix3x3<T>
operator-(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2);
template<typename T>
Matrix3x3<T>
operator-(const Matrix3x3<T>& m);
template<typename T>
Matrix3x3<T>
operator/(const Matrix3x3<T>& m, T divisor);
template<typename T>
bool
operator==(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2);
template<typename T>
bool
operator!=(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2);

template<typename T>
Matrix3x3<T>
inverse(const Matrix3x3<T>& m, bool* invertible = nullptr);
template<typename T>
Matrix3x3<T>
transpose(const Matrix3x3<T>& m);

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Matrix 4x4                                                                //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

/**
 *  Matrix4x4
 */
template<typename T> struct Matrix4x4 {
public:
    T m[4][4]; // column-major order

public:
    Matrix4x4();

    template<typename _T> explicit Matrix4x4(_T s);

    template<typename _T>
    Matrix4x4(_T m11,
              _T m21,
              _T m31,
              _T m41, // colomn-major order
              _T m12,
              _T m22,
              _T m32,
              _T m42,
              _T m13,
              _T m23,
              _T m33,
              _T m43,
              _T m14,
              _T m24,
              _T m34,
              _T m44);

    template<typename _T>
    Matrix4x4(const Vector4<_T>& column1,
              const Vector4<_T>& column2,
              const Vector4<_T>& column3,
              const Vector4<_T>& column4);

    template<typename _T> explicit Matrix4x4(const Matrix3x3<_T>& m);

    template<typename _T> explicit Matrix4x4(const Matrix4x4<_T>& other);

    const T& operator()(int row, int column) const
    {
        assert(row >= 0 && row < 4 && column >= 0 && column < 4);
        return m[column][row];
    }
    T& operator()(int row, int column)
    {
        assert(row >= 0 && row < 4 && column >= 0 && column < 4);
        return m[column][row];
    }
    const T* operator[](int index) const
    {
        return m[index];
    }
    T* operator[](int index)
    {
        return m[index];
    }

    operator const T*() const
    {
        return (const T*)&m[0][0];
    }
    operator T*()
    {
        return (T*)(&m[0][0]);
    }

    // transformation matrices
    static Matrix4x4<T> fromFrustum(T left, T right, T bottom, T top, T zNear, T zFar);
    template<bool _A>
    static Matrix4x4<T> fromLookAt(const Vector<T, 3, _A>& eye,
                                   const Vector<T, 3, _A>& center,
                                   const Vector<T, 3, _A>& up);
    static Matrix4x4<T> fromLookAt(T eyex, T eyey, T eyez, T centerx, T centery, T centerz, T upx, T upy, T upz);
    static Matrix4x4<T> fromOrtho(T left, T right, T bottom, T top, T zNear, T zFar);
    static Matrix4x4<T> fromPerspective(T fovy, T aspect, T zNear, T zFar);
    template<bool _A> static Matrix4x4<T> fromRotate(T angle, const Vector<T, 3, _A>& vector);
    static Matrix4x4<T>                   fromRotate(T angle, T x, T y, T z);
    template<bool _A> static Matrix4x4<T> fromScale(const Vector<T, 3, _A>& vector);
    static Matrix4x4<T>                   fromScale(T x, T y, T z);
    template<bool _A> static Matrix4x4<T> fromTranslate(const Vector<T, 3, _A>& vector);
    static Matrix4x4<T>                   fromTranslate(T x, T y, T z);

    Matrix4x4<T>& operator*=(const Matrix4x4<T>& other);
    Matrix4x4<T>& operator*=(T factor);
    Matrix4x4<T>& operator+=(const Matrix4x4<T>& other);
    Matrix4x4<T>& operator-=(const Matrix4x4<T>& other);
    Matrix4x4<T>& operator/=(T divisor);
};

template<typename T> Matrix4x4<T>            operator*(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2);
template<typename T, bool A> Vector<T, 3, A> operator*(const Vector<T, 3, A>& v, const Matrix4x4<T>& m);
template<typename T, bool A> Vector<T, 3, A> operator*(const Matrix4x4<T>& m, const Vector<T, 3, A>& v);
template<typename T> Vector4<T>              operator*(const Vector4<T>& v, const Matrix4x4<T>& m);
template<typename T> Vector4<T>              operator*(const Matrix4x4<T>& m, const Vector4<T>& v);
template<typename T> Matrix4x4<T>            operator*(T factor, const Matrix4x4<T>& m);
template<typename T> Matrix4x4<T>            operator*(const Matrix4x4<T>& m, T factor);
template<typename T>
Matrix4x4<T>
operator+(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2);
template<typename T>
Matrix4x4<T>
operator-(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2);
template<typename T>
Matrix4x4<T>
operator-(const Matrix4x4<T>& m);
template<typename T>
Matrix4x4<T>
operator/(const Matrix4x4<T>& m, T divisor);
template<typename T>
bool
operator==(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2);
template<typename T>
bool
operator!=(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2);

template<typename T>
Matrix4x4<T>
inverse(const Matrix4x4<T>& m, bool* invertible = nullptr);
template<typename T>
Matrix4x4<T>
transpose(const Matrix4x4<T>& m);

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Matrix3x3 functions                                                       //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T> inline Matrix3x3<T>::Matrix3x3()
{
    m[0][0] = T(1.0);
    m[0][1] = T(0.0);
    m[0][2] = T(0.0);
    m[1][0] = T(0.0);
    m[1][1] = T(1.0);
    m[1][2] = T(0.0);
    m[2][0] = T(0.0);
    m[2][1] = T(0.0);
    m[2][2] = T(1.0);
}

template<typename T> template<typename _T> inline Matrix3x3<T>::Matrix3x3(_T s)
{
    m[0][0] = T(s);
    m[0][1] = T(0.0);
    m[0][2] = T(0.0);
    m[1][0] = T(0.0);
    m[1][1] = T(s);
    m[1][2] = T(0.0);
    m[2][0] = T(0.0);
    m[2][1] = T(0.0);
    m[2][2] = T(s);
}

template<typename T>
template<typename _T>
inline Matrix3x3<T>::Matrix3x3(_T m11, _T m21, _T m31, _T m12, _T m22, _T m32, _T m13, _T m23, _T m33)
{
    m[0][0] = T(m11);
    m[0][1] = T(m21);
    m[0][2] = T(m31);
    m[1][0] = T(m12);
    m[1][1] = T(m22);
    m[1][2] = T(m32);
    m[2][0] = T(m13);
    m[2][1] = T(m23);
    m[2][2] = T(m33);
}

template<typename T>
template<typename _T, bool _A>
inline Matrix3x3<T>::Matrix3x3(const Vector<_T, 3, _A>& column1,
                               const Vector<_T, 3, _A>& column2,
                               const Vector<_T, 3, _A>& column3)
{
    m[0][0] = T(column1.x);
    m[0][1] = T(column1.y);
    m[0][2] = T(column1.z);
    m[1][0] = T(column2.x);
    m[1][1] = T(column2.y);
    m[1][2] = T(column2.z);
    m[2][0] = T(column3.x);
    m[2][1] = T(column3.y);
    m[2][2] = T(column3.z);
}

template<typename T> template<typename _T> inline Matrix3x3<T>::Matrix3x3(const Matrix3x3<_T>& other)
{
    m[0][0] = T(other.m[0][0]);
    m[0][1] = T(other.m[0][1]);
    m[0][2] = T(other.m[0][2]);
    m[1][0] = T(other.m[1][0]);
    m[1][1] = T(other.m[1][1]);
    m[1][2] = T(other.m[1][2]);
    m[2][0] = T(other.m[2][0]);
    m[2][1] = T(other.m[2][1]);
    m[2][2] = T(other.m[2][2]);
}

template<typename T> template<typename _T> inline Matrix3x3<T>::Matrix3x3(const Matrix4x4<_T>& other)
{
    m[0][0] = T(other.m[0][0]);
    m[0][1] = T(other.m[0][1]);
    m[0][2] = T(other.m[0][2]);
    m[1][0] = T(other.m[1][0]);
    m[1][1] = T(other.m[1][1]);
    m[1][2] = T(other.m[1][2]);
    m[2][0] = T(other.m[2][0]);
    m[2][1] = T(other.m[2][1]);
    m[2][2] = T(other.m[2][2]);
}

template<typename T>
inline Matrix3x3<T>&
Matrix3x3<T>::operator*=(const Matrix3x3<T>& other)
{
    *this = *this * other;
    return *this;
}

template<typename T>
inline Matrix3x3<T>&
Matrix3x3<T>::operator*=(T factor)
{
    m[0][0] *= factor;
    m[0][1] *= factor;
    m[0][2] *= factor;
    m[1][0] *= factor;
    m[1][1] *= factor;
    m[1][2] *= factor;
    m[2][0] *= factor;
    m[2][1] *= factor;
    m[2][2] *= factor;
    return *this;
}

template<typename T>
inline Matrix3x3<T>&
Matrix3x3<T>::operator+=(const Matrix3x3<T>& other)
{
    m[0][0] += other.m[0][0];
    m[0][1] += other.m[0][1];
    m[0][2] += other.m[0][2];
    m[1][0] += other.m[1][0];
    m[1][1] += other.m[1][1];
    m[1][2] += other.m[1][2];
    m[2][0] += other.m[2][0];
    m[2][1] += other.m[2][1];
    m[2][2] += other.m[2][2];
    return *this;
}

template<typename T>
inline Matrix3x3<T>&
Matrix3x3<T>::operator-=(const Matrix3x3<T>& other)
{
    m[0][0] -= other.m[0][0];
    m[0][1] -= other.m[0][1];
    m[0][2] -= other.m[0][2];
    m[1][0] -= other.m[1][0];
    m[1][1] -= other.m[1][1];
    m[1][2] -= other.m[1][2];
    m[2][0] -= other.m[2][0];
    m[2][1] -= other.m[2][1];
    m[2][2] -= other.m[2][2];
    return *this;
}

template<typename T>
inline Matrix3x3<T>&
Matrix3x3<T>::operator/=(T divisor)
{
    m[0][0] /= divisor;
    m[0][1] /= divisor;
    m[0][2] /= divisor;
    m[1][0] /= divisor;
    m[1][1] /= divisor;
    m[1][2] /= divisor;
    m[2][0] /= divisor;
    m[2][1] /= divisor;
    m[2][2] /= divisor;
    return *this;
}

template<typename T> inline Matrix3x3<T> operator*(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2)
{
    return Matrix3x3<T>(m1.m[0][0] * m2.m[0][0] + m1.m[1][0] * m2.m[0][1] + m1.m[2][0] * m2.m[0][2],
                        m1.m[0][1] * m2.m[0][0] + m1.m[1][1] * m2.m[0][1] + m1.m[2][1] * m2.m[0][2],
                        m1.m[0][2] * m2.m[0][0] + m1.m[1][2] * m2.m[0][1] + m1.m[2][2] * m2.m[0][2],
                        m1.m[0][0] * m2.m[1][0] + m1.m[1][0] * m2.m[1][1] + m1.m[2][0] * m2.m[1][2],
                        m1.m[0][1] * m2.m[1][0] + m1.m[1][1] * m2.m[1][1] + m1.m[2][1] * m2.m[1][2],
                        m1.m[0][2] * m2.m[1][0] + m1.m[1][2] * m2.m[1][1] + m1.m[2][2] * m2.m[1][2],
                        m1.m[0][0] * m2.m[2][0] + m1.m[1][0] * m2.m[2][1] + m1.m[2][0] * m2.m[2][2],
                        m1.m[0][1] * m2.m[2][0] + m1.m[1][1] * m2.m[2][1] + m1.m[2][1] * m2.m[2][2],
                        m1.m[0][2] * m2.m[2][0] + m1.m[1][2] * m2.m[2][1] + m1.m[2][2] * m2.m[2][2]);
}

template<typename T, bool A> inline Vector<T, 3, A> operator*(const Vector<T, 3, A>& v, const Matrix3x3<T>& m)
{
    return Vector<T, 3, A>(v.x * m.m[0][0] + v.y * m.m[0][1] + v.z * m.m[0][2],
                           v.x * m.m[1][0] + v.y * m.m[1][1] + v.z * m.m[1][2],
                           v.x * m.m[2][0] + v.y * m.m[2][1] + v.z * m.m[2][2]);
}

template<typename T, bool A> inline Vector<T, 3, A> operator*(const Matrix3x3<T>& m, const Vector<T, 3, A>& v)
{
    return Vector<T, 3, A>(m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z,
                           m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z,
                           m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z);
}

template<typename T> inline Matrix3x3<T> operator*(T factor, const Matrix3x3<T>& m)
{
    return (m * factor);
}

template<typename T> inline Matrix3x3<T> operator*(const Matrix3x3<T>& m, T factor)
{
    return Matrix3x3<T>(m.m[0][0] * factor, m.m[0][1] * factor, m.m[0][2] * factor, m.m[1][0] * factor,
                        m.m[1][1] * factor, m.m[1][2] * factor, m.m[2][0] * factor, m.m[2][1] * factor,
                        m.m[2][2] * factor);
}

template<typename T>
inline Matrix3x3<T>
operator+(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2)
{
    return Matrix3x3<T>(m1.m[0][0] + m2.m[0][0], m1.m[0][1] + m2.m[0][1], m1.m[0][2] + m2.m[0][2],
                        m1.m[1][0] + m2.m[1][0], m1.m[1][1] + m2.m[1][1], m1.m[1][2] + m2.m[1][2],
                        m1.m[2][0] + m2.m[2][0], m1.m[2][1] + m2.m[2][1], m1.m[2][2] + m2.m[2][2]);
}

template<typename T>
inline Matrix3x3<T>
operator-(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2)
{
    return Matrix3x3<T>(m1.m[0][0] - m2.m[0][0], m1.m[0][1] - m2.m[0][1], m1.m[0][2] - m2.m[0][2],
                        m1.m[1][0] - m2.m[1][0], m1.m[1][1] - m2.m[1][1], m1.m[1][2] - m2.m[1][2],
                        m1.m[2][0] - m2.m[2][0], m1.m[2][1] - m2.m[2][1], m1.m[2][2] - m2.m[2][2]);
}

template<typename T>
inline Matrix3x3<T>
operator-(const Matrix3x3<T>& m)
{
    return Matrix3x3<T>(-m.m[0][0], -m.m[0][1], -m.m[0][2], -m.m[1][0], -m.m[1][1], -m.m[1][2], -m.m[2][0], -m.m[2][1],
                        -m.m[2][2]);
}

template<typename T>
inline Matrix3x3<T>
operator/(const Matrix3x3<T>& m, T divisor)
{
    return Matrix3x3<T>(m.m[0][0] / divisor, m.m[0][1] / divisor, m.m[0][2] / divisor, m.m[1][0] / divisor,
                        m.m[1][1] / divisor, m.m[1][2] / divisor, m.m[2][0] / divisor, m.m[2][1] / divisor,
                        m.m[2][2] / divisor);
}

template<typename T>
inline bool
operator==(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2)
{
    return (m1.m[0][0] == m2.m[0][0] && m1.m[0][1] == m2.m[0][1] && m1.m[0][2] == m2.m[0][2] &&
            m1.m[1][0] == m2.m[1][0] && m1.m[1][1] == m2.m[1][1] && m1.m[1][2] == m2.m[1][2] &&
            m1.m[2][0] == m2.m[2][0] && m1.m[2][1] == m2.m[2][1] && m1.m[2][2] == m2.m[2][2]);
}

template<typename T>
inline bool
operator!=(const Matrix3x3<T>& m1, const Matrix3x3<T>& m2)
{
    return !(m1 == m2);
}

template<typename T>
inline Matrix3x3<T>
inverse(const Matrix3x3<T>& m, bool* invertible)
{
    Matrix3x3<T> inv;
    inv.m[0][0] = (m[1][1] * m[2][2] - m[1][2] * m[2][1]);
    inv.m[1][0] = -(m[1][0] * m[2][2] - m[1][2] * m[2][0]);
    inv.m[2][0] = (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
    T det       = m[0][0] * inv.m[0][0] + m[0][1] * inv.m[1][0] + m[0][2] * inv.m[2][0];
    if (det == T(0.0)) {
        if (invertible != nullptr)
            *invertible = false;
        return Matrix3x3<T>();
    }
    inv.m[0][1] = -(m[0][1] * m[2][2] - m[0][2] * m[2][1]);
    inv.m[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]);
    inv.m[2][1] = -(m[0][0] * m[2][1] - m[0][1] * m[2][0]);
    inv.m[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]);
    inv.m[1][2] = -(m[0][0] * m[1][2] - m[0][2] * m[1][0]);
    inv.m[2][2] = (m[0][0] * m[1][1] - m[0][1] * m[1][0]);
    inv *= (T(1.0) / det);
    if (invertible != nullptr)
        *invertible = true;
    return inv;
}

template<typename T>
inline Matrix3x3<T>
transpose(const Matrix3x3<T>& m)
{
    return Matrix3x3<T>(m[0][0], m[1][0], m[2][0], m[0][1], m[1][1], m[2][1], m[0][2], m[1][2], m[2][2]);
}

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Matrix4x4 functions                                                       //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T> inline Matrix4x4<T>::Matrix4x4()
{
    m[0][0] = T(1.0);
    m[0][1] = T(0.0);
    m[0][2] = T(0.0);
    m[0][3] = T(0.0);
    m[1][0] = T(0.0);
    m[1][1] = T(1.0);
    m[1][2] = T(0.0);
    m[1][3] = T(0.0);
    m[2][0] = T(0.0);
    m[2][1] = T(0.0);
    m[2][2] = T(1.0);
    m[2][3] = T(0.0);
    m[3][0] = T(0.0);
    m[3][1] = T(0.0);
    m[3][2] = T(0.0);
    m[3][3] = T(1.0);
}

template<typename T> template<typename _T> inline Matrix4x4<T>::Matrix4x4(_T s)
{
    m[0][0] = T(s);
    m[0][1] = T(0.0);
    m[0][2] = T(0.0);
    m[0][3] = T(0.0);
    m[1][0] = T(0.0);
    m[1][1] = T(s);
    m[1][2] = T(0.0);
    m[1][3] = T(0.0);
    m[2][0] = T(0.0);
    m[2][1] = T(0.0);
    m[2][2] = T(s);
    m[2][3] = T(0.0);
    m[3][0] = T(0.0);
    m[3][1] = T(0.0);
    m[3][2] = T(0.0);
    m[3][3] = T(s);
}

template<typename T>
template<typename _T>
inline Matrix4x4<T>::Matrix4x4(_T m11,
                               _T m21,
                               _T m31,
                               _T m41,
                               _T m12,
                               _T m22,
                               _T m32,
                               _T m42,
                               _T m13,
                               _T m23,
                               _T m33,
                               _T m43,
                               _T m14,
                               _T m24,
                               _T m34,
                               _T m44)
{
    m[0][0] = T(m11);
    m[0][1] = T(m21);
    m[0][2] = T(m31);
    m[0][3] = T(m41);
    m[1][0] = T(m12);
    m[1][1] = T(m22);
    m[1][2] = T(m32);
    m[1][3] = T(m42);
    m[2][0] = T(m13);
    m[2][1] = T(m23);
    m[2][2] = T(m33);
    m[2][3] = T(m43);
    m[3][0] = T(m14);
    m[3][1] = T(m24);
    m[3][2] = T(m34);
    m[3][3] = T(m44);
}

template<typename T>
template<typename _T>
inline Matrix4x4<T>::Matrix4x4(const Vector4<_T>& column1,
                               const Vector4<_T>& column2,
                               const Vector4<_T>& column3,
                               const Vector4<_T>& column4)
{
    m[0][0] = T(column1.x);
    m[0][1] = T(column1.y);
    m[0][2] = T(column1.z);
    m[0][3] = T(column1.w);
    m[1][0] = T(column2.x);
    m[1][1] = T(column2.y);
    m[1][2] = T(column2.z);
    m[1][3] = T(column2.w);
    m[2][0] = T(column3.x);
    m[2][1] = T(column3.y);
    m[2][2] = T(column3.z);
    m[2][3] = T(column3.w);
    m[3][0] = T(column4.x);
    m[3][1] = T(column4.y);
    m[3][2] = T(column4.z);
    m[3][3] = T(column4.w);
}

template<typename T> template<typename _T> inline Matrix4x4<T>::Matrix4x4(const Matrix3x3<_T>& m)
{
    m[0][0] = T(m.m[0][0]);
    m[0][1] = T(m.m[0][1]);
    m[0][2] = T(m.m[0][2]);
    m[0][3] = T(0.0);
    m[1][0] = T(m.m[1][0]);
    m[1][1] = T(m.m[1][1]);
    m[1][2] = T(m.m[1][2]);
    m[1][3] = T(0.0);
    m[2][0] = T(m.m[2][0]);
    m[2][1] = T(m.m[2][1]);
    m[2][2] = T(m.m[2][2]);
    m[2][3] = T(0.0);
    m[3][0] = T(0.0);
    m[3][1] = T(0.0);
    m[3][2] = T(0.0);
    m[3][3] = T(1.0);
}

template<typename T> template<typename _T> inline Matrix4x4<T>::Matrix4x4(const Matrix4x4<_T>& other)
{
    m[0][0] = T(other.m[0][0]);
    m[0][1] = T(other.m[0][1]);
    m[0][2] = T(other.m[0][2]);
    m[0][3] = T(other.m[0][3]);
    m[1][0] = T(other.m[1][0]);
    m[1][1] = T(other.m[1][1]);
    m[1][2] = T(other.m[1][2]);
    m[1][3] = T(other.m[1][3]);
    m[2][0] = T(other.m[2][0]);
    m[2][1] = T(other.m[2][1]);
    m[2][2] = T(other.m[2][2]);
    m[2][3] = T(other.m[2][3]);
    m[3][0] = T(other.m[3][0]);
    m[3][1] = T(other.m[3][1]);
    m[3][2] = T(other.m[3][2]);
    m[3][3] = T(other.m[3][3]);
}

template<typename T>
inline Matrix4x4<T>
Matrix4x4<T>::fromFrustum(T left, T right, T bottom, T top, T zNear, T zFar)
{
    if (left == right || bottom == top || zNear == zFar)
        return Matrix4x4<T>();
    T width  = right - left;
    T height = top - bottom;
    T clip   = zFar - zNear;
    return Matrix4x4<T>(T(2.0) * zNear / width, T(0.0), T(0.0), T(0.0), T(0.0), T(2.0) * zNear / height, T(0.0), T(0.0),
                        (left + right) / width, (top + bottom) / height, -(zNear + zFar) / clip, T(-1.0), T(0.0),
                        T(0.0), T(-2.0) * zNear * zFar / clip, T(0.0));
}

template<typename T>
template<bool _A>
inline Matrix4x4<T>
Matrix4x4<T>::fromLookAt(const Vector<T, 3, _A>& eye, const Vector<T, 3, _A>& center, const Vector<T, 3, _A>& up)
{
    Vector<T, 3, _A> forward  = normalize(center - eye);
    Vector<T, 3, _A> side     = normalize(cross(forward, up));
    Vector<T, 3, _A> upVector = cross(side, forward);
    Matrix4x4<T> ret(side.x, upVector.x, -forward.x, T(0.0), side.y, upVector.y, -forward.y, T(0.0), side.z, upVector.z,
                     -forward.z, T(0.0), T(0.0), T(0.0), T(0.0), T(1.0));
    ret *= fromTranslate(-eye);
    return ret;
}

template<typename T>
inline Matrix4x4<T>
Matrix4x4<T>::fromLookAt(T eyex, T eyey, T eyez, T centerx, T centery, T centerz, T upx, T upy, T upz)
{
    return fromLookAt(Vector3<T>(eyex, eyey, eyez), Vector3<T>(centerx, centery, centerz), Vector3<T>(upx, upy, upz));
}

template<typename T>
inline Matrix4x4<T>
Matrix4x4<T>::fromOrtho(T left, T right, T bottom, T top, T zNear, T zFar)
{
    if (left == right || bottom == top || zNear == zFar)
        return Matrix4x4<T>();
    T width  = right - left;
    T height = top - bottom;
    T clip   = zFar - zNear;
    return Matrix4x4<T>(T(2.0) / width, T(0.0), T(0.0), T(0.0), T(0.0), T(2.0) / height, T(0.0), T(0.0), T(0.0), T(0.0),
                        T(-2.0) / clip, T(0.0), -(left + right) / width, -(top + bottom) / height,
                        -(zNear + zFar) / clip, T(1.0));
}

// fovy := view angle in the y direction, in degrees
template<typename T>
inline Matrix4x4<T>
Matrix4x4<T>::fromPerspective(T fovy, T aspect, T zNear, T zFar)
{
    if (zNear == zFar || aspect == T(0.0))
        return Matrix4x4<T>();
    T rads = radians(fovy * T(0.5));
    T sine = T(sin(rads));
    if (sine == T(0.0))
        return Matrix4x4<T>();
    T cotan = T(cos(rads)) / sine;
    T clip  = zFar - zNear;
    return Matrix4x4<T>(cotan / aspect, T(0.0), T(0.0), T(0.0), T(0.0), cotan, T(0.0), T(0.0), T(0.0), T(0.0),
                        -(zNear + zFar) / clip, T(-1.0), T(0.0), T(0.0), -(T(2.0) * zNear * zFar) / clip, T(0.0));
}

// angle := in degrees
template<typename T>
template<bool _A>
inline Matrix4x4<T>
Matrix4x4<T>::fromRotate(T angle, const Vector<T, 3, _A>& vector)
{
    Vector<T, 3, _A> v  = normalize(vector);
    T                a  = radians(angle);
    T                c  = T(cos(a));
    T                s  = T(sin(a));
    T                ic = T(1.0) - c;
    return Matrix4x4<T>(v.x * v.x * ic + c, v.y * v.x * ic + v.z * s, v.x * v.z * ic - v.y * s, T(0.0),
                        v.x * v.y * ic - v.z * s, v.y * v.y * ic + c, v.y * v.z * ic + v.x * s, T(0.0),
                        v.x * v.z * ic + v.y * s, v.y * v.z * ic - v.x * s, v.z * v.z * ic + c, T(0.0), T(0.0), T(0.0),
                        T(0.0), T(1.0));
}

template<typename T>
inline Matrix4x4<T>
Matrix4x4<T>::fromRotate(T angle, T x, T y, T z)
{
    return fromRotate(angle, Vector3<T>(x, y, z));
}

template<typename T>
template<bool _A>
inline Matrix4x4<T>
Matrix4x4<T>::fromScale(const Vector<T, 3, _A>& vector)
{
    return Matrix4x4<T>(vector.x, T(0.0), T(0.0), T(0.0), T(0.0), vector.y, T(0.0), T(0.0), T(0.0), T(0.0), vector.z,
                        T(0.0), T(0.0), T(0.0), T(0.0), T(1.0));
}

template<typename T>
inline Matrix4x4<T>
Matrix4x4<T>::fromScale(T x, T y, T z)
{
    return fromScale(Vector3<T>(x, y, z));
}

template<typename T>
template<bool _A>
inline Matrix4x4<T>
Matrix4x4<T>::fromTranslate(const Vector<T, 3, _A>& vector)
{
    return Matrix4x4<T>(T(1.0), T(0.0), T(0.0), T(0.0), T(0.0), T(1.0), T(0.0), T(0.0), T(0.0), T(0.0), T(1.0), T(0.0),
                        vector.x, vector.y, vector.z, T(1.0));
}

template<typename T>
inline Matrix4x4<T>
Matrix4x4<T>::fromTranslate(T x, T y, T z)
{
    return fromTranslate(Vector3<T>(x, y, z));
}

template<typename T>
inline Matrix4x4<T>&
Matrix4x4<T>::operator*=(const Matrix4x4<T>& other)
{
    *this = *this * other;
    return *this;
}

template<typename T>
inline Matrix4x4<T>&
Matrix4x4<T>::operator*=(T factor)
{
    m[0][0] *= factor;
    m[0][1] *= factor;
    m[0][2] *= factor;
    m[0][3] *= factor;
    m[1][0] *= factor;
    m[1][1] *= factor;
    m[1][2] *= factor;
    m[1][3] *= factor;
    m[2][0] *= factor;
    m[2][1] *= factor;
    m[2][2] *= factor;
    m[2][3] *= factor;
    m[3][0] *= factor;
    m[3][1] *= factor;
    m[3][2] *= factor;
    m[3][3] *= factor;
    return *this;
}

template<typename T>
inline Matrix4x4<T>&
Matrix4x4<T>::operator+=(const Matrix4x4<T>& other)
{
    m[0][0] += other.m[0][0];
    m[0][1] += other.m[0][1];
    m[0][2] += other.m[0][2];
    m[0][3] += other.m[0][3];
    m[1][0] += other.m[1][0];
    m[1][1] += other.m[1][1];
    m[1][2] += other.m[1][2];
    m[1][3] += other.m[1][3];
    m[2][0] += other.m[2][0];
    m[2][1] += other.m[2][1];
    m[2][2] += other.m[2][2];
    m[2][3] += other.m[2][3];
    m[3][0] += other.m[3][0];
    m[3][1] += other.m[3][1];
    m[3][2] += other.m[3][2];
    m[3][3] += other.m[3][3];
    return *this;
}

template<typename T>
inline Matrix4x4<T>&
Matrix4x4<T>::operator-=(const Matrix4x4<T>& other)
{
    m[0][0] -= other.m[0][0];
    m[0][1] -= other.m[0][1];
    m[0][2] -= other.m[0][2];
    m[0][3] -= other.m[0][3];
    m[1][0] -= other.m[1][0];
    m[1][1] -= other.m[1][1];
    m[1][2] -= other.m[1][2];
    m[1][3] -= other.m[1][3];
    m[2][0] -= other.m[2][0];
    m[2][1] -= other.m[2][1];
    m[2][2] -= other.m[2][2];
    m[2][3] -= other.m[2][3];
    m[3][0] -= other.m[3][0];
    m[3][1] -= other.m[3][1];
    m[3][2] -= other.m[3][2];
    m[3][3] -= other.m[3][3];
    return *this;
}

template<typename T>
inline Matrix4x4<T>&
Matrix4x4<T>::operator/=(T divisor)
{
    m[0][0] /= divisor;
    m[0][1] /= divisor;
    m[0][2] /= divisor;
    m[0][3] /= divisor;
    m[1][0] /= divisor;
    m[1][1] /= divisor;
    m[1][2] /= divisor;
    m[1][3] /= divisor;
    m[2][0] /= divisor;
    m[2][1] /= divisor;
    m[2][2] /= divisor;
    m[2][3] /= divisor;
    m[3][0] /= divisor;
    m[3][1] /= divisor;
    m[3][2] /= divisor;
    m[3][3] /= divisor;
    return *this;
}

// matrix multiplication
template<typename T> inline Matrix4x4<T> operator*(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2)
{
    return Matrix4x4<T>(
      m1.m[0][0] * m2.m[0][0] + m1.m[1][0] * m2.m[0][1] + m1.m[2][0] * m2.m[0][2] + m1.m[3][0] * m2.m[0][3],
      m1.m[0][1] * m2.m[0][0] + m1.m[1][1] * m2.m[0][1] + m1.m[2][1] * m2.m[0][2] + m1.m[3][1] * m2.m[0][3],
      m1.m[0][2] * m2.m[0][0] + m1.m[1][2] * m2.m[0][1] + m1.m[2][2] * m2.m[0][2] + m1.m[3][2] * m2.m[0][3],
      m1.m[0][3] * m2.m[0][0] + m1.m[1][3] * m2.m[0][1] + m1.m[2][3] * m2.m[0][2] + m1.m[3][3] * m2.m[0][3],
      m1.m[0][0] * m2.m[1][0] + m1.m[1][0] * m2.m[1][1] + m1.m[2][0] * m2.m[1][2] + m1.m[3][0] * m2.m[1][3],
      m1.m[0][1] * m2.m[1][0] + m1.m[1][1] * m2.m[1][1] + m1.m[2][1] * m2.m[1][2] + m1.m[3][1] * m2.m[1][3],
      m1.m[0][2] * m2.m[1][0] + m1.m[1][2] * m2.m[1][1] + m1.m[2][2] * m2.m[1][2] + m1.m[3][2] * m2.m[1][3],
      m1.m[0][3] * m2.m[1][0] + m1.m[1][3] * m2.m[1][1] + m1.m[2][3] * m2.m[1][2] + m1.m[3][3] * m2.m[1][3],
      m1.m[0][0] * m2.m[2][0] + m1.m[1][0] * m2.m[2][1] + m1.m[2][0] * m2.m[2][2] + m1.m[3][0] * m2.m[2][3],
      m1.m[0][1] * m2.m[2][0] + m1.m[1][1] * m2.m[2][1] + m1.m[2][1] * m2.m[2][2] + m1.m[3][1] * m2.m[2][3],
      m1.m[0][2] * m2.m[2][0] + m1.m[1][2] * m2.m[2][1] + m1.m[2][2] * m2.m[2][2] + m1.m[3][2] * m2.m[2][3],
      m1.m[0][3] * m2.m[2][0] + m1.m[1][3] * m2.m[2][1] + m1.m[2][3] * m2.m[2][2] + m1.m[3][3] * m2.m[2][3],
      m1.m[0][0] * m2.m[3][0] + m1.m[1][0] * m2.m[3][1] + m1.m[2][0] * m2.m[3][2] + m1.m[3][0] * m2.m[3][3],
      m1.m[0][1] * m2.m[3][0] + m1.m[1][1] * m2.m[3][1] + m1.m[2][1] * m2.m[3][2] + m1.m[3][1] * m2.m[3][3],
      m1.m[0][2] * m2.m[3][0] + m1.m[1][2] * m2.m[3][1] + m1.m[2][2] * m2.m[3][2] + m1.m[3][2] * m2.m[3][3],
      m1.m[0][3] * m2.m[3][0] + m1.m[1][3] * m2.m[3][1] + m1.m[2][3] * m2.m[3][2] + m1.m[3][3] * m2.m[3][3]);
}

template<typename T, bool A> inline Vector<T, 3, A> operator*(const Vector<T, 3, A>& v, const Matrix4x4<T>& m)
{
    T x = v.x * m.m[0][0] + v.y * m.m[0][1] + v.z * m.m[0][2] + m.m[0][3];
    T y = v.x * m.m[1][0] + v.y * m.m[1][1] + v.z * m.m[1][2] + m.m[1][3];
    T z = v.x * m.m[2][0] + v.y * m.m[2][1] + v.z * m.m[2][2] + m.m[2][3];
    T w = v.x * m.m[3][0] + v.y * m.m[3][1] + v.z * m.m[3][2] + m.m[3][3];
    return (w == T(1.0) ? Vector<T, 3, A>(x, y, z) : Vector<T, 3, A>(x / w, y / w, z / w));
}

template<typename T, bool A> inline Vector<T, 3, A> operator*(const Matrix4x4<T>& m, const Vector<T, 3, A>& v)
{
    T x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z + m.m[3][0];
    T y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z + m.m[3][1];
    T z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z + m.m[3][2];
    T w = m.m[0][3] * v.x + m.m[1][3] * v.y + m.m[2][3] * v.z + m.m[3][3];
    return (w == T(1.0) ? Vector<T, 3, A>(x, y, z) : Vector<T, 3, A>(x / w, y / w, z / w));
}

template<typename T> inline Vector4<T> operator*(const Vector4<T>& v, const Matrix4x4<T>& m)
{
    return Vector4<T>(v.x * m.m[0][0] + v.y * m.m[0][1] + v.z * m.m[0][2] + v.w * m.m[0][3],
                      v.x * m.m[1][0] + v.y * m.m[1][1] + v.z * m.m[1][2] + v.w * m.m[1][3],
                      v.x * m.m[2][0] + v.y * m.m[2][1] + v.z * m.m[2][2] + v.w * m.m[2][3],
                      v.x * m.m[3][0] + v.y * m.m[3][1] + v.z * m.m[3][2] + v.w * m.m[3][3]);
}

template<typename T> inline Vector4<T> operator*(const Matrix4x4<T>& m, const Vector4<T>& v)
{
    return Vector4<T>(m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z + m.m[3][0] * v.w,
                      m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z + m.m[3][1] * v.w,
                      m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z + m.m[3][2] * v.w,
                      m.m[0][3] * v.x + m.m[1][3] * v.y + m.m[2][3] * v.z + m.m[3][3] * v.w);
}

template<typename T> inline Matrix4x4<T> operator*(T factor, const Matrix4x4<T>& m)
{
    return (m * factor);
}

template<typename T> inline Matrix4x4<T> operator*(const Matrix4x4<T>& m, T factor)
{
    return Matrix4x4<T>(m.m[0][0] * factor, m.m[0][1] * factor, m.m[0][2] * factor, m.m[0][3] * factor,
                        m.m[1][0] * factor, m.m[1][1] * factor, m.m[1][2] * factor, m.m[1][3] * factor,
                        m.m[2][0] * factor, m.m[2][1] * factor, m.m[2][2] * factor, m.m[2][3] * factor,
                        m.m[3][0] * factor, m.m[3][1] * factor, m.m[3][2] * factor, m.m[3][3] * factor);
}

template<typename T>
inline Matrix4x4<T>
operator+(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2)
{
    return Matrix4x4<T>(
      m1.m[0][0] + m2.m[0][0], m1.m[0][1] + m2.m[0][1], m1.m[0][2] + m2.m[0][2], m1.m[0][3] + m2.m[0][3],
      m1.m[1][0] + m2.m[1][0], m1.m[1][1] + m2.m[1][1], m1.m[1][2] + m2.m[1][2], m1.m[1][3] + m2.m[1][3],
      m1.m[2][0] + m2.m[2][0], m1.m[2][1] + m2.m[2][1], m1.m[2][2] + m2.m[2][2], m1.m[2][3] + m2.m[2][3],
      m1.m[3][0] + m2.m[3][0], m1.m[3][1] + m2.m[3][1], m1.m[3][2] + m2.m[3][2], m1.m[3][3] + m2.m[3][3]);
}

template<typename T>
inline Matrix4x4<T>
operator-(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2)
{
    return Matrix4x4<T>(
      m1.m[0][0] - m2.m[0][0], m1.m[0][1] - m2.m[0][1], m1.m[0][2] - m2.m[0][2], m1.m[0][3] - m2.m[0][3],
      m1.m[1][0] - m2.m[1][0], m1.m[1][1] - m2.m[1][1], m1.m[1][2] - m2.m[1][2], m1.m[1][3] - m2.m[1][3],
      m1.m[2][0] - m2.m[2][0], m1.m[2][1] - m2.m[2][1], m1.m[2][2] - m2.m[2][2], m1.m[2][3] - m2.m[2][3],
      m1.m[3][0] - m2.m[3][0], m1.m[3][1] - m2.m[3][1], m1.m[3][2] - m2.m[3][2], m1.m[3][3] - m2.m[3][3]);
}

template<typename T>
inline Matrix4x4<T>
operator-(const Matrix4x4<T>& m)
{
    return Matrix4x4<T>(-m.m[0][0], -m.m[0][1], -m.m[0][2], -m.m[0][3], -m.m[1][0], -m.m[1][1], -m.m[1][2], -m.m[1][3],
                        -m.m[2][0], -m.m[2][1], -m.m[2][2], -m.m[2][3], -m.m[3][0], -m.m[3][1], -m.m[3][2], -m.m[3][3]);
}

template<typename T>
inline Matrix4x4<T>
operator/(const Matrix4x4<T>& m, T divisor)
{
    return Matrix4x4<T>(m.m[0][0] / divisor, m.m[0][1] / divisor, m.m[0][2] / divisor, m.m[0][3] / divisor,
                        m.m[1][0] / divisor, m.m[1][1] / divisor, m.m[1][2] / divisor, m.m[1][3] / divisor,
                        m.m[2][0] / divisor, m.m[2][1] / divisor, m.m[2][2] / divisor, m.m[2][3] / divisor,
                        m.m[3][0] / divisor, m.m[3][1] / divisor, m.m[3][2] / divisor, m.m[3][3] / divisor);
}

template<typename T>
inline bool
operator==(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2)
{
    return (
      m1.m[0][0] == m2.m[0][0] && m1.m[0][1] == m2.m[0][1] && m1.m[0][2] == m2.m[0][2] && m1.m[0][3] == m2.m[0][3] &&
      m1.m[1][0] == m2.m[1][0] && m1.m[1][1] == m2.m[1][1] && m1.m[1][2] == m2.m[1][2] && m1.m[1][3] == m2.m[1][3] &&
      m1.m[2][0] == m2.m[2][0] && m1.m[2][1] == m2.m[2][1] && m1.m[2][2] == m2.m[2][2] && m1.m[2][3] == m2.m[2][3] &&
      m1.m[3][0] == m2.m[3][0] && m1.m[3][1] == m2.m[3][1] && m1.m[3][2] == m2.m[3][2] && m1.m[3][3] == m2.m[3][3]);
}

template<typename T>
inline bool
operator!=(const Matrix4x4<T>& m1, const Matrix4x4<T>& m2)
{
    return !(m1 == m2);
}

// non-member functions

template<typename T>
inline T
determinant(const Matrix4x4<T>& m)
{
    return (
      m[0][0] * (m[1][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) - m[1][2] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) +
                 m[1][3] * (m[2][1] * m[3][2] - m[2][2] * m[3][1])) +
      m[0][1] *
        -(m[1][0] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) - m[1][2] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
          m[1][3] * (m[2][0] * m[3][2] - m[2][2] * m[3][0])) +
      m[0][2] * (m[1][0] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) - m[1][1] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
                 m[1][3] * (m[2][0] * m[3][1] - m[2][1] * m[3][0])) +
      m[0][3] *
        -(m[1][0] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]) - m[1][1] * (m[2][0] * m[3][2] - m[2][2] * m[3][0]) +
          m[1][2] * (m[2][0] * m[3][1] - m[2][1] * m[3][0])));
}

template<typename T>
inline Matrix4x4<T>
inverse(const Matrix4x4<T>& m, bool* invertible)
{
    Matrix4x4<T> inv;
    inv.m[0][0] =
      (m[1][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) - m[1][2] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) +
       m[1][3] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]));
    inv.m[1][0] =
      -(m[1][0] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) - m[1][2] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
        m[1][3] * (m[2][0] * m[3][2] - m[2][2] * m[3][0]));
    inv.m[2][0] =
      (m[1][0] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) - m[1][1] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
       m[1][3] * (m[2][0] * m[3][1] - m[2][1] * m[3][0]));
    inv.m[3][0] =
      -(m[1][0] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]) - m[1][1] * (m[2][0] * m[3][2] - m[2][2] * m[3][0]) +
        m[1][2] * (m[2][0] * m[3][1] - m[2][1] * m[3][0]));
    T det = m[0][0] * inv.m[0][0] + m[0][1] * inv.m[1][0] + m[0][2] * inv.m[2][0] + m[0][3] * inv.m[3][0];
    if (det == T(0.0)) {
        if (invertible != nullptr)
            *invertible = false;
        return Matrix4x4<T>();
    }
    inv.m[0][1] =
      -(m[0][1] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) - m[0][2] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) +
        m[0][3] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]));
    inv.m[1][1] =
      (m[0][0] * (m[2][2] * m[3][3] - m[2][3] * m[3][2]) - m[0][2] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
       m[0][3] * (m[2][0] * m[3][2] - m[2][2] * m[3][0]));
    inv.m[2][1] =
      -(m[0][0] * (m[2][1] * m[3][3] - m[2][3] * m[3][1]) - m[0][1] * (m[2][0] * m[3][3] - m[2][3] * m[3][0]) +
        m[0][3] * (m[2][0] * m[3][1] - m[2][1] * m[3][0]));
    inv.m[3][1] =
      (m[0][0] * (m[2][1] * m[3][2] - m[2][2] * m[3][1]) - m[0][1] * (m[2][0] * m[3][2] - m[2][2] * m[3][0]) +
       m[0][2] * (m[2][0] * m[3][1] - m[2][1] * m[3][0]));
    inv.m[0][2] =
      (m[0][1] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) - m[0][2] * (m[1][1] * m[3][3] - m[1][3] * m[3][1]) +
       m[0][3] * (m[1][1] * m[3][2] - m[1][2] * m[3][1]));
    inv.m[1][2] =
      -(m[0][0] * (m[1][2] * m[3][3] - m[1][3] * m[3][2]) - m[0][2] * (m[1][0] * m[3][3] - m[1][3] * m[3][0]) +
        m[0][3] * (m[1][0] * m[3][2] - m[1][2] * m[3][0]));
    inv.m[2][2] =
      (m[0][0] * (m[1][1] * m[3][3] - m[1][3] * m[3][1]) - m[0][1] * (m[1][0] * m[3][3] - m[1][3] * m[3][0]) +
       m[0][3] * (m[1][0] * m[3][1] - m[1][1] * m[3][0]));
    inv.m[3][2] =
      -(m[0][0] * (m[1][1] * m[3][2] - m[1][2] * m[3][1]) - m[0][1] * (m[1][0] * m[3][2] - m[1][2] * m[3][0]) +
        m[0][2] * (m[1][0] * m[3][1] - m[1][1] * m[3][0]));
    inv.m[0][3] =
      -(m[0][1] * (m[1][2] * m[2][3] - m[1][3] * m[2][2]) - m[0][2] * (m[1][1] * m[2][3] - m[1][3] * m[2][1]) +
        m[0][3] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]));
    inv.m[1][3] =
      (m[0][0] * (m[1][2] * m[2][3] - m[1][3] * m[2][2]) - m[0][2] * (m[1][0] * m[2][3] - m[1][3] * m[2][0]) +
       m[0][3] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]));
    inv.m[2][3] =
      -(m[0][0] * (m[1][1] * m[2][3] - m[1][3] * m[2][1]) - m[0][1] * (m[1][0] * m[2][3] - m[1][3] * m[2][0]) +
        m[0][3] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]));
    inv.m[3][3] =
      (m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) - m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
       m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]));
    inv *= (T(1.0) / det);
    if (invertible != nullptr)
        *invertible = true;
    return inv;
}

template<typename T>
inline Matrix4x4<T>
transpose(const Matrix4x4<T>& m)
{
    return Matrix4x4<T>(m[0][0], m[1][0], m[2][0], m[3][0], m[0][1], m[1][1], m[2][1], m[3][1], m[0][2], m[1][2],
                        m[2][2], m[3][2], m[0][3], m[1][3], m[2][3], m[3][3]);
}

} // namespace math
} // namespace vidi

#endif // VIDICODEBASE_MATH_MATRIX_H
