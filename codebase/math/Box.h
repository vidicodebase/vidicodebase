//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_MATH_BOX_H
#define VIDICODEBASE_MATH_BOX_H

#include "Range.h"
#include "Vector.h"
#include <vidi_base_export.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Forward Declarations                                                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T, bool A> class Range<Vector<T, 3, A>>;
template<typename T, bool A = false> using Box = Range<Vector<T, 3, A>>;

using box3i = Box<int32_t>;
using box3f = Box<float>;
using box3d = Box<double>;

using box3ia = Box<int32_t, true>;
using box3fa = Box<float, true>;
using box3da = Box<double, true>;

using range2i = Range<Vector2<int32_t>>;
using range2f = Range<Vector2<float>>;
using range2d = Range<Vector2<double>>;

using range3i = Box<int32_t>;
using range3f = Box<float>;
using range3d = Box<double>;

} // namespace math
} // namespace vidi

namespace vidi {
using math::box3d;
using math::box3da;
using math::box3f;
using math::box3fa;
using math::box3i;
using math::box3ia;
using math::range2d;
using math::range2f;
using math::range2i;
using math::range3d;
using math::range3f;
using math::range3i;
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Actual Definitions Start from Here                                        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template<typename T, bool A> class Range<Vector<T, 3, A>> {
public:
    Range() : _min(), _max() {}
    Range(const Vector<T, 3, A>& corner1, const Vector<T, 3, A>& corner2)
    {
        setExtents(corner1, corner2);
    }

    const Vector<T, 3, A>& minimum() const
    {
        return _min;
    }
    const Vector<T, 3, A>& maximum() const
    {
        return _max;
    }
    Vector<T, 3, A>& minimum()
    {
        return _min;
    }
    Vector<T, 3, A>& maximum()
    {
        return _max;
    }

    void setExtents(const Vector<T, 3, A>& corner1, const Vector<T, 3, A>& corner2)
    {
        _min = min(corner1, corner2);
        _max = max(corner1, corner2);
    }

    Vector<T, 3, A> center() const
    {
        return ((_min + _max) * T(.5));
    }
    Vector<T, 3, A> size() const
    {
        return (_max - _min);
    }

    bool contains(const Vector<T, 3, A>& point) const
    {
        return (point.x >= _min.x && point.x <= _max.x && point.y >= _min.y && point.y <= _max.y && point.z >= _min.z &&
                point.z <= _max.z);
    }

    bool contains(const Box<T, A>& box) const
    {
        return (box._min.x >= _min.x && box._max.x <= _max.x && box._min.y >= _min.y && box._max.y <= _max.y &&
                box._min.z >= _min.z && box._max.z <= _max.z);
    }

    void extend(const Box<T, A>& other)
    {
        _min = min(_min, other._min);
        _max = max(_max, other._max);
    }

    // TODO implement: void intersect(const Box& box);
    // TODO implement: Box intersected(const Box& box);

private:
    Vector<T, 3, A> _min;
    Vector<T, 3, A> _max;
};

template<typename T, bool A>
inline bool
operator==(const Box<T, A>& b1, const Box<T, A>& b2)
{
    return (b1.minimum() == b2.minimum() && b1.maximum() == b2.maximum());
}

} // namespace math
} // namespace vidi

#endif // VIDICODEBASE_MATH_BOX_H
