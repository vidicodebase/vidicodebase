//
// Created by qadwu on 6/1/19.
//

#include "Basic.h"
#include <vector>
#include <vidiBase.h>

namespace vidi {

/**
 * @brief The ViDi math module
 */
namespace math {
}

} // namespace vidi

REGISTER_TYPE(ui8, std::vector<uint8_t>, "Vector<uint8>[1]");
REGISTER_TYPE(ui16, std::vector<uint16_t>, "Vector<uint16>[1]");
REGISTER_TYPE(ui32, std::vector<uint32_t>, "Vector<uint32>[1]");
REGISTER_TYPE(ui64, std::vector<uint64_t>, "Vector<uint64>[1]");
REGISTER_TYPE(i8, std::vector<int8_t>, "Vector<int8>[1]");
REGISTER_TYPE(i16, std::vector<int16_t>, "Vector<int16>[1]");
REGISTER_TYPE(i32, std::vector<int32_t>, "Vector<int32>[1]");
REGISTER_TYPE(i64, std::vector<int64_t>, "Vector<int64>[1]");
REGISTER_TYPE(str, std::vector<std::string>, "Vector<string>[1]");
REGISTER_TYPE(b, std::vector<bool>, "Vector<bool>[1]");
REGISTER_TYPE(c, std::vector<char>, "Vector<char>[1]");
REGISTER_TYPE(f, std::vector<float>, "Vector<float>[1]");
REGISTER_TYPE(d, std::vector<double>, "Vector<double>[1]");
ALIAS_TYPE(i, std::vector<int>, "Vector<int>[1]");
ALIAS_TYPE(uc, std::vector<unsigned char>, "Vector<uchar>[1]");
ALIAS_TYPE(ui, std::vector<unsigned int>, "Vector<uint>[1]");
