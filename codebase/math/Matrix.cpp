//
// Created by qadwu on 6/1/19.
//

#include "Matrix.h"

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Explicit Instantiation                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template struct Matrix3x3<float>;
template struct Matrix3x3<double>;
template struct Matrix4x4<float>;
template struct Matrix4x4<double>;

} // namespace math
} // namespace vidi
