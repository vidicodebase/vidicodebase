//
// Created by qadwu on 6/1/19.
//

#include "Range.h"
#include <vidiBase.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Explicit Instantiation                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template class Range<int32_t>;
template class Range<float>;
template class Range<double>;

} // namespace math
} // namespace vidi

REGISTER_TYPE_SIMPLE(0, range1i);
REGISTER_TYPE_SIMPLE(1, range1f);
REGISTER_TYPE_SIMPLE(2, range1d);
