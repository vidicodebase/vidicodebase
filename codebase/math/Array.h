//
// Created by qadwu on 5/30/19.
//

#ifndef VIDICODEBASE_MATH_ARRAY_H
#define VIDICODEBASE_MATH_ARRAY_H

#include "Vector.h"
#include <vector>

namespace vidi {
namespace math {

template<typename T, int N = 1> class Array {
public:
    Array(T* ptr, Vector<size_t, N> len) : data(ptr), size(len) {}

public:
    Vector<size_t, N> size;
    T*                data;
};

template<typename T> class Array<T, 1> {
public:
    Array(T* ptr, size_t len) : data(ptr), size(len) {}
    Array(std::vector<T>& vec) : data(vec.data()), size(vec.size()) {}

public:
    size_t size;
    T*     data;
};

} // namespace math
} // namespace vidi

#endif // VIDICODEBASE_MATH_ARRAY_H
