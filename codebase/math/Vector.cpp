//
// Created by qadwu on 6/1/19.
//

#include "Vector.h"
#include <vidiBase.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Explicit Instantiation                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

namespace vidi {
namespace math {

template struct Vector<int32_t, 2, false>;
template struct Vector<float, 2, false>;
template struct Vector<double, 2, false>;

template struct Vector<int32_t, 3, false>;
template struct Vector<float, 3, false>;
template struct Vector<double, 3, false>;

template struct Vector<int32_t, 3, true>;
template struct Vector<float, 3, true>;
template struct Vector<double, 3, true>;

template struct Vector<int32_t, 4, false>;
template struct Vector<float, 4, false>;
template struct Vector<double, 4, false>;

} // namespace math
} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Others                                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

REGISTER_TYPE_SIMPLE(20, vec2b);
REGISTER_TYPE_SIMPLE(21, vec2i);
REGISTER_TYPE_SIMPLE(22, vec2f);
REGISTER_TYPE_SIMPLE(23, vec2d);
REGISTER_TYPE_SIMPLE(30, vec3b);
REGISTER_TYPE_SIMPLE(31, vec3i);
REGISTER_TYPE_SIMPLE(32, vec3f);
REGISTER_TYPE_SIMPLE(33, vec3d);
REGISTER_TYPE_SIMPLE(40, vec4b);
REGISTER_TYPE_SIMPLE(41, vec4i);
REGISTER_TYPE_SIMPLE(42, vec4f);
REGISTER_TYPE_SIMPLE(43, vec4d);
