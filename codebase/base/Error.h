//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_BASE_ERROR_H
#define VIDICODEBASE_BASE_ERROR_H

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4275)
#endif

#include <vidi_base_export.h>

#include <initializer_list>
#include <sstream>
#include <stdexcept>
#include <string>

namespace vidi {

/**
 * The basic type for error handling. Because this class is a derived class of
 * STL, we make it inline.
 */
class VIDI_BASE_EXPORT Exception : public std::runtime_error {
public:
    /**
     * @brief Destructors
     */
    ~Exception() noexcept override = default;

    /**
     * @brief Default constructor
     * @details Constructs an empty error. The default message is "unknown"
     */
    Exception() : std::runtime_error("unknown exception") {}

    /**
     * @brief Constructs the error with multiple stringifiable items
     */
    template<typename... Ts> explicit Exception(Ts... rest) : runtime_error(merge(rest...)) {}

protected:
    template<typename T> std::string convert_to_string(T const& value)
    {
        std::ostringstream oss;
        oss << value << " ";
        return oss.str();
    }

    template<typename... Args> std::string merge(Args&&... args)
    {
        std::string result;
        using std::to_string;
        int unpack[]{ 0, (result += convert_to_string(std::forward<Args>(args)), 0)... };
        static_cast<void>(unpack);
        return result.substr(0, result.size()-1);
    }
};

class TypeError : public Exception {
public:
    ~TypeError() noexcept override = default;
    template<typename... Ts> explicit TypeError(Ts... rest) : Exception("[TypeError]", rest...) {}
};

class LookupError : public Exception {
public:
    ~LookupError() noexcept override = default;
    template<typename... Ts> explicit LookupError(Ts... rest) : Exception("[LookupError]", rest...) {}
};

class ValueError : public Exception {
public:
    ~ValueError() noexcept override = default;
    template<typename... Ts> explicit ValueError(Ts... rest) : Exception("[ValueError]", rest...) {}
};

class NameError : public Exception {
public:
    ~NameError() noexcept override = default;
    template<typename... Ts> explicit NameError(Ts... rest) : Exception("[NameError]", rest...) {}
};

} // namespace vidi

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif // VIDICODEBASE_BASE_ERROR_H
