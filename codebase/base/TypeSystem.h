//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_BASE_TYPESYSTEM_H
#define VIDICODEBASE_BASE_TYPESYSTEM_H

#include "Error.h"
#include <functional>
#include <memory>
#include <string>
#include <typeinfo>
#include <unordered_map>
#include <vidiAny.h>

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Handlers                                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
namespace vidi {

namespace details {
extern std::unordered_map<std::string, size_t> typeMap;
}

/**
 * Query type name from std::type_info
 */
std::string
getTypeName(const std::type_info& type);

/**
 * Query type name from type hash code
 */
std::string
getTypeName(size_t code);

/**
 * Query type hash code from std::type_info
 */
size_t
getTypeCode(const std::type_info& type);

/**
 * Query type hash code from type name
 */
size_t
getTypeCode(const std::string& name);

/**
 * Query type name from C++ type
 */
template<typename T>
std::string
getTypeName() noexcept;

/**
 * Query type hash code from C++ type
 */
template<typename T>
size_t
getTypeCode() noexcept
{
    return typeid(T).hash_code();
}

/**
 * Register a new type name
 * @param name
 * @return true
 */
bool
registerType(const std::string& name, const std::type_info&) noexcept;

/**
 * Check if type has been registered
 */
bool isTypeRegistered(const std::type_info&) noexcept;

/**
 * Check if type has been registered
 */
bool isTypeRegistered(size_t) noexcept;

/**
 * Check if type has been registered
 */
bool isTypeRegistered(const std::string& name) noexcept;

} // namespace vidi

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Macros                                                                    //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
// clang-format off

#define SPECIALIZE_TYPE(Id, Type, StrName)                                     \
namespace vidi {                                                               \
template<> std::string getTypeName<Type>() noexcept { return StrName; }        \
}

#define ALIAS_TYPE(Id, Type, StrName)                                          \
namespace vidi {                                                               \
namespace typesystem {                                                         \
static const bool _##Id##_t_register_ = /* NOLINT(cert-err58-cpp) */           \
  registerType(StrName, typeid(Type));                                         \
}                                                                              \
}

#define REGISTER_TYPE(Id, Type, StrName)                                       \
SPECIALIZE_TYPE(Id, Type, StrName)                                             \
ALIAS_TYPE(Id, Type, StrName)

#define REGISTER_TYPE_SIMPLE(id, x) REGISTER_TYPE(id, x, #x)

// clang format on

#endif // VIDICODEBASE_BASE_TYPESYSTEM_H
