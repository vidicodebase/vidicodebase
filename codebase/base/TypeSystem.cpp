//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#include "TypeSystem.h"
#include "Log.h"

#include <mutex>
#include <set>

//============================================================================//
//                                                                            //
//  Errors                                                                    //
//                                                                            //
//============================================================================//
namespace vidi {
class TypeSystemError : public TypeError {
public:
    ~TypeSystemError() noexcept override = default;

    template<typename... Ts>
    explicit TypeSystemError(const std::type_info& type, Ts... rest)
      : TypeError("unregistered type", type.name(), type.hash_code(), rest...)
    {
    }

    template<typename... Ts>
    explicit TypeSystemError(size_t code, Ts... rest)
      : TypeError("unknown type code", code, rest...)
    {
    }

    template<typename... Ts>
    explicit TypeSystemError(std::string name, Ts... rest)
      : TypeError("unknown type name", std::move(name), rest...)
    {
    }
};
} // namespace vidi

//============================================================================//
//                                                                            //
//  Static Variables                                                          //
//                                                                            //
//============================================================================//

#define DEBUG 1

namespace vidi {
namespace details {
std::unordered_map<std::string, size_t>           typeNameMap;
std::unordered_map<size_t, std::set<std::string>> typeHashMap;
} // namespace details
} // namespace vidi

using vidi::details::typeHashMap;
using vidi::details::typeNameMap;

//============================================================================//
//                                                                            //
//  Function Definitions                                                      //
//                                                                            //
//============================================================================//
namespace vidi {

bool
registerType(const std::string& name, const std::type_info& info) noexcept
{
    const auto code = info.hash_code();
    if (typeNameMap.count(name) != 0) {
        forcewarn() << "type name " << name << " has been taken." << std::endl;
    }
    typeNameMap[name] = code;
    if (typeHashMap.count(code) != 0) {
        typeHashMap[code].insert(name);
    } else {
        typeHashMap[code] = { name };
    }
    return true;
}

bool
isTypeRegistered(const std::type_info& type) noexcept
{
    return typeHashMap.count(type.hash_code()) != 0;
}

bool isTypeRegistered(size_t code) noexcept
{
    return typeHashMap.count(code) != 0;
}

bool isTypeRegistered(const std::string& name) noexcept
{
    return typeNameMap.count(name) != 0;
}

std::string
getTypeName(const std::type_info& type)
{
    return getTypeName(type.hash_code());
}

std::string
getTypeName(size_t code)
{
    if (typeHashMap.count(code) != 0) {
        return *(typeHashMap.at(code).begin());
    }
    throw TypeSystemError(code, "found in getTypeName");
}

size_t
getTypeCode(const std::type_info& type)
{
    return type.hash_code(); // :-D
}

size_t
getTypeCode(const std::string& name)
{
    if (typeNameMap.count(name) != 0) {
        return typeNameMap[name];
    }
    else {
        throw TypeSystemError(name, "found in getTypeCode");
    }
}

} // namespace vidi

//============================================================================//
//                                                                            //
//  Register C Types                                                          //
//                                                                            //
//============================================================================//

REGISTER_TYPE(ui8, uint8_t, "uint8");
REGISTER_TYPE(ui16, uint16_t, "uint16");
REGISTER_TYPE(ui32, uint32_t, "uint32");
REGISTER_TYPE(ui64, uint64_t, "uint64");
REGISTER_TYPE(i8, int8_t, "int8");
REGISTER_TYPE(i16, int16_t, "int16");
REGISTER_TYPE(i32, int32_t, "int32");
REGISTER_TYPE(i64, int64_t, "int64");
REGISTER_TYPE(str, std::string, "string");

REGISTER_TYPE_SIMPLE(b, bool);
REGISTER_TYPE_SIMPLE(c, char);
REGISTER_TYPE_SIMPLE(f, float);
REGISTER_TYPE_SIMPLE(d, double);

ALIAS_TYPE(i, int, "int")
ALIAS_TYPE(ui, unsigned int, "uint")
ALIAS_TYPE(uc, unsigned char, "uchar")
