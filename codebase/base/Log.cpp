//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#include "Log.h"
#include "vidiMPI.h"
#include <fstream>

///////////////////////////////////////////////////////////////////////////////

static std::fstream filestream;
inline bool
DoesFileExist(const std::string& name)
{
    std::ifstream file(name.c_str());
    return file.good();
}

namespace vidi {
namespace details {

template<> Log<STDOUT>::Log() : _os(&std::cout), _msg_begin(), _msg_end() {}

template<> Log<STDERR>::Log() : _os(&std::cerr), _msg_begin("\033[1;33m"), _msg_end("\033[0m") {}

template<> Log<LOGFILE>::Log() : _os(&filestream), _msg_begin(), _msg_end()
{
    if (mpi::GetSize() > 1) {
        filestream.open("vidi-rank-" + std::to_string(mpi::GetRank()) + ".txt", std::fstream::out);
    }
    else {
        filestream.open("vidi-log.txt", std::fstream::out);
    }
}

template<> Log<NULLFILE>::Log() : _os(nullptr), _msg_begin(), _msg_end() {}

template<>
Log<NULLFILE>&
Log<NULLFILE>::operator<<(std::ostream& (*fp)(std::ostream&))
{
    return *this;
}

template<>
Log<NULLFILE>&
Log<NULLFILE>::operator<<(ios_type& (*fp)(ios_type&))
{
    return *this;
}

template<>
Log<NULLFILE>&
Log<NULLFILE>::operator<<(std::ios_base& (*fp)(std::ios_base&))
{
    return *this;
}

} // namespace details
} // namespace vidi

///////////////////////////////////////////////////////////////////////////////

namespace vidi {

template class details::Log<details::STDOUT>;
template class details::Log<details::STDERR>;

} // namespace vidi
