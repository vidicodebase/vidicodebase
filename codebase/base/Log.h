//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_BASE_LOG_H
#define VIDICODEBASE_BASE_LOG_H

#include <iostream>
#include <vidi_base_export.h>

namespace vidi {
namespace details {

enum log_mode_t { STDOUT, STDERR, LOGFILE, NULLFILE };

template<int MODE> class Log {
    typedef std::basic_ios<std::ostream::char_type, std::ostream::traits_type> ios_type;

public:
    ~Log()          = default;
    Log(const Log&) = delete;
    Log& operator=(const Log&) = delete;
    Log& operator=(Log&&) noexcept = delete;

    template<class T> Log& operator<<(const T& t);

    Log& operator<<(std::ostream& (*fp)(std::ostream&));
    Log& operator<<(ios_type& (*fp)(ios_type&));
    Log& operator<<(std::ios_base& (*fp)(std::ios_base&));

    static Log& get();

private:
    Log(Log&&) noexcept = default;
    Log();

private:
    std::ostream* _os;
    std::string   _msg_begin;
    std::string   _msg_end;
};

template<int MODE>
template<class T>
Log<MODE>&
Log<MODE>::operator<<(const T& t)
{
    *_os << _msg_begin << t << _msg_end;
    return *this;
}

template<int MODE>
Log<MODE>&
Log<MODE>::operator<<(std::ostream& (*fp)(std::ostream&))
{
    *_os << _msg_begin << fp << _msg_end;
    return *this;
}

template<int MODE>
Log<MODE>&
Log<MODE>::operator<<(ios_type& (*fp)(ios_type&))
{
    *_os << _msg_begin << fp << _msg_end;
    return *this;
}

template<int MODE>
Log<MODE>&
Log<MODE>::operator<<(std::ios_base& (*fp)(std::ios_base&))
{
    *_os << _msg_begin << fp << _msg_end;
    return *this;
}

template<int MODE>
Log<MODE>&
Log<MODE>::get()
{
    static details::Log<MODE> instance = details::Log<MODE>();
    return instance;
}

template<> Log<STDOUT>::Log();
template<> Log<STDERR>::Log();
template<> Log<LOGFILE>::Log();
template<> Log<NULLFILE>::Log();

template<>
template<class T>
Log<NULLFILE>&
Log<NULLFILE>::operator<<(const T& t)
{
    return *this;
}

template<>
Log<NULLFILE>&
Log<NULLFILE>::operator<<(std::ostream& (*fp)(std::ostream&));

template<>
Log<NULLFILE>&
Log<NULLFILE>::operator<<(ios_type& (*fp)(ios_type&));

template<>
Log<NULLFILE>&
Log<NULLFILE>::operator<<(std::ios_base& (*fp)(std::ios_base&));

} // namespace details
} // namespace vidi

// convenience function to get Log instance
namespace vidi {

#ifndef NDEBUG
inline details::Log<details::STDOUT>&
log()
{
    return details::Log<details::STDOUT>::get();
}
inline details::Log<details::STDERR>&
warn()
{
    return details::Log<details::STDERR>::get();
}
#else
inline details::Log<details::NULLFILE>&
log()
{
    return details::Log<details::NULLFILE>::get();
}
inline details::Log<details::NULLFILE>&
warn()
{
    return details::Log<details::NULLFILE>::get();
}
#endif

inline details::Log<details::STDOUT>&
forcelog()
{
    return details::Log<details::STDOUT>::get();
}

inline details::Log<details::STDERR>&
forcewarn()
{
    return details::Log<details::STDERR>::get();
}

inline details::Log<details::LOGFILE>&
logfile()
{
    return details::Log<details::LOGFILE>::get();
}

inline details::Log<details::NULLFILE>&
lognull()
{
    return details::Log<details::NULLFILE>::get();
}

} // namespace vidi

#endif // VIDICODEBASE_BASE_LOG_H
