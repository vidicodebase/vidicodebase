//
// Created by Qi Wu on 2019-07-06.
//

#ifndef VIDICODEBASE_BASE_MPI_H
#define VIDICODEBASE_BASE_MPI_H

#include <vidiMarco.h>

#ifdef VIDI_USE_MPI
#include <mpi.h>
#endif

#include <algorithm>
#include <cstdint>

// ---------------------------------------------------------------------------------

#define MPI_DEBUG                                                                 \
    std::cout << __FILE__ << " line " << __LINE__ << " " << __FUNCTION__ << "\n"; \
    mpi::Barrier();

// ---------------------------------------------------------------------------------

#ifdef VIDI_USE_MPI
#define _MPI_COMM_WORLD MPI_COMM_WORLD
#else
#define _MPI_COMM_WORLD 0
#define MPI_COMM_WORLD _MPI_COMM_WORLD
typedef enum _MPI_Datatype {
    MPI_DATATYPE_NULL,
    MPI_CHAR,
    MPI_UNSIGNED_CHAR,
    MPI_SHORT,
    MPI_UNSIGNED_SHORT,
    MPI_INT,
    MPI_UNSIGNED,
    MPI_LONG,
    MPI_UNSIGNED_LONG,
    MPI_LONG_LONG_INT,
    MPI_LONG_LONG,
    MPI_FLOAT,
    MPI_DOUBLE,
    MPI_LONG_DOUBLE,
    MPI_BYTE,
    MPI_WCHAR,
    MPI_PACKED,
    MPI_LB,
    MPI_UB,
    MPI_C_COMPLEX,
    MPI_C_FLOAT_COMPLEX,
    MPI_C_DOUBLE_COMPLEX,
    MPI_C_LONG_DOUBLE_COMPLEX,
    MPI_2INT,
    MPI_C_BOOL,
    MPI_SIGNED_CHAR,
    MPI_UNSIGNED_LONG_LONG,
    MPI_CHARACTER,
    MPI_INTEGER,
    MPI_REAL,
    MPI_LOGICAL,
    MPI_COMPLEX,
    MPI_DOUBLE_PRECISION,
    MPI_2INTEGER,
    MPI_2REAL,
    MPI_DOUBLE_COMPLEX,
    MPI_2DOUBLE_PRECISION,
    MPI_2COMPLEX,
    MPI_2DOUBLE_COMPLEX,
    MPI_REAL2,
    MPI_REAL4,
    MPI_COMPLEX8,
    MPI_REAL8,
    MPI_COMPLEX16,
    MPI_REAL16,
    MPI_COMPLEX32,
    MPI_INTEGER1,
    MPI_COMPLEX4,
    MPI_INTEGER2,
    MPI_INTEGER4,
    MPI_INTEGER8,
    MPI_INTEGER16,
    MPI_INT8_T,
    MPI_INT16_T,
    MPI_INT32_T,
    MPI_INT64_T,
    MPI_UINT8_T,
    MPI_UINT16_T,
    MPI_UINT32_T,
    MPI_UINT64_T,
    MPI_AINT,
    MPI_OFFSET,
    MPI_FLOAT_INT,
    MPI_DOUBLE_INT,
    MPI_LONG_INT,
    MPI_SHORT_INT,
    MPI_LONG_DOUBLE_INT
} MPI_Datatype;
typedef enum _MPI_Op {
    MPI_OP_NULL,
    MPI_MAX,
    MPI_MIN,
    MPI_SUM,
    MPI_PROD,
    MPI_LAND,
    MPI_BAND,
    MPI_LOR,
    MPI_BOR,
    MPI_LXOR,
    MPI_BXOR,
    MPI_MINLOC,
    MPI_MAXLOC,
    MPI_REPLACE
} MPI_Op;
#endif

// ---------------------------------------------------------------------------------

namespace vidi {
namespace mpi {

// ---------------------------------------------------------------------------------
#ifdef VIDI_USE_MPI
typedef MPI_Comm comm_t;
#else
typedef int comm_t; // doesn't really matter because MPI is not used
#endif
typedef MPI_Datatype data_t;
typedef MPI_Op       op_t;

// ---------------------------------------------------------------------------------
// helper functions to convert C++ type to MPI type
template<typename T>
MPI_Datatype
DataType();

#define _define_mpi_data_type(type, mpi_type)                            \
    template<> inline MPI_Datatype DataType<type>() { return mpi_type; }

_define_mpi_data_type(char, MPI_CHAR);
_define_mpi_data_type(unsigned char, MPI_UNSIGNED_CHAR);   // == uint8_t
_define_mpi_data_type(short, MPI_SHORT);                   // == int16_t
_define_mpi_data_type(unsigned short, MPI_UNSIGNED_SHORT); // == uint16_t
_define_mpi_data_type(int, MPI_INT);                       // == int32_t
_define_mpi_data_type(unsigned, MPI_UNSIGNED);             // == uint32_t
_define_mpi_data_type(long, MPI_LONG);                     // == uint64_t
_define_mpi_data_type(unsigned long, MPI_UNSIGNED_LONG);   // == uint64_t
_define_mpi_data_type(long long int, MPI_LONG_LONG_INT);
// MPI_LONG_LONG
_define_mpi_data_type(float, MPI_FLOAT);
_define_mpi_data_type(double, MPI_DOUBLE);
_define_mpi_data_type(long double, MPI_LONG_DOUBLE);
// _define_mpi_data_type(char, MPI_BYTE);
// _define_mpi_data_type(wchar_t, MPI_WCHAR);
// MPI_PACKED
// MPI_LB
// MPI_UB
// MPI_C_COMPLEX
// MPI_C_FLOAT_COMPLEX
// MPI_C_DOUBLE_COMPLEX
// MPI_C_LONG_DOUBLE_COMPLEX
// MPI_2INT
// MPI_C_BOOL
_define_mpi_data_type(signed char, MPI_SIGNED_CHAR); // == int8_t
_define_mpi_data_type(unsigned long long, MPI_UNSIGNED_LONG_LONG);
// MPI_CHARACTER
// MPI_INTEGER
// MPI_REAL
// MPI_LOGICAL
// MPI_COMPLEX
// MPI_DOUBLE_PRECISION
// MPI_2INTEGER
// MPI_2REAL
// MPI_DOUBLE_COMPLEX
// MPI_2DOUBLE_PRECISION
// MPI_2COMPLEX
// MPI_2DOUBLE_COMPLEX
// MPI_REAL2
// MPI_REAL4
// MPI_COMPLEX8
// MPI_REAL8
// MPI_COMPLEX16
// MPI_REAL16
// MPI_COMPLEX32
// MPI_INTEGER1
// MPI_COMPLEX4
// MPI_INTEGER2
// MPI_INTEGER4
// MPI_INTEGER8
// MPI_INTEGER16
// _define_mpi_data_type(int8_t, MPI_INT8_T);
// _define_mpi_data_type(int16_t, MPI_INT16_T);
// _define_mpi_data_type(int32_t, MPI_INT32_T);
// _define_mpi_data_type(int64_t, MPI_INT64_T);
// _define_mpi_data_type(uint8_t, MPI_UINT8_T);
// _define_mpi_data_type(uint16_t, MPI_UINT16_T);
// _define_mpi_data_type(uint32_t, MPI_UINT32_T);
// _define_mpi_data_type(uint64_t, MPI_UINT64_T);
// MPI_AINT
// MPI_OFFSET
// MPI_FLOAT_INT
// MPI_DOUBLE_INT
// MPI_LONG_INT
// MPI_SHORT_INT
// MPI_LONG_DOUBLE_INT

#undef _define_mpi_data_type

// ---------------------------------------------------------------------------------
// reference https://www.mpich.org/static/docs/v3.2.x/www3/Constants.html
inline size_t
SizeOf(data_t type)
{
    if (type == MPI_CHAR)
        return sizeof(char);
    if (type == MPI_SIGNED_CHAR)
        return sizeof(signed char);
    if (type == MPI_UNSIGNED_CHAR)
        return sizeof(unsigned char);
    if (type == MPI_BYTE)
        return sizeof(char);
    if (type == MPI_SHORT)
        return sizeof(short);
    if (type == MPI_UNSIGNED_SHORT)
        return sizeof(unsigned short);
    if (type == MPI_INT)
        return sizeof(int);
    if (type == MPI_UNSIGNED)
        return sizeof(unsigned int);
    if (type == MPI_LONG)
        return sizeof(long);
    if (type == MPI_UNSIGNED_LONG)
        return sizeof(unsigned long);
    if (type == MPI_LONG_LONG_INT)
        return sizeof(long long);
    if (type == MPI_LONG_LONG)
        return sizeof(long long);
    if (type == MPI_UNSIGNED_LONG_LONG)
        return sizeof(unsigned long long);
    if (type == MPI_FLOAT)
        return sizeof(float);
    if (type == MPI_DOUBLE)
        return sizeof(double);
    if (type == MPI_LONG_DOUBLE)
        return sizeof(long double);
    throw std::runtime_error("unknown MPI data type");
}

// ---------------------------------------------------------------------------------
inline bool
Init(int* argc = nullptr, char*** argv = nullptr)
{
#ifdef VIDI_USE_MPI
    MPI_Init(nullptr, nullptr);
    return true;
#else
    return true;
#endif
}

inline bool
Finalize()
{
#ifdef VIDI_USE_MPI
    return MPI_Finalize() == MPI_SUCCESS;
#else
    return true;
#endif
}

inline int
GetRank(comm_t comm = _MPI_COMM_WORLD)
{
#ifdef VIDI_USE_MPI
    int rank = 0;
    int flag;
    MPI_Initialized(&flag);
    if (flag)
        MPI_Comm_rank(comm, &rank);
    return rank;
#else
    return 0;
#endif
}

inline int
GetSize(comm_t comm = _MPI_COMM_WORLD)
{
#ifdef VIDI_USE_MPI
    int size = 1;
    int flag;
    MPI_Initialized(&flag);
    if (flag)
        MPI_Comm_size(comm, &size);
    return size;
#else
    return 1;
#endif
}

inline void
Barrier(comm_t comm = _MPI_COMM_WORLD)
{
#ifdef VIDI_USE_MPI
    int flag;
    MPI_Initialized(&flag);
    if (flag)
        MPI_Barrier(comm);
#endif
}

inline void
Allreduce(void* sendbuf, void* recvbuf, int count, data_t datatype, op_t op, comm_t comm)
{
#ifdef VIDI_USE_MPI
    int flag;
    MPI_Initialized(&flag);
    if (flag)
        int ierr = MPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
#else
    std::copy((char*)recvbuf, ((char*)recvbuf) + SizeOf(datatype) * count, (char*)sendbuf);
#endif
}

template<typename T>
inline void
Allreduce(T* sendbuf, T* recvbuf, int count, op_t op, comm_t comm)
{
#ifdef VIDI_USE_MPI
    int flag;
    MPI_Initialized(&flag);
    if (flag)
        int ierr = MPI_Allreduce(sendbuf, recvbuf, count, DataType<T>(), op, comm);
#else
    std::copy((char*)recvbuf, ((char*)recvbuf) + sizeof(T) * count, (char*)sendbuf);
#endif
}

} // namespace mpi
} // namespace vidi

#endif // VIDICODEBASE_BASE_MPI_H
