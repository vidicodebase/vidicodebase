//
// Created by Qi Wu on 2019-06-27.
//

#ifndef VIDICODEBASE_VIDIMARCO_H
#define VIDICODEBASE_VIDIMARCO_H

#define _STRING(x) #x

#define TOSTRING(x) _STRING(x)

#define CODE_LOCATION __FILE__ " (" TOSTRING(__LINE__) ")"

#define FUNC_LOCATION "[" + std::string(__FUNCTION__) + "]"

#define PING                                                                  \
    {                                                                         \
        std::stringstream msg;                                                \
        msg << "PING " << FUNC_LOCATION << " " << CODE_LOCATION << std::endl; \
        ::vidi::warn() << msg.str();                                          \
    }

#define ERROR_UNIMPLEMENTED                                                                    \
    {                                                                                          \
        std::stringstream msg;                                                                 \
        msg << "Unimplemented Feature " << FUNC_LOCATION << " " << CODE_LOCATION << std::endl; \
        throw std::runtime_error(msg.str());                                                   \
    }

#define WARN_ILLOGICAL                                                       \
    ::vidi::warn() << "This function should not be called. calling it will " \
                      "have no effect.";

#ifndef VIDI_USE_CXX_STD_14
#define VIDI_DEPRECATED_API
#else
#define VIDI_DEPRECATED_API [[deprecated]]
#endif

#include <sstream>

#endif // VIDICODEBASE_VIDIMARCO_H
