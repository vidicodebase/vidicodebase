//
// Created by Qi Wu on 2019-06-26.
//

#ifndef VIDICODEBASE_VIDISERIALIZABLE_H
#define VIDICODEBASE_VIDISERIALIZABLE_H

#include "serializable/Functions.h"
#include "serializable/JsonParser.h"
#include "serializable/JsonSerialization.h"
#include "serializable/TraitSerializable.h"
#include "serializable/TraitToString.h"

#endif // VIDICODEBASE_VIDISERIALIZABLE_H
