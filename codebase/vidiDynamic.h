//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#pragma once

#include "dynamic/Library.h"
#include "dynamic/ObjectFactory.h"
#include "dynamic/Parameter.h"
#include "dynamic/TraitExternallyNamed.h"
#include "dynamic/TraitParameterized.h"

#include <vidiMarco.h>

// clang-format off
#define VIDI_REGISTER_OBJECT(Object, object_name, InternalClass, external_name)\
  extern "C" /*VIDI_EXPORT*/                                                   \
      Object *vidi_create_##object_name##__##external_name()                   \
  {                                                                            \
    auto *instance = new InternalClass;                                        \
    instance->addExternalName(TOSTRING(external_name));                        \
    return instance;                                                           \
  }                                                                            \
  /* additional declaration to avoid "extra ;" -Wpedantic warnings */          \
  Object *vidi_create_##object_name##__##external_name();
// clang-format on

namespace vidi {
/**
 * @brief The ViDi tools for implementing dynamically loadable modules
 */
namespace dynamic {
}
} // namespace vidi
