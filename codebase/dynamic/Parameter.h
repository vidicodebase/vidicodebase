//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#pragma once

#include <vidi_base_export.h>

#include <functional>
#include <memory>
#include <string>

#define SETTER_T std::function<void(T)>
#define GETTER_T std::function<T()>

namespace vidi {
namespace dynamic {

/**
 * @brief An untyped parameter
 */
class Parameter {
public:
    /** @name Special Functions */
    /// @{
    virtual ~Parameter();
    Parameter(Parameter&&) noexcept;
    Parameter& operator=(Parameter&&) noexcept;
    Parameter(const Parameter&);
    Parameter& operator=(const Parameter&);
    explicit Parameter(std::string name);
    ///@}

    /**
     * Manually set the variable dirtiness.
     * @param flag
     */
    void setDirty(bool flag = true);

    /**
     * Manually check the variable's dirtiness.
     * @return
     */
    bool isDirty() const;

protected:
    std::string _name;         //!< name under which this parameter is registered
    bool        _dirty = true; //!< variable dirtiness
};

/**
 * @brief A typed parameter with setter and getter
 */
template<typename T> class ParameterTyped : public Parameter {
public:
    using setter_t = SETTER_T;
    using getter_t = GETTER_T;

public:
    explicit ParameterTyped(std::string name, getter_t getter, setter_t setter);

public:
    getter_t _getter;
    setter_t _setter;
};

///////////////////////////////////////////////////////////////////////////////

template<typename T>
ParameterTyped<T>::ParameterTyped(std::string name, getter_t getter, setter_t setter)
  : Parameter(std::move(name)), _getter(std::move(getter)), _setter(std::move(setter))
{
}

///////////////////////////////////////////////////////////////////////////////

} // namespace dynamic
} // namespace vidi
