//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#pragma once

#include "Parameter.h"

#include <vidi_base_export.h>

#include <vidiBase.h>
#include <vidiMath.h>

#include <unordered_map>
#include <vector>

namespace vidi {
namespace dynamic {

/**
 * @brief defines an object who can take typed parameters
 */
class TraitParameterized {
public:
    using all_t                     = std::shared_ptr<Parameter>;
    template<class T> using param_t = std::shared_ptr<ParameterTyped<T>>;

public:
    /** @name Special Functions */
    ///@{
    virtual ~TraitParameterized()                     = default;
    TraitParameterized(TraitParameterized&&) noexcept = default;
    TraitParameterized& operator=(TraitParameterized&&) noexcept = default;
    TraitParameterized(const TraitParameterized&)                = default;
    TraitParameterized& operator=(const TraitParameterized&) = default;
    TraitParameterized();
    ///@}

    /**
     * This is to check if an input parameter has been updated
     * @param name
     * @return
     */
    bool checkAndUpdateParameter(const std::string& name);

    /**
     * @brief Check if a given name maps to a defined parameter
     * @param name
     * @return
     */
    bool hasParameter(const std::string& name) const noexcept;

    /**
     * @brief Set a parameter with given name to given value.
     * @tparam T
     * @param name
     * @param val
     */
    template<typename T> void setParameter(const std::string& name, const T& val);

    /**
     * @brief Retrieve a parameter's setter with given name
     * @tparam T
     * @param name
     * @return
     */
    template<typename T> SETTER_T getParameterSetter(const std::string& name);

    /**
     * @brief Retrieve a parameter's getter with given name
     * @tparam T
     * @param name
     * @return
     */
    template<typename T> GETTER_T getParameterGetter(const std::string& name);

    /**
     * @brief Retrieve a parameter value with given name
     * @tparam T
     * @param name
     * @return
     */
    template<typename T> T getParameter(const std::string& name) const;

    /** @name Helper Functions */
    ///@{
    // string
    void        setParameterStr(const std::string& name, const std::string& val);
    std::string getParameterStr(const std::string& name) const;
    // scalars
    bool   getParameter1b(const std::string& name) const;
    int    getParameter1i(const std::string& name) const;
    float  getParameter1f(const std::string& name) const;
    double getParameter1d(const std::string& name) const;
    // vector 2
    vec2b getParameterVec2b(const std::string& name) const;
    vec2i getParameterVec2i(const std::string& name) const;
    vec2f getParameterVec2f(const std::string& name) const;
    vec2d getParameterVec2d(const std::string& name) const;
    // vector 3
    vec3b getParameterVec3b(const std::string& name) const;
    vec3i getParameterVec3i(const std::string& name) const;
    vec3f getParameterVec3f(const std::string& name) const;
    vec3d getParameterVec3d(const std::string& name) const;
    // vector 4
    vec4b getParameterVec4b(const std::string& name) const;
    vec4i getParameterVec4i(const std::string& name) const;
    vec4f getParameterVec4f(const std::string& name) const;
    vec4d getParameterVec4d(const std::string& name) const;
    ///@}

protected:
    /**
     * @brief Create a parameter for the class. This function should be called
     * in children classes' constructors
     * @tparam T
     * @param name
     * @param setter
     * @param getter
     * @param checker
     */
    template<class T> void _defineParameter(const std::string& name, GETTER_T getter, SETTER_T setter);

    /**
     * @brief Create a read-only parameter :)
     * @tparam T
     * @param name
     * @param getter
     */
    template<class T> void _defineParameter(const std::string& name, GETTER_T getter);

    /**
     * @brief Version without a getter
     * @tparam T
     * @param name
     * @param setter
     */
    template<class T> void _defineParameter(const std::string& name, SETTER_T setter);

    /**
     * @brief function to create all parameters. must be called in the
     * constructor.
     */
    virtual void _setup() = 0;

private:
    /**
     * @brief Find out if a given parameter name exists
     * @param name The name of the parameter
     * @param ret The return value
     * @return Whether the parameter name exists
     */
    bool _findParameterName(const std::string& name, all_t& ret) const noexcept;

    /// @brief Find a given parameter. Return a nullptr if not exist in the
    /// release mode.
    template<class T> std::shared_ptr<ParameterTyped<T>> _findParameter(const std::string& name) const;

private:
    /** @brief list of parameters attached to this object */
    std::shared_ptr<std::unordered_map<std::string, all_t>> _paramList;
};

///////////////////////////////////////////////////////////////////////////////

inline void
TraitParameterized::setParameterStr(const std::string& name, const std::string& val)
{
    setParameter<std::string>(name, val);
}

inline std::string
TraitParameterized::getParameterStr(const std::string& name) const
{
    return getParameter<std::string>(name);
}

inline bool
TraitParameterized::getParameter1b(const std::string& name) const
{
    return getParameter<bool>(name);
}
inline int
TraitParameterized::getParameter1i(const std::string& name) const
{
    return getParameter<int>(name);
}
inline float
TraitParameterized::getParameter1f(const std::string& name) const
{
    return getParameter<float>(name);
}
inline double
TraitParameterized::getParameter1d(const std::string& name) const
{
    return getParameter<double>(name);
}

inline vec2b
TraitParameterized::getParameterVec2b(const std::string& name) const
{
    return getParameter<vec2b>(name);
}
inline vec2i
TraitParameterized::getParameterVec2i(const std::string& name) const
{
    return getParameter<vec2i>(name);
}
inline vec2f
TraitParameterized::getParameterVec2f(const std::string& name) const
{
    return getParameter<vec2f>(name);
}
inline vec2d
TraitParameterized::getParameterVec2d(const std::string& name) const
{
    return getParameter<vec2d>(name);
}

inline vec3b
TraitParameterized::getParameterVec3b(const std::string& name) const
{
    return getParameter<vec3b>(name);
}
inline vec3i
TraitParameterized::getParameterVec3i(const std::string& name) const
{
    return getParameter<vec3i>(name);
}
inline vec3f
TraitParameterized::getParameterVec3f(const std::string& name) const
{
    return getParameter<vec3f>(name);
}
inline vec3d
TraitParameterized::getParameterVec3d(const std::string& name) const
{
    return getParameter<vec3d>(name);
}

inline vec4b
TraitParameterized::getParameterVec4b(const std::string& name) const
{
    return getParameter<vec4b>(name);
}
inline vec4i
TraitParameterized::getParameterVec4i(const std::string& name) const
{
    return getParameter<vec4i>(name);
}
inline vec4f
TraitParameterized::getParameterVec4f(const std::string& name) const
{
    return getParameter<vec4f>(name);
}
inline vec4d
TraitParameterized::getParameterVec4d(const std::string& name) const
{
    return getParameter<vec4d>(name);
}

///////////////////////////////////////////////////////////////////////////////

template<typename T>
void
TraitParameterized::setParameter(const std::string& name, const T& val)
{
    auto ptr = _findParameter<T>(name);
    ptr->setDirty();
    ptr->_setter(val);
}

template<typename T>
T
TraitParameterized::getParameter(const std::string& name) const
{
    return _findParameter<T>(name)->_getter();
}

template<typename T>
SETTER_T
TraitParameterized::getParameterSetter(const std::string& name)
{
    return _findParameter<T>(name)->_setter;
}

template<typename T>
GETTER_T
TraitParameterized::getParameterGetter(const std::string& name)
{
    return _findParameter<T>(name)->_getter;
}

///////////////////////////////////////////////////////////////////////////////

template<class T>
std::shared_ptr<ParameterTyped<T>>
TraitParameterized::_findParameter(const std::string& name) const
{
    all_t raw;
    bool  valid = _findParameterName(name, raw);
#ifndef NDEBUG
    if (!valid)
        throw Exception("Invalid parameter name: " + name);
#endif
    auto ptr = std::dynamic_pointer_cast<ParameterTyped<T>>(raw);
    return ptr;
}

template<class T>
void
TraitParameterized::_defineParameter(const std::string& name, GETTER_T getter, SETTER_T setter)
{
    all_t raw;
    bool  valid = _findParameterName(name, raw);
#ifndef NDEBUG
    if (valid) {
        throw Exception("Parameter (" + name + ") has already been created.");
    }
#endif
    auto p              = std::make_shared<ParameterTyped<T>>(name, std::move(getter), std::move(setter));
    (*_paramList)[name] = p;
}

template<class T>
void
TraitParameterized::_defineParameter(const std::string& name, GETTER_T getter)
{
    SETTER_T setter = [=](T v) {
        warn() << "This function should not be called. calling it will have no "
                  "effect.";
    };
    _defineParameter(name, getter, setter);
}

template<class T>
void
TraitParameterized::_defineParameter(const std::string& name, SETTER_T setter)
{
    GETTER_T getter = [=]() {
        warn() << "This function should not be called. calling it "
                  "will have no effect.";
        return T();
    };
    _defineParameter(name, getter, setter);
}

///////////////////////////////////////////////////////////////////////////////

} // namespace dynamic
} // namespace vidi
