//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#include "TraitExternallyNamed.h"

namespace vidi {
namespace dynamic {

TraitExternallyNamed::TraitExternallyNamed() : _externalNames(std::make_shared<name_list_t>()) {}

void
TraitExternallyNamed::addExternalName(std::string name)
{
    _externalNames->emplace_back(name);
}

} // namespace dynamic
} // namespace vidi
