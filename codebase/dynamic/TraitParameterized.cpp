//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#include "TraitParameterized.h"

namespace vidi {
namespace dynamic {

///////////////////////////////////////////////////////////////////////////////

TraitParameterized::TraitParameterized() : _paramList(std::make_shared<std::unordered_map<std::string, all_t>>()) {}

bool
TraitParameterized::checkAndUpdateParameter(const std::string& name)
{
    all_t ret;
    bool  valid = _findParameterName(name, ret);
#ifndef NDEBUG
    if (!valid)
        throw Exception("Invalid parameter name: " + name);
#endif
    if (ret->isDirty()) {
        ret->setDirty(false);
        return true;
    }
    else {
        return false;
    }
}

bool
TraitParameterized::hasParameter(const std::string& name) const noexcept
{
    all_t ret;
    return _findParameterName(name, ret);
}

bool
TraitParameterized::_findParameterName(const std::string& name, all_t& ret) const noexcept
{
    auto it = _paramList->find(name);
    if (it != _paramList->end()) {
        ret = it->second;
        return true;
    }
    else {
        return false;
    }
}

} // namespace dynamic
} // namespace vidi
