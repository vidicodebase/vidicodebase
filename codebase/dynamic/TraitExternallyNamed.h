//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#pragma once

#include <vidi_base_export.h>

#include <memory>
#include <string>
#include <vector>

namespace vidi {
namespace dynamic {

/**
 * @brief defines an object that can be created with an external string name
 */
class TraitExternallyNamed {
    using name_list_t = std::vector<std::string>;

public:
    virtual ~TraitExternallyNamed() = default;
    TraitExternallyNamed();
    void addExternalName(std::string name);

private:
    /** @brief list of names created for this object */
    std::shared_ptr<name_list_t> _externalNames; //!< the special parameter for all objects
};

} // namespace dynamic
} // namespace vidi
