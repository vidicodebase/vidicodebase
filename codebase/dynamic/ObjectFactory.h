//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_DYNAMIC_OBJECTFACTORY_H
#define VIDICODEBASE_DYNAMIC_OBJECTFACTORY_H

#include "Library.h"
#include <memory>
#include <vidiBase.h>

namespace vidi {
/**
 * @brief The ViDi tools for implementing dynamically loadable modules
 */
namespace dynamic {

class UnknownDynamicModuleError : public Exception {
public:
    UnknownDynamicModuleError(const std::string& type, const std::string& name)
      : Exception("Could not find",
                  "'" + type + "'",
                  "of type ",
                  "'" + name + "'.",
                  "Make sure you have the correct libraries linked.")
    {
    }
};

namespace details {

/**
 * The function to create an object from a string
 * @tparam T The base type
 * @param type The external type name
 * @return A new instance of the object
 */
template<typename T>
std::shared_ptr<T>
objectFactory(const std::string& type)
{
    // Function pointer type for creating a concrete instance of a subtype of
    // this class.
    using creationFunctionPointer = T* (*)();

    // Function pointers corresponding to each subtype.
    creationFunctionPointer symbol;

    // type of the abstract class
    const auto tstr = getTypeName<T>();

    // Construct the name of the creation function to look for.
    std::string creationFunctionName = "vidi_create_" + tstr + "__" + type;

    // Load library from the disk
    auto& repo = *LibraryRepository::GetInstance();
    repo.addDefaultLibrary();

    // Look for the named function.
    symbol = (creationFunctionPointer)repo.getSymbol(creationFunctionName);

    // The named function may not be found if the requested subtype is not
    // known.
    // if (!symbol) {
    //     warn() << "unrecognized " << tstr << " type '" << type << "'." << std::endl;
    // }

    // Create a concrete instance of the requested subtype.
    auto* object = symbol ? (*symbol)() : nullptr;

    if (object == nullptr) {
        throw UnknownDynamicModuleError(tstr, type);
    }

    std::shared_ptr<T> ret(object);
    return ret;
}

} // namespace details
} // namespace dynamic
} // namespace vidi

#endif // VIDICODEBASE_DYNAMIC_OBJECTFACTORY_H
