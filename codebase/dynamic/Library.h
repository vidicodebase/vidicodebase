//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson), Min Shih                                //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//
/*
 * The following code is modified based on codes from the ospray project
 */
// ========================================================================= //
// Copyright 2009-2018 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
// ========================================================================= //

#pragma once

#include <vidi_base_export.h>
// std
#include <map>
#include <memory>
#include <string>

namespace vidi {
namespace dynamic {
class LibraryRepository;

/**
 * @brief Internal API
 */
namespace details {
/**
 * This is a class for loading dynamic libraries from the system.
 * Tested systems: Linux
 */
class Library {
private:
    friend class vidi::dynamic::LibraryRepository;

public:
    /* opens a shared library */
    explicit Library(const std::string& name);
    ~Library();
    /* returns address of a symbol from the library */
    void* getSymbol(const std::string& sym) const;

private:
    explicit Library(void* const);
    std::string libraryName;
    void*       lib             = nullptr;
    bool        freeLibOnDelete = true;
};
} // namespace details

/**
 * This is the class for managing all loaded libraries. One can access the
 * global instance of this class using LibraryRepository::GetInstance()
 */
class LibraryRepository {
public:
    /* obtain the global instance */
    static LibraryRepository* GetInstance();

    /* delete the global instance */
    static void CleanupInstance();

    ~LibraryRepository();

    /* add a library to the repo */
    void add(const std::string& name);

    /* remove a library to the repo */
    void remove(const std::string& name);

    /* returns address of a symbol from any library in the repo */
    void* getSymbol(const std::string&) const;

    /* check if the library exists */
    bool libraryExists(const std::string& name) const;

    /* load default stuffs */
    void addDefaultLibrary();

private:
    LibraryRepository() = default;
    std::map<std::string, details::Library*>  repo;
    static std::unique_ptr<LibraryRepository> instance;
};

} // namespace dynamic
} // namespace vidi
