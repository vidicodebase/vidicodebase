//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) Qi Wu (Wilson)                                               //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#include "Parameter.h"

namespace vidi {
namespace dynamic {

Parameter::~Parameter() = default;

Parameter::Parameter(Parameter&& other) noexcept
{
    _name  = std::move(other._name);
    _dirty = other._dirty;
}

Parameter&
Parameter::operator=(Parameter&& other) noexcept
{
    if (this != &other) {
        _name  = std::move(other._name);
        _dirty = other._dirty;
    }
    return *this;
}

Parameter::Parameter(std::string name) : _name{ std::move(name) } {}

Parameter::Parameter(const Parameter&) = default;

Parameter&
Parameter::operator=(const Parameter&) = default;

void
Parameter::setDirty(bool flag)
{
    _dirty = flag;
}

bool
Parameter::isDirty() const
{
    return _dirty;
}

} // namespace dynamic
} // namespace vidi
