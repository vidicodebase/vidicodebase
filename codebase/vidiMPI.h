//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//

#ifndef VIDICODEBASE_VIDIMPI_H
#define VIDICODEBASE_VIDIMPI_H

#include "base/Parallel.h"

#endif // VIDICODEBASE_VIDIMPI_H
