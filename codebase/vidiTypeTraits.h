//===========================================================================//
//                                                                           //
// LibViDi3D                                                                 //
// Copyright(c) 2018 Qi Wu (Wilson)                                          //
// University of California, Davis                                           //
// MIT Licensed                                                              //
//                                                                           //
//===========================================================================//
// clang-format off

#ifndef VIDI_BASE_TYPETRAITS_H
#define VIDI_BASE_TYPETRAITS_H

#include <type_traits>

namespace vidi {

#ifndef VIDI_USE_CXX_STD_14
/**
 * @brief Implementation of std::conditional_t
 * @details Reference https://en.cppreference.com/w/cpp/types/conditional
 */
template<bool B, class T, class F>
using conditional_t = typename std::conditional<B, T, F>::type;
#else
template<bool B, class T, class F>
using conditional_t = std::conditional_t<B, T, F>;
#endif

#ifndef VIDI_USE_CXX_STD_17
/**
 * @brief Implementation of std::conjunction
 * @details https://en.cppreference.com/w/cpp/types/conjunction
 */
template<class...> struct conjunction : std::true_type {
};
template<class B1> struct conjunction<B1> : B1 {
};
template<class B1, class... Bn>
struct conjunction<B1, Bn...>
  : conditional_t<bool(B1::value), conjunction<Bn...>, B1> {
};
#else
template<class B1, class... Bn> using conjunction = std::conjunction<B1, Bn...>;
#endif

#ifndef VIDI_USE_CXX_STD_17
/**
 * @brief Implementation of std::negation
 * @details https://en.cppreference.com/w/cpp/types/negation
 */
template<class B>
struct negation : std::integral_constant<bool, !bool(B::value)> {
};
#else
template<class B> using negation = std::negation<B>;
#endif

} // namespace vidi

#endif // VIDI_BASE_TYPETRAITS_H
