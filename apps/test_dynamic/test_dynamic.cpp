//
// Created by wilson on 10/14/2019.
//
#include "test_lib.h"
#include <vidiDynamic.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

REGISTER_TYPE(0, AbsEchoClass, "AbsEchoClass")

int main() {
    auto repo = vidi::dynamic::LibraryRepository::GetInstance();
    while(true) {
        repo->add("vidi_test_lib");
        auto x = vidi::dynamic::details::objectFactory<AbsEchoClass>("EchoClass");
        x->run();
	    x.reset();
	    repo->remove("vidi_test_lib");
#ifdef _WIN32
        Sleep(5000);
#else
	    sleep(5);
#endif
    }
}
