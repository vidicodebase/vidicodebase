//
// Created by wilson on 10/14/2019.
//
#include <iostream>
#include <vidiDynamic.h>

#include "test_lib.h"

class EchoClass : public AbsEchoClass {
public:
    void run() override
    {
        std::cout << "xxx" << std::endl;
    }
};

VIDI_REGISTER_OBJECT(AbsEchoClass, AbsEchoClass, EchoClass, EchoClass);
