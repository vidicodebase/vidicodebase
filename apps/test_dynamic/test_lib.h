//
// Created by wilson on 10/14/2019.
//

#ifndef VIDICODEBASE_TEST_LIB_H
#define VIDICODEBASE_TEST_LIB_H

#include <vidiDynamic.h>

class AbsEchoClass : public vidi::dynamic::TraitExternallyNamed {
public:
    virtual void run() = 0;
};

#endif // VIDICODEBASE_TEST_LIB_H
