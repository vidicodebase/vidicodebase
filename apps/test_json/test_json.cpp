//
// Created by qadwu on 10/13/19.
//

#include <vidiBase.h>
#include <vidiSerializable.h>

//#include "rapidjson/document.h"
//#include "rapidjson/error/en.h"
//#include <rapidjson/istreamwrapper.h>
#include <fstream>

//namespace rj = ::rapidjson;

#include <iostream>

using vidi::serializable::JsonParser;
using vidi::serializable::JsonValue;

const char* j = "{ //hello\n"
                "\"hello\": \"world\","
                "\"t\": true }";

int
main()
{
//    std::string text = "";
//    std::ifstream myfile ("../apps/test_json/vidi.json");
//    if (myfile.is_open())
//    {
//        std::string line;
//        while ( getline (myfile,line) )
//        {
//            text += line + '\n';
//        }
//        myfile.close();
//    }
//    else std::cout << "Unable to open file";
//
//    std::cout << text << std::endl;
//
//    std::ifstream ifs("../apps/test_json/vidi.json");
//    rj::IStreamWrapper isw(ifs);
//
//    rj::Document document;
//    if (document.ParseStream<rj::kParseCommentsFlag>(isw).HasParseError()) {
//        fprintf(stderr, "\nError(offset %u): %s\n",
//                (unsigned)document.GetErrorOffset(),
//                rj::GetParseError_En(document.GetParseError()));
//        // ...
//    }
//
//    std::cout << document.IsObject() << std::endl;
//    static const char* kTypeNames[] =
//      { "Null", "False", "True", "Object", "Array", "String", "Number" };
//    for (auto& m : document.GetObject())
//        printf("Type of member %s is %s\n",
//               m.name.GetString(), kTypeNames[m.value.GetType()]);
//
//    unsigned int x;
//    std::stringstream ss;
//    ss << std::hex << "fffefffe";
//    ss >> x;
//
//    std::cout << x << std::endl;

//    JsonParser parser;
//    JsonValue input;
//    input = 1;
//    // std::cout << input.isBool() << std::endl;
//
//    if (!parser.load("../apps/test_json/vidi.json", input))
//        throw vidi::Exception("failed to open JSON");
//
//    std::cout << parser.stringify(input) << std::endl;
//    auto& jobj = input.toObject();
//    for (auto it = jobj.begin(); it != jobj.end(); ++it) {
//        std::cout << parser.stringify((*it).second) << std::endl;
//    }
//
//    for (auto it : input) {
//        std::cout << parser.stringify(it) << std::endl;
//    }
//    input["numbers-f1"];
//    std::cout << parser.stringify(input.fetch("numbers-f1", 3)) << std::endl;
//    std::cout << parser.stringify(input.fetch("mySlider", "@fields", "min")) << std::endl;

    JsonValue json;
    JsonParser().load("/home/qadwu/Work/data/vidi3d/insitu/v2/s3d.json", json);

    auto& sourceArr = json["dataSource"].toArray();
    auto& source1 = sourceArr[0];
    auto& source2 = json["dataSource"].toArray()[0];

    std::cout << sourceArr.size() << std::endl;
    std::cout << source1.size() << std::endl;
    std::cout << source2.size() << std::endl;

    auto& blocks1 = source1["blocks"].toArray();
    auto blocks2 = source1.fetch("blocks").toArray();
    std::cout << blocks1.size() << std::endl;
    std::cout << blocks2.size() << std::endl;

    // read the blocks and corresponding ghost region information
    const JsonValue::Array& data = source1.fetch("blocks").toArray();

    auto b = data[0];

    b["file"] = "jsonData[FILE_LOCATION].toString();";

    std::cout << data.size() << std::endl;
    std::cout << JsonValue::typeName(source1.fetch("blocks").type()) << std::endl;
    std::cout << JsonParser().stringify(data[0]) << std::endl;
    std::cout << source1.fetch("blocks").toArray().size() << std::endl;

    // Region& region = _regions[0];

    return 0;
}

