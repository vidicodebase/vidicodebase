//
// Created by Qi Wu on 2/5/20.
//

#if TEST_STD_11
# ifndef VIDI_USE_CXX_STD_11
#  error "VIDI_USE_CXX_STD_11 is not defined"
# endif
# if !(VIDI_USE_CXX_STD_11)
#  error "VIDI_USE_CXX_STD_11 is not false"
# endif
#endif

#if TEST_STD_14
# ifndef VIDI_USE_CXX_STD_14
#  error "VIDI_USE_CXX_STD_14 is not defined"
# endif
# if !(VIDI_USE_CXX_STD_14)
#  error "VIDI_USE_CXX_STD_14 is not false"
# endif
#endif

#if TEST_STD_17
# ifndef VIDI_USE_CXX_STD_17
#  error "VIDI_USE_CXX_STD_17 is not defined"
# endif
# if !(VIDI_USE_CXX_STD_17)
#  error "VIDI_USE_CXX_STD_17 is not false"
# endif
#endif

#if TEST_STD_20
# ifndef VIDI_USE_CXX_STD_20
#  error "VIDI_USE_CXX_STD_20 is not defined"
# endif
# if !(VIDI_USE_CXX_STD_20)
#  error "VIDI_USE_CXX_STD_20 is not false"
# endif
#endif

int main() {
    return 0;
}