//
// Created by Qi Wu on 2019-10-02.
//

#include <vidiBase.h>
#include <iostream>

int
main()
{
    try {
        throw vidi::Exception("this", "is", 1, "error");
    }
    catch (std::exception& e) {
        std::cout << e.what() << "." << std::endl;
    }
    return 1;
}
