#!/bin/bash

if [ -z ${1} ]; then
    REMOTE=origin
else
    REMOTE=${1}
fi

LOCAL=$(git rev-parse --show-toplevel)
ROOT=$(git rev-parse --show-superproject-working-tree)
if [ -z ${ROOT} ]; then
    ROOT=${LOCAL}
fi

# do the top level repository
cd ${ROOT}
git checkout master
git pull ${REMOTE} master
git submodule update --recursive

# do submodules
for m in $(find ${ROOT}/* -maxdepth 0 -type d -name "vidi-*")
do
    cd ${m}
    git checkout master
    git pull ${REMOTE} master
    cd ${ROOT}
done

# double check
if [ -z "$(git status --porcelain)" ]; then 
    echo "All up-to-date"
else 
    echo "Something is wrong!"
fi
