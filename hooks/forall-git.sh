#!/bin/bash

CMD="${@}"
LOCAL=$(git rev-parse --show-toplevel)
ROOT=$(git rev-parse --show-superproject-working-tree)
if [ -z ${ROOT} ]; then
    ROOT=${LOCAL}
fi

# do the top level repository
echo ---------- root ----------
cd ${ROOT}
git ${CMD}

# do submodules
for m in $(find ${ROOT}/* -maxdepth 0 -type d -name "vidi-*")
do
    echo ---------- ${m##*/} ----------
    cd ${m}
    git ${CMD}
    cd ${ROOT}
done
