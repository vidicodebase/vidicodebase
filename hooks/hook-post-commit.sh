#!/bin/sh

exit 1 # not working

function FIND_UPWARD () {
    WORK=${PWD}; cd ${1}
    while [[ ${PWD} != / ]]; do
        RET=$(find "${PWD}"/ -maxdepth 1 "${@:2}")
        if [[ -z "${RET}" ]]; then
            cd ..
        else
            echo ${RET}; cd ${WORK}; break
        fi
    done
}

# The submodule name
PROJ=$(basename `git rev-parse --show-toplevel`)
# GIT root directory
ROOT=$(git rev-parse --show-toplevel)
# The commit message just made
COMMIT=$(git log -1 HEAD --pretty=format:%s)

# Backup some environmental variables
OLD_GIT_DIR=${GIT_DIR}
OLD_GIT_INDEX_FILE=${GIT_INDEX_FILE}
OLD_PWD=$(pwd) # Save the current directory as we want to come back here

if [[ -d "${ROOT}/.git" ]]; then
    cd ${OLD_PWD}
else
    # Now we swap to a different git repository
    export GIT_DIR=$(FIND_UPWARD ${ROOT}/.. -name .git)
    export GIT_INDEX_FILE=${GIT_DIR}/index    
    cd ${ROOT}/.. # One level up from the root should be the base repository
    # Make an automatic commit
    git add ${PROJ}
    git commit -m "[${PROJ} auto] ${COMMIT}"
    # We swap back
    export GIT_DIR=${OLD_GIT_DIR}
    export GIT_INDEX_FILE=${OLD_GIT_INDEX_FILE}
    cd ${OLD_PWD}
fi
