#!/bin/bash

# run from each project's root directory

gitlab_base=git@gitlab.com:vidicodebase
origin_base=git@sphere.cs.ucdavis.edu:vidiCodeBase

root=$(git rev-parse --show-superproject-working-tree)
base=$(basename $PWD)
if [ "${base}" = "$(basename $root)" ]; then
    base=vidiCodeBase
fi
if [ "" = "$(basename $root)" ]; then
    base=vidiCodeBase
fi

if [ -z "$(git remote -v | grep gitlab)" ]; then
    git remote add gitlab ${gitlab_base}/${base}.git
else
    git remote set-url gitlab ${gitlab_base}/${base}.git
fi

if [ -z "$(git remote -v | grep origin)" ]; then
    git remote add origin ${origin_base}/${base}.git
else
    git remote set-url origin ${origin_base}/${base}.git
fi

git remote set-url --delete --push origin ${origin_base}
git remote set-url --delete --push origin ${gitlab_base}
git remote set-url --add --push origin ${origin_base}/${base}.git
git remote set-url --add --push origin ${gitlab_base}/${base}.git
