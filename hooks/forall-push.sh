#!/bin/bash

if [ -z ${1} ]; then
    REMOTE=origin
else
    REMOTE=${1}
fi

LOCAL=$(git rev-parse --show-toplevel)
ROOT=$(git rev-parse --show-superproject-working-tree)
if [ -z ${ROOT} ]; then
    ROOT=${LOCAL}
fi

# do the top level repository
cd ${ROOT}
git push ${REMOTE} master

# do submodules
for m in $(find ${ROOT}/* -maxdepth 0 -type d -name "vidi-*")
do
    cd ${m}
    git push ${REMOTE} master
    cd ${ROOT}
done
