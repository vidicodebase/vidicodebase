#!/bin/bash

# GIT root directory
VIDI=$(git rev-parse --show-toplevel)
ROOT=$(git rev-parse --show-superproject-working-tree)
if [ -z ${ROOT} ]; then
    ROOT=${VIDI}
fi

# The submodule name
PROJ=$(basename ${ROOT})

for m in $(find ${ROOT}/.git/modules -name 'index')
do
    # Compute relative path
    MODULE=$(dirname ${m})/hooks
    SCRIPT=$(python -c "import os.path; print os.path.relpath('${VIDI}/hooks', '${MODULE}')")
    
    # Dry run
    echo ln -s ${SCRIPT}/hook-post-commit.sh post-commit
    # Now do it
    cd ${MODULE}
    rm post-commit
    ln -s ${SCRIPT}/hook-post-commit.sh post-commit
done
