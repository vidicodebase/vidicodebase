# ┌──────────────────────────────────────────────────────────────────┐
# │  Projects Settings                                               │
# └──────────────────────────────────────────────────────────────────┘
cmake_minimum_required(VERSION 3.10)
project(vidi VERSION 1.0)

include(GNUInstallDirs)
include(CMakePackageConfigHelpers)
include(GenerateExportHeader)
set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS TRUE CACHE BOOL "" FORCE)

# ┌──────────────────────────────────────────────────────────────────┐
# │  Setup                                                           │
# └──────────────────────────────────────────────────────────────────┘
include(vidi-cmake/AddLibrary.cmake)
include(vidi-cmake/AddExecutable.cmake)
include(vidi-cmake/AddObject.cmake)
include(vidi-cmake/CMakeCompilerStandard.cmake)

# automatically configure CXX standard
if(CMAKE_CXX_COMPILER_ID MATCHES "PGI")
  check_max_cxx_standard(VIDI_CXX_STD VIDI_CXX_FLAG
    11)
else()
  check_max_cxx_standard(VIDI_CXX_STD VIDI_CXX_FLAG
    17 1z 14 1y 11 1x 0x)
endif()

# options for fine tuning
option(VIDI_TREAT_WARN_AS_ERROR "Treat warnings as errors" OFF)
if(VIDI_TREAT_WARN_AS_ERROR)
  vidi_project_configure(TREAT_WARN_AS_ERROR)
endif()

option(VIDI_INSTALL_DEV_HEADERS "Install development files" ON)
if(VIDI_INSTALL_DEV_HEADERS)
  vidi_project_configure(INSTALL_DEV_HEADERS)
endif()

# set global properties
vidi_project_configure(
  LIB_OUTPUT_PATH ${PROJECT_BINARY_DIR}
  EXE_OUTPUT_PATH ${PROJECT_BINARY_DIR}
  CXX_COMPILE_STD ${VIDI_CXX_STD}
  CXX_COMPILE_STD_FLAG ${VIDI_CXX_FLAG}
  INSTALL_LIBDIR ${CMAKE_INSTALL_LIBDIR}
  INSTALL_BINDIR ${CMAKE_INSTALL_BINDIR}
  INSTALL_INCDIR ${CMAKE_INSTALL_INCLUDEDIR}
  3RDPARTY_ROOT ${CMAKE_CURRENT_LIST_DIR}/ext
)

set(CMAKE_MODULE_PATH
  ${CMAKE_CURRENT_LIST_DIR}/vidi-cmake/modules
  ${CMAKE_MODULE_PATH})

# ┌──────────────────────────────────────────────────────────────────┐
# │  3rdparty Libraries                                              │
# └──────────────────────────────────────────────────────────────────┘
include(vidi-cmake/modules/ConfigureOpenGL.cmake)
vidi_find_opengl()

# ┌──────────────────────────────────────────────────────────────────┐
# │  Add Sub-Libraries                                               │
# └──────────────────────────────────────────────────────────────────┘
add_subdirectory(vidi-base)
add_subdirectory(vidi-math)
add_subdirectory(vidi-serializable)
add_subdirectory(vidi-dynamic)
add_subdirectory(vidi-gfxapi)

# ┌──────────────────────────────────────────────────────────────────┐
# │  Add Applications                                                │
# └──────────────────────────────────────────────────────────────────┘
option(VIDI_BUILD_APP "Build VIDI testing applications" ON)
if(VIDI_BUILD_APP)
  add_subdirectory(apps)
endif()

# ┌──────────────────────────────────────────────────────────────────┐
# │  Add External Packages                                           │
# └──────────────────────────────────────────────────────────────────┘
#foreach(package ${VIDI_ENABLED_PACKAGES})
#  include(Configure${package})
#endforeach()

# ┌──────────────────────────────────────────────────────────────────┐
# │  Export and Documentations                                       │
# └──────────────────────────────────────────────────────────────────┘
vidi_doc_gen()
